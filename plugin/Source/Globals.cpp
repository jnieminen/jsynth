/*
  ==============================================================================

    Globals.cpp
    Created: 19 Sep 2019 8:21:42pm
    Author:  jussi

  ==============================================================================
*/

#include <algorithm>
#include "Globals.h"
#include "OnePoleFilters.h"

std::vector<std::pair<float&, std::vector<float>>>::iterator
ShockAbsorber::find_entry_by_param(const float& value)
{
    for (auto iter = cache.begin(); iter != cache.end(); iter++)
    {
        if (iter->first == value)
            return iter;
    }

    return cache.end();
}

void ShockAbsorber::add_to_cache(float& param, float new_value)
{
    auto entry_iter = find_entry_by_param(param);
    float x = new_value;

    if (entry_iter == cache.end())
    {
        std::pair<float&, std::vector<float>> cache_entry = {param, {new_value}};
        cache.push_back(cache_entry);
        entry_iter = cache.end() - 1;
        const unsigned int num_ticks = 31;

        for (unsigned int i = 0; i < num_ticks; ++i)
            entry_iter->second.push_back(0.0f);
    }

    OnePoleLowpassFilter filter = {};
    filter.state = param;
    const float g = 0.0002f;  // Todo: define g
    auto rev_iter = entry_iter->second.rbegin();

    for (; rev_iter < entry_iter->second.rend() - 1; ++rev_iter)
        *rev_iter = filter.tick(x, g);

    *rev_iter = new_value;
}

void ShockAbsorber::update_parameters()
{
    for (auto& entry : cache)
    {
        entry.first = entry.second.back();
        entry.second.pop_back();
    }

    auto b_empty_cache = [](std::pair<float&, std::vector <float>> entry)
    {
        return entry.second.empty();
    };

    std::remove_if(cache.begin(), cache.end(), b_empty_cache);
}

float ShockAbsorber::get_target(const float& param)
{
    auto iter = find_entry_by_param(param);

    if (iter == cache.end())
        return param;
    else
        return iter->second.front();
}

ParamWithSmoothing::ParamWithSmoothing(float initial_value)
{
    for (unsigned int i = 0; i < cache_size; ++i)
        cache.push_back(initial_value);
}

void ParamWithSmoothing::init_cache()
{
    const auto previous_index = (position-1) % cache.size();
    const float target_value = cache.at(previous_index);
    cache.resize(cache_size);

    for (auto& value : cache)
        value = target_value;
}

/* Updates the static variables that hold the ramp length, the block size and
the cache size. After calling this function, you should call init_cache() for
each object of this class! */
void ParamWithSmoothing::update_cache_size_info(
    double sample_rate, unsigned int new_block_size)
{
    ramp_length = ((unsigned int)sample_rate / 1000);  // 1 ms
    block_size = new_block_size;
    cache_size = round_to_next_power_of_two(ramp_length);

    if (cache_size < block_size)
        cache_size = round_to_next_power_of_two(block_size);
}

void ParamWithSmoothing::operator=(float new_value)
{
    // Calculates a secondary cache using the new value as the target and the
    // final value in the current block as the start (position + blocksize - 1).
    secondary_cache.reserve(cache_size);

    for (auto i = 0; i < cache_size; ++i)
        secondary_cache.push_back(0.0f);

    unsigned int write_index = (position + block_size) % cache_size;
    OnePoleLowpassFilter filter = {};
    filter.state = cache.at((position + block_size - 1) % cache_size);
    const float g = 0.0002f;  // Todo: define g

    for (auto i = 0; i < ramp_length; ++i)
    {
        secondary_cache.at(write_index) = filter.tick(new_value, g);
        ++write_index %= cache_size;
    }

    // Fill the rest of the vector with the target value.
    for (auto i = ramp_length; i < cache.size(); ++i)
    {
        secondary_cache.at(write_index) = new_value;
        ++write_index %= cache_size;
    }
}

float ParamWithSmoothing::get() const
{
    return cache.at(read_index);
}

void ParamWithSmoothing::next_sample() noexcept
{
    ++read_index %= cache_size;
}

void ParamWithSmoothing::update_cache()
{
    if (secondary_cache.empty())
        return;

    cache = secondary_cache;
    secondary_cache.clear();
}

unsigned int ParamWithSmoothing::position = 0;
unsigned int ParamWithSmoothing::read_index = 0;
unsigned int ParamWithSmoothing::block_size = 1;
unsigned int ParamWithSmoothing::ramp_length = 1;
unsigned int ParamWithSmoothing::cache_size = 1;

unsigned int round_to_next_power_of_two(unsigned int num)
{
    unsigned int i = 0;
    while (num > 0)
    {
        num >>= 1;
        ++i;
    }
    return 1 << i;
}

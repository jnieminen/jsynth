/*
  ==============================================================================

    Globals.h
    Created: 18 Sep 2017 7:24:53pm
    Author:  jussi

    This header file contains data structures that are shared by all voices
  ==============================================================================
*/

#pragma once
#include <array>
#include <vector>


enum Waveform : unsigned int
{
    SIN = 0,
    TRI,
    PULSE,
    SAW,
    PULSEPB,
    SAWPB,
    NOISE
};

enum FilterType : unsigned int
{
    LOWPASS_12 = 0,
    LOWPASS_24,
    HIGHPASS_12,
    HIGHPASS_24
};

typedef struct {
    enum Waveform waveform;
    float pulseWidth;
    //int transpose;
    //int fineTune;
    //float detune;
    float inverseSampleRate;
} OscGlobals;

typedef struct {
    enum Waveform waveform;
    //float pulseWidth;
    //int transpose;
    //int fineTune;
} OscPrimary;

typedef struct {
    enum Waveform waveform;
    //int transpose;
    //int fineTune;
    //bool sync;
} OscSecondary;

typedef struct {
    float g;                    // Cutoff in radians
    float res;                  // Resonance gain
    float velDepth;             // Velocity depth scalar
    enum FilterType filterType; // Lowpass 12dB, highpass 24dB, etc.
    float piOverFs;             // Pi / sampleRate
    float maxG;                 // Cutoff maximum in radians
    float minG;                 // Cutoff minimum in radians
} FilterGlobals;

typedef struct {
    // Level parameters
    float decayLv;
    float sustainLv;

    // Upscaled levels for gain saturation
    float decayLvOvershot;
    float sustainLvOvershot;
    float overshootFactor;

    // One pole LP filter coefficients for each stage
    float b0Attack;
    float a1Attack;
    float b0Decay;
    float a1Decay;
    float b0Release;
    float a1Release;
} AegGlobals;

typedef struct {
    float a1Attack;
    float a1Decay;
    float decayLevel;
    float sustainLevel;
    float decayLevelOrig;
    float sustainLevelOrig;
    float attackTimeInv;
    float decayTimeInv;
    float releaseTimeInv;
    float envDepth;
} FegGlobals;

typedef struct {
    float gain;
    float velocity_depth;
    struct { float left_scalar; float right_scalar; } pan;
} AmplifierGlobals;

typedef struct {
    unsigned int time_in_samples;
} PortamentoGlobals;


/* This class acts as a "shock absorber" preventing audible clicks in
situations where a user would rapidly crank knobs between 11 and 0. The class
smooths out parameter changes using a one pole lowpass filter. Several filter
ticks are calculated at once and cached in a vector for performance. The class
provides a method update_parameters() that moves the last values from the cache
to the designated structs.

The process loop should call this method before processing each sample!
Otherwise, parameter changes will not come to effect.
*/
class ShockAbsorber
{
public:
    void add_to_cache(float& param, float new_value);
    void update_parameters();
    float get_target(const float& param);

private:
    std::vector<std::pair<float&, std::vector<float>>>::iterator find_entry_by_param(const float& value);
    std::vector<std::pair<float&, std::vector<float>>> cache = {};
};

class ParamWithSmoothing
{
public:
    ParamWithSmoothing(float intial_value=0.0f);
    void init_cache();
    void update_cache();
    void operator=(float new_value);
    float get() const;
    static void update_cache_size_info(double sample_rate, unsigned int block_size);
    static void next_sample() noexcept;

private:
    static unsigned int position;
    static unsigned int read_index;
    static unsigned int block_size;
    static unsigned int ramp_length;
    static unsigned int cache_size;

    // Allocate caches when blocksize is known. cachelen >= blocksize. The
    // cache initial length depends on the static variable cache_size.
    std::vector<float> cache;
    std::vector<float> secondary_cache;
};

unsigned int round_to_next_power_of_two(unsigned int num);

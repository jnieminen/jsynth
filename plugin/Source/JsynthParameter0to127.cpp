/*
  ==============================================================================

    JsynthParameter0to127.cpp
    Created: 10 Sep 2016 9:20:12pm
    Author:  jussi

  ==============================================================================
*/

#include "JsynthParameter0to127.h"

// Constructor implementation
JsynthParameter0to127::JsynthParameter0to127 (std::function<void(float)> myLambda,
                                                String pid,
                                                String pname,
                                                float minValue,
                                                float maxValue,
                                                float defv)
:
range (minValue, maxValue),
id (pid),
name (pname),
setValueCallback (myLambda),
value (defv),
defValue (defv)
{}

// Empty destructor implementation
JsynthParameter0to127::~JsynthParameter0to127 ()
{}

//int JsynthParameter0to127::limitRange (int v) const noexcept
//{
//    return jlimit (minValue, maxValue, v);
//}
//
//float JsynthParameter0to127::convertTo0to1 (int v) const noexcept
//{
//    return (limitRange (v) - minValue) / (float) (maxValue - minValue);
//}
//
//int JsynthParameter0to127::convertFrom0to1 (float v) const noexcept
//{
//    return limitRange (roundToInt ((v * (float) (maxValue - minValue)) + minValue));
//}

int JsynthParameter0to127::getNumSteps() const
{
    return AudioProcessorParameter::getNumSteps();
}

float JsynthParameter0to127::getValue() const
{
    return range.convertTo0to1 (value);
}

float JsynthParameter0to127::getDefaultValue() const
{
    return range.convertTo0to1 (defValue);
}

float JsynthParameter0to127::getValueForText (const String& text) const
{
    //return convertTo0to1 (text.getIntValue());
    return text.getFloatValue();
}

String JsynthParameter0to127::getText (float v, int length) const
{
    //String asText (range.convertFrom0to1 (v), 2);

    // Present the string with zero decimal places
    String asText (v, 0);
    return length > 0 ? asText.substring (0, length) : asText;
}

// newValue is between 0.0 and 1.0
// This method is always called by the host!
void JsynthParameter0to127::setValue (float newValue)
{
    // Conver to integer (0 - 127)
    value = range.convertFrom0to1 (newValue);

    // Execute lambda expression
    setValueCallback(value);
}

// This method is used by ParameterSlider of the synth!
// Notifies the host of the value change. SetValueNotifyingHost() also calls setValue().
JsynthParameter0to127& JsynthParameter0to127::operator= (float newValue)
{
    if (value != newValue)
        setValueNotifyingHost (range.convertTo0to1 (newValue));

    return *this;
}

String JsynthParameter0to127::getName (int maximumStringLength) const
{
    return name.substring (0, maximumStringLength);
}
String JsynthParameter0to127::getLabel() const
{
    return label;
}

/*
  ==============================================================================

    JsynthParameter0to127.h
    Created: 10 Sep 2016 9:20:12pm
    Author:  jussi

  ==============================================================================
*/

#ifndef JSYNTHPARAMETER0TO127_H_INCLUDED
#define JSYNTHPARAMETER0TO127_H_INCLUDED

#include <functional>
#include "../JuceLibraryCode/JuceHeader.h"

// Generic Slider parameter.

class JsynthParameter0to127 : public AudioProcessorParameter
{
public:

    JsynthParameter0to127 (std::function<void(float)> myLambda,
                            String parameterID, String name, float minValue,
                            float maxValue, float defv);

    ~JsynthParameter0to127 ();

    // newValue can be arbitrary. Use this to interface with the slider value!
    JsynthParameter0to127& operator= (float newValue);

    // Provides methods to map the value range to/from 0..1.
    // Also provides a skew factor to easily create logarithmic value ranges.
    NormalisableRange<float> range;

    /** Returns the parameter's current value. */
    float get() const noexcept      { return value; }

    /** Returns the parameter's current value. */
    operator float() const noexcept { return value; }

    float getValue() const override;
    void setValue (float newValue) override;
    int getNumSteps() const override;
    String getText (float v, int length) const override;
    float getValueForText (const String& text) const override;
    float getDefaultValue() const override;

    /** Provides access to the parameter's ID string. */
    const String id;

    /** Provides access to the parameter's name. */
    const String name;

    /** Provides access to the parameter's label. */
    const String label;

    String getName (int) const override;
    String getLabel() const override;

private:

    std::function<void(float)>setValueCallback;

    float value;
    float defValue;

//    int limitRange (int v) const noexcept;

};




#endif  // JSYNTHPARAMETER0TO127_H_INCLUDED

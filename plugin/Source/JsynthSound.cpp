/*
  ==============================================================================

    JsynthSound.cpp

    Author:  Jussi Nieminen

  ==============================================================================
*/

#include "JsynthSound.h"

// Constructor
JsynthSound::JsynthSound()
{}

// Destructor
JsynthSound::~JsynthSound()
{}

// Member functions
bool JsynthSound::appliesToNote(int midiNoteNumber)
{
    return true;
}

bool JsynthSound::appliesToChannel(int midiChannel)
{
    return true;
}

/*
  ==============================================================================

    JsynthSound.h

    Author:  Jussi Nieminen

  ==============================================================================
*/

#ifndef JSYNTHSOUND_H_INCLUDED
#define JSYNTHSOUND_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class JsynthSound : public SynthesiserSound
{

public:
    // Constructor
    JsynthSound();

    // Destructor
    ~JsynthSound();

    // Member functions
    bool appliesToNote(int midiNoteNumber) override;
    bool appliesToChannel(int midiChannel) override;

};

#endif

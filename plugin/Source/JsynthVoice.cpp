/*
  ==============================================================================

    JsynthVoice.cpp

    Author:  Jussi Nieminen

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "JsynthVoice.h"
#include "JsynthSound.h"
#include <cstdio>


JsynthVoice::JsynthVoice(const JsynthAudioProcessor& p)
    :
    osc1            (p.oscGlobals),
    filter          (p.filterGlobals),
    ampEG           (p.aegGlobals),
    filterEG        (p.fegGlobals),
    processor       (p),
    velocity_scaled (1.0f)
{}

bool JsynthVoice::canPlaySound (SynthesiserSound* sound)
{
    return dynamic_cast<JsynthSound*> (sound) != nullptr;
}

void JsynthVoice::startNote (
    int midiNoteNumber,
    float velocity,
    SynthesiserSound* /*sound*/,
    int /*currentPitchWheelPosition*/
)
{
    frequency = (float)MidiMessage::getMidiNoteInHertz(midiNoteNumber);
    
    osc1.setFrequency(frequency, processor.oscGlobals.inverseSampleRate);
    osc1.initPhase();
    portamento.set_factor(frequency, processor.portamentoGlobals.time_in_samples);
    Portamento::start_frequency = &frequency;

    // Calculate the velocity scalars according to velocity depth parameters.
    velocity_scaled = (1.0f - processor.amplifierGlobals.velocity_depth)
        + velocity * processor.amplifierGlobals.velocity_depth;

    filter.gateOn(velocity);
    ampEG.gateOn();
    filterEG.gateOn();
}

void JsynthVoice::stopNote (float /*velocity*/, bool /*allowTailOff*/)
{
    // The key is released. Set EGs to Release mode.
    ampEG.gateOff();
    filterEG.gateOff();
}

void JsynthVoice::pitchWheelMoved (int /*newValue*/)
{
    // not implemented for the purposes of this demo!
}

void JsynthVoice::controllerMoved (int /*controllerNumber*/, int /*newValue*/)
{
    // not implemented for the purposes of this demo!
}

//void JsynthVoice::setCurrentPlaybackSampleRate (double newSampleRate)
//{
//    SynthesiserVoice::setCurrentPlaybackSampleRate (newSampleRate);
//}

void JsynthVoice::renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples)
{
    // The note has died out if the amp envelope has switched itself off.
    if (ampEG.getState() == EG_OFF)
    {
        clearCurrentNote();
        return;
    }

    while (--numSamples >= 0)
    {
        // OSC
        float currentSample = osc1.calculateSample();

        // FILTER
        currentSample = filter.processSample (currentSample, filterEG.calculateLevel(), 1.0f);

        // AMPLIFIER
        currentSample *= processor.amplifierGlobals.gain;
        currentSample *= velocity_scaled;
        currentSample *= ampEG.calculateLevel();

        std::pair<float, float> stereo_pair = {
            currentSample * processor.amplifierGlobals.pan.left_scalar,
            currentSample * processor.amplifierGlobals.pan.right_scalar
        };

        outputBuffer.addSample(0, startSample, stereo_pair.first);
        outputBuffer.addSample(1, startSample, stereo_pair.second);

        ++startSample;
    }
}

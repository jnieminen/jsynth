/*
  ==============================================================================

    JsynthVoice.h

    Author:  Jussi Nieminen

    Implements a single note that plays. Contains core syntehsizer elements,
    such as oscillators, filters and envelope generators and plugs them in the
    process loop.
  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "EnvelopeGenerator.h"
#include "JsynthOscillators.h"
#include "JsynthFilters.h"
#include "Portamento.h"


class JsynthVoice : public SynthesiserVoice
{
public:
    explicit JsynthVoice(const JsynthAudioProcessor& p);

    bool canPlaySound (SynthesiserSound*) override;
    void startNote (int midiNoteNumber, float velocity,
                    SynthesiserSound* sound,
                    int currentPitchWheelPosition) override;

    void stopNote (float /*velocity*/, bool allowTailOff) override;
    void pitchWheelMoved (int newValue) override;
    void controllerMoved (int controllerNumber, int newValue) override;
    void renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples) override;
    float* get_pointer_to_frequency() { return &frequency; };

    // The following components have voice specific state.
    Oscillator osc1;
    Portamento portamento;
    JsynthFilter filter;
    EnvelopeGenerator ampEG;
    EnvelopeGeneratorFreq filterEG;

private:
    // A reference to the processor enables access to shared data.
    const JsynthAudioProcessor& processor;
    float frequency;
    float velocity_scaled;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (JsynthVoice)
};

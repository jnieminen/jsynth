/*
  ==============================================================================

    ParameterSlider.h
    Created: 8 Sep 2016 12:42:41pm
    Author:  jussi

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "JsynthParameter0to127.h"

// (Taken from the JUCE demo plugin PluginEditor.cpp)

class ParameterSlider   : public Slider,
                          private Timer
{
public:
    ParameterSlider (JsynthParameter0to127& p)
        : Slider (p.getName (256)), param (p)
    {
        setRange(p.range.start, p.range.end, p.range.interval);
        startTimerHz (30);
        Slider::setDoubleClickReturnValue (true, p.getDefaultValue());
        updateSliderPos();
    }

    void valueChanged() override
    {
        //JsynthParameter0to127& rf = static_cast<JsynthParameter0to127&>(param);
        if (isMouseButtonDown())
            //rf = (float) Slider::getValue();
            param.setValueNotifyingHost (param.range.convertTo0to1 ((float) Slider::getValue()));
        else
            param.setValue (param.range.convertTo0to1 ((float) Slider::getValue()));
    }

    void timerCallback() override       { updateSliderPos(); }

    void startedDragging() override     { param.beginChangeGesture(); }
    void stoppedDragging() override     { param.endChangeGesture();   }

    double getValueFromText (const String& text) override   { return param.getValueForText (text); }
    String getTextFromValue (double value) override         { return param.getText ((float) value, 1024); }

    void updateSliderPos()
    {
        //JsynthParameter0to127& rf = static_cast<JsynthParameter0to127&>(param);
        //const float newValue = float(rf);
        const float newValue = float(param);

        if (newValue != (float) Slider::getValue() && ! isMouseButtonDown())
            Slider::setValue (newValue);
    }

    JsynthParameter0to127& param;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ParameterSlider)
};

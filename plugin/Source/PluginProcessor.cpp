/*
  ==============================================================================

    PluginProcessor.cpp

    Author:  Jussi Nieminen

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "JsynthSound.h"
#include "JsynthVoice.h"
#include <math.h>
#include <cstdio>
//==============================================================================
JsynthAudioProcessor::JsynthAudioProcessor()
:
sampleRate (44100.0f)
{
    oscGlobals.waveform             = SIN;
    oscGlobals.pulseWidth           = 0.5f;
    oscGlobals.inverseSampleRate    = 1.0f / sampleRate;
    filterGlobals.g                 = MIN_CUTOFF * float_Pi / sampleRate;
    filterGlobals.res               = 0.0f;
    filterGlobals.filterType        = LOWPASS_12;
    filterGlobals.velDepth          = 0.0f;
    filterGlobals.piOverFs          = float_Pi / sampleRate;
    filterGlobals.maxG              = MAX_CUTOFF * filterGlobals.piOverFs;
    filterGlobals.minG              = MIN_CUTOFF * filterGlobals.piOverFs;
    fegGlobals.a1Attack             = 1.0f;
    fegGlobals.a1Decay              = 1.0f;
    fegGlobals.attackTimeInv        = 0.0f;
    fegGlobals.decayTimeInv         = 0.0f;
    fegGlobals.releaseTimeInv       = 0.0f;
    fegGlobals.decayLevel           = 1.0f;
    fegGlobals.sustainLevel         = 1.0f;
    fegGlobals.decayLevelOrig       = 1.0f;
    fegGlobals.sustainLevelOrig     = 1.0f;
    fegGlobals.envDepth             = 0.0f;
    amplifierGlobals.gain           = 1.0f;
    amplifierGlobals.velocity_depth = 1.0f;
    amplifierGlobals.pan            = {0.5f, 0.5f};
    aegGlobals.overshootFactor      = 1.1566f;
    aegGlobals.decayLv              = 1.0f;
    aegGlobals.sustainLv            = 0.0f;
    aegGlobals.decayLvOvershot      = aegGlobals.overshootFactor;
    aegGlobals.sustainLvOvershot    = 0.0f;
    aegGlobals.b0Attack             = 1.0f;
    aegGlobals.a1Attack             = 1.0f;
    aegGlobals.b0Decay              = 1.0f;
    aegGlobals.a1Decay              = 1.0f;
    aegGlobals.b0Release            = 1.0f;
    aegGlobals.a1Release            = 1.0f;
    portamentoGlobals.time_in_samples = 1;

    const int numVoices = 16;

    // Add some voices...
    for (int i = numVoices; --i >= 0;)
        synth.addVoice (new JsynthVoice(*this));

    // ..and give the synth a sound to play
    synth.addSound (new JsynthSound());

    // Lambda expressions that enable different function calls to be passed
    // to the parameter's setValue() member function
    auto lambdaAegAttack = [this](float sliderValue)
    {
        // attackTime is a float value between [0..127]
        float timeInSamples = lookupTables.getEgTime(sliderValue / 127.0f) * sampleRate;
        //float timeInSamples = interpolate(egTimeLookup,
                                        //(31.0f/127.0f) * sliderValue) * sampleRate;

        aegGlobals.a1Attack = exp(-1.0f / timeInSamples);
        aegGlobals.b0Attack = 1.0f - aegGlobals.a1Attack;
    };

    auto lambdaAegDecay = [this](float sliderValue)
    {
        float timeInSamples = lookupTables.getEgTime(sliderValue / 127.0f) * sampleRate;
        //float timeInSamples = interpolate(egTimeLookup, (31.0f/127.0f) * sliderValue);
        //timeInSamples *= sampleRate;

        aegGlobals.a1Decay = exp(-1 / timeInSamples);
        aegGlobals.b0Decay = 1 - aegGlobals.a1Decay;
    };

    auto lambdaAegSustain = [this](float sliderValue)
    {
        //aegGlobals.sustainLv = param2LevelTable[roundToInt(susLevel)];
        aegGlobals.sustainLv = (1.0f/127.0f) * sliderValue;

        // Calculate an upscaled level so that the gain saturates at sustainLv
        aegGlobals.sustainLvOvershot = aegGlobals.overshootFactor
            * (aegGlobals.sustainLv - aegGlobals.decayLv) + aegGlobals.decayLv;
    };

    auto lambdaAegRelease = [this](float sliderValue)
    {
        float timeInSamples = lookupTables.getEgTime(sliderValue / 127.0f) * sampleRate;
        //float timeInSamples = interpolate(egTimeLookup, (31.0f/127.0f) * sliderValue);
        //timeInSamples *= sampleRate;

        aegGlobals.a1Release = exp(-1.0f / timeInSamples);
        aegGlobals.b0Release = 1.0f - aegGlobals.a1Release;
    };

    auto lambdaFilterCutoff = [this](float sliderValue)
    {
        // g = pi * fc / fs
        filterGlobals.g = lookupTables.getCutoff(sliderValue / 1023.0f)
                        * filterGlobals.piOverFs;
        //filterGlobals.g = interpolate(cutoffLookup, (255.0f/1023.0f) * sliderValue) * filterGlobals.piOverFs;
        //printf("DEBUG g = %f\n", filterGlobals.g);
    };

    auto lambdaFilterResonance = [this](float resonance)
    {
        // Scale by the number of cascading filters
        filterGlobals.res = resonance * 4.0f;
    };

    auto lambdaFilterEnvDepth = [this](float sliderValue)
    {
        fegGlobals.envDepth = lookupTables.getEnvDepth(sliderValue / 127.0f);

        // Scale with the new envelope depth
        fegGlobals.decayLevel = 1.0f + (fegGlobals.decayLevelOrig - 1.0f)
                                     * fegGlobals.envDepth;
        fegGlobals.sustainLevel = 1.0f + (fegGlobals.sustainLevelOrig - 1.0f)
                                       * fegGlobals.envDepth;

        // Update EG coefficients
        fegGlobals.a1Attack = pow(fegGlobals.decayLevel, fegGlobals.attackTimeInv);
        fegGlobals.a1Decay = pow(fegGlobals.sustainLevel, fegGlobals.decayTimeInv);
    };

    auto lambdaFegAttack = [this](float sliderValue)
    {
        //fegGlobals.attackTimeInv = 1.0f / (interpolate(egTimeLookup, (31.0f/127.0f) * attackTime) * sampleRate);
        fegGlobals.attackTimeInv = 1.0f / (lookupTables.getEgTime(sliderValue / 127.0f) * sampleRate);
        fegGlobals.a1Attack = pow(fegGlobals.decayLevel, fegGlobals.attackTimeInv);
    };

    auto lambdaFegDecay = [this](float sliderValue)
    {
        //fegGlobals.decayTimeInv = 1.0f / (interpolate(egTimeLookup, (31.0f/127.0f) * decayTime) * sampleRate);
        fegGlobals.decayTimeInv = 1.0f / (lookupTables.getEgTime(sliderValue / 127.0f) * sampleRate);
        fegGlobals.a1Decay = pow(fegGlobals.sustainLevel, fegGlobals.decayTimeInv);
    };

    auto lambdaFegDecayLevel = [this](float sliderValue)
    {
        //fegGlobals.decayLevelOrig = fegLevelLookup[(unsigned int)(sliderValue + 64.0f)];
        fegGlobals.decayLevelOrig = lookupTables.getFegLevel((unsigned int)(sliderValue + 64.0f));

        fegGlobals.decayLevel = 1.0f + (fegGlobals.decayLevelOrig - 1.0f)
                                     * fegGlobals.envDepth;

        fegGlobals.a1Attack = pow(fegGlobals.decayLevel, fegGlobals.attackTimeInv);
    };

    auto lambdaFegSustain = [this](float sliderValue)
    {
        //fegGlobals.sustainLevelOrig = fegLevelLookup[(unsigned int)(sliderValue + 64.0f)];
        fegGlobals.sustainLevelOrig = lookupTables.getFegLevel((unsigned int)(sliderValue + 64.0f));

        fegGlobals.sustainLevel = 1.0f + (fegGlobals.sustainLevelOrig - 1.0f)
                                       * fegGlobals.envDepth;

        fegGlobals.a1Decay = pow(fegGlobals.sustainLevel, fegGlobals.decayTimeInv);
    };

    auto lambdaFegRelease = [this](float sliderValue)
    {
        //float timeInSamples = interpolate(egTimeLookup, (31.0f/127.0f) * releaseTime) * sampleRate;
        float timeInSamples = lookupTables.getEgTime(sliderValue / 127.0f) * sampleRate;

        fegGlobals.releaseTimeInv = 1.0f / timeInSamples;

        for (int i = synth.getNumVoices(); --i >= 0;)
        {
            JsynthVoice* voice = static_cast<JsynthVoice*> (synth.getVoice(i));
            // Call gateOff again if an EG is in RELEASE mode
            if (voice->filterEG.getState() == EG_RELEASE)
                voice->filterEG.gateOff();
        }
    };

    auto lambdaGain = [this](float slider_value)
    {
        // For now, range [0.0..1.0]
        amplifierGlobals.gain = slider_value;
    };

    auto lambdaPan = [this](float slider_value)
    {
        // For now, range [0.0..1.0]
        amplifierGlobals.pan.left_scalar = 1.0f - slider_value;
        amplifierGlobals.pan.right_scalar = slider_value;
    };

    auto lambdaVelocityDepth = [this](float slider_value)
    {
        // For now, range [0.0..1.0]
        amplifierGlobals.velocity_depth = slider_value;
    };

    /* This creates our parameters. We'll keep some raw pointers to them in
    this class, so that we can easily access them later, but the base
    class will take care of deleting them for us.
    */
    addParameter(aegAttackParam = new JsynthParameter0to127(lambdaAegAttack,
                                                            "aegAttack",
                                                            "AEG attack",
                                                            0.0f, 127.0f, 63.0f));
    addParameter(aegDecayParam = new JsynthParameter0to127(lambdaAegDecay,
                                                            "aegDecay",
                                                            "AEG decay",
                                                            0.0f, 127.0f, 63.0f));
    addParameter(aegSustainParam = new JsynthParameter0to127(lambdaAegSustain,
                                                            "aegSustain",
                                                            "AEG sustain",
                                                            0.0f, 127.0f, 63.0f));
    addParameter(aegReleaseParam = new JsynthParameter0to127(lambdaAegRelease,
                                                            "aegRelease",
                                                            "AEG release",
                                                            0.0f, 127.0f, 63.0f));
    addParameter(filterCutoffParam = new JsynthParameter0to127(lambdaFilterCutoff,
                                                            "filterCutoff",
                                                            "filter cutoff frequency",
                                                            0.0f, 1023.0f, 512.0f));
    addParameter(filterResonanceParam = new JsynthParameter0to127(lambdaFilterResonance,
                                                            "filterResonance",
                                                            "filter resonance",
                                                            0.0f, 1.0f, 0.0f));
    addParameter(filterEnvDepthParam = new JsynthParameter0to127(lambdaFilterEnvDepth,
                                                            "filterEnvDepth",
                                                            "filter envelope depth",
                                                            0.0f, 1.0f, 0.0f));
    addParameter(fegAttackParam = new JsynthParameter0to127(lambdaFegAttack,
                                                            "fegAttack",
                                                            "FEG attack",
                                                            0.0f, 127.0f, 63.0f));
    addParameter(fegDecayParam = new JsynthParameter0to127(lambdaFegDecay,
                                                            "fegDecay",
                                                            "FEG decay",
                                                            0.0f, 127.0f, 63.0f));
    addParameter(fegDecayLevelParam = new JsynthParameter0to127(lambdaFegDecayLevel,
                                                            "fegDecayLevel",
                                                            "FEG decay level",
                                                            -64.0f, 63.0f, 0.0f));
    addParameter(fegSustainParam = new JsynthParameter0to127(lambdaFegSustain,
                                                            "fegSustain",
                                                            "FEG sustain",
                                                            -64.0f, 63.0f, 0.0f));
    addParameter(fegReleaseParam = new JsynthParameter0to127(lambdaFegRelease,
                                                            "fegRelease",
                                                            "FEG release",
                                                            0.0f, 127.0f, 63.0f));
    addParameter(GainParam = new JsynthParameter0to127(
        lambdaGain, "Gain", "Gain", 0.0f, 1.0f, 1.0f
    ));
    addParameter(PanParam = new JsynthParameter0to127(
        lambdaPan, "Pan", "Pan", 0.0f, 1.0f, 0.5f
    ));
    addParameter(VelocityDepthParam = new JsynthParameter0to127(
        lambdaVelocityDepth, "Velocity depth", "Velocity depth", 0.0f, 1.0f, 1.0f
    ));

    // Initialize portamento to the frequency of the first voice.
    Portamento::start_frequency = static_cast<JsynthVoice*>(
        synth.getVoice(0))->get_pointer_to_frequency();
}

JsynthAudioProcessor::~JsynthAudioProcessor()
{}

//==============================================================================
const String JsynthAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool JsynthAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool JsynthAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double JsynthAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int JsynthAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int JsynthAudioProcessor::getCurrentProgram()
{
    return 0;
}

void JsynthAudioProcessor::setCurrentProgram (int index)
{
}

const String JsynthAudioProcessor::getProgramName (int index)
{
    return String();
}

void JsynthAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void JsynthAudioProcessor::prepareToPlay (double newSampleRate, int samplesPerBlock)
{
    sampleRate = float(newSampleRate);

    // Re-calculate parameters that depend on sample rate
    filterGlobals.piOverFs = float_Pi / sampleRate;

    // Adjust cutoff limits according to the new fs so that they stay relatively equal.
    filterGlobals.maxG = (sampleRate / 44100.0f) * MAX_CUTOFF * filterGlobals.piOverFs;
    filterGlobals.minG = (sampleRate / 44100.0f) * MIN_CUTOFF * filterGlobals.piOverFs;

    oscGlobals.inverseSampleRate = 1.0f / sampleRate;
    synth.setCurrentPlaybackSampleRate(newSampleRate);

    // Clear the midi event buffer
    keyboardState.reset();

    // Stop the active synth voices. Clear delay lines etc.
    reset();
}

void JsynthAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
    keyboardState.reset();
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool JsynthAudioProcessor::setPreferredBusArrangement(bool isInput,
                                                      int bus,
                                                      const AudioChannelSet& preferredSet)
{
    // Reject any bus arrangements that are not compatible with your plugin

    const int numChannels = preferredSet.size();

   #if JucePlugin_IsMidiEffect
    if (numChannels != 0)
        return false;
   #elif JucePlugin_IsSynth
    if (isInput || (numChannels != 1 && numChannels != 2))
        return false;
   #else
    if (numChannels != 1 && numChannels != 2)
        return false;

    if (! AudioProcessor::setPreferredBusArrangement (! isInput, bus, preferredSet))
        return false;
   #endif

    return AudioProcessor::setPreferredBusArrangement (isInput, bus, preferredSet);
}
#endif

void JsynthAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    // We only generate output channels

    // Clear the buffers if needed?
    buffer.clear();

    // store numsamples from buffer
    const int numSamples = buffer.getNumSamples();

    // add messages to the buffer if the user is clicking on the on-screen keys
    keyboardState.processNextMidiBuffer (midiMessages, 0, numSamples, true);

    // and now get our synth to process these midi events and generate its output.
    synth.renderNextBlock (buffer, midiMessages, 0, numSamples);

    /*for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        float* channelData = buffer.getWritePointer (channel);

        // ..do something to the data...
    }*/
}

//==============================================================================
bool JsynthAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* JsynthAudioProcessor::createEditor()
{
    return new JsynthAudioProcessorEditor (*this);
}

//==============================================================================
void JsynthAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void JsynthAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

void JsynthAudioProcessor::configureFiltersToLowpass12dB()
{
    for (auto i = 0; i < synth.getNumVoices(); ++i)
    {
        JsynthVoice* voice = static_cast<JsynthVoice*> (synth.getVoice(i));
        voice->filter.configure_lowpass_12dB();
    }
}

void JsynthAudioProcessor::configureFiltersToLowpass24dB()
{
    for (auto i = 0; i < synth.getNumVoices(); ++i)
    {
        JsynthVoice* voice = static_cast<JsynthVoice*> (synth.getVoice(i));
        voice->filter.configure_lowpass_24dB();
    }
}

void JsynthAudioProcessor::configureFiltersToHighpass12dB()
{
    for (auto i = 0; i < synth.getNumVoices(); ++i)
    {
        JsynthVoice* voice = static_cast<JsynthVoice*> (synth.getVoice(i));
        voice->filter.configure_highpass_12dB();
    }
}

void JsynthAudioProcessor::configureFiltersToHighpass24dB()
{
    for (auto i = 0; i < synth.getNumVoices(); ++i)
    {
        JsynthVoice* voice = static_cast<JsynthVoice*> (synth.getVoice(i));
        voice->filter.configure_highpass_24dB();
    }
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new JsynthAudioProcessor();
}

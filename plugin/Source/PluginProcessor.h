/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "JsynthParameter0to127.h"
#include "JsynthTables.h"
#include "Globals.h"

#define MAX_CUTOFF 22020
#define MIN_CUTOFF 40


/* Audio Processor class ==================================================== */

class JsynthAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    JsynthAudioProcessor();
    ~JsynthAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool setPreferredBusArrangement (bool isInput, int bus, const AudioChannelSet& preferredSet) override;
   #endif

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    //==============================================================================
    void configureFiltersToLowpass12dB();
    void configureFiltersToLowpass24dB();
    void configureFiltersToHighpass12dB();
    void configureFiltersToHighpass24dB();

    //int getNumParameters() override;
    //float getParameter(int index) override;

    float sampleRate;
    OscGlobals oscGlobals;
    FilterGlobals filterGlobals;
    FegGlobals fegGlobals;
    AmplifierGlobals amplifierGlobals;
    AegGlobals aegGlobals;
    PortamentoGlobals portamentoGlobals;
    Synthesiser synth;
    MidiKeyboardState keyboardState;

    JsynthParameter0to127* aegAttackParam;
    JsynthParameter0to127* aegDecayParam;
    JsynthParameter0to127* aegSustainParam;
    JsynthParameter0to127* aegReleaseParam;
    JsynthParameter0to127* filterCutoffParam;
    JsynthParameter0to127* filterResonanceParam;
    JsynthParameter0to127* filterEnvDepthParam;
    JsynthParameter0to127* fegAttackParam;
    JsynthParameter0to127* fegDecayParam;
    JsynthParameter0to127* fegDecayLevelParam;
    JsynthParameter0to127* fegSustainParam;
    JsynthParameter0to127* fegReleaseParam;
    JsynthParameter0to127* GainParam;
    JsynthParameter0to127* PanParam;
    JsynthParameter0to127* VelocityDepthParam;

    LookupTables lookupTables;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (JsynthAudioProcessor)
};

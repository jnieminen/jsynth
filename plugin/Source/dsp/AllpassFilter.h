/*
  ==============================================================================

    AllpassFilter.h
    Created: 14 May 2019 11:27:31pm
    Author:  jussi

  ==============================================================================
*/

#pragma once


class AllpassFilter
{
public:
    float tick(const float x)
    {
        float y = a1 * (x - y_prev) + x_prev;
        x_prev = x;
        y_prev = y;
        return y;
    }

    void set_coefficient(const float fraction)
    {
        a1 = (1.0f - fraction) / (fraction + 1.0f);
    }

private:
    float a1     = 0.0f;
    float x_prev = 0.0f;
    float y_prev = 0.0f;
};

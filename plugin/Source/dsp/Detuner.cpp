/*
  ==============================================================================

    Detuner.cpp
    Created: 16 May 2019 11:13:27pm
    Author:  jussi

  ==============================================================================
*/

#include <cmath>
#include "Detuner.h"

Detuner::Detuner(const double sample_rate)
: 
pitch_shifter_a(sample_rate),
pitch_shifter_b(sample_rate),
pitch_shifter_c(sample_rate),
pitch_shifter_d(sample_rate)
{}

void Detuner::set_intensity(const float intensity)
{
    // Set max frequency shift factor to a quint.
    const float max_s = std::pow(2.0f, 5.0f/12.0f);
    float s = intensity * max_s;
    pitch_shifter_a.set_shift_factor(s);
    pitch_shifter_b.set_shift_factor(1.0f / s);
    pitch_shifter_c.set_shift_factor(std::pow(s, 3));
    pitch_shifter_d.set_shift_factor(1.0f / std::pow(s, 3));
}

float Detuner::tick(const float x)
{
    float x_a = pitch_shifter_a.tick(x);
    float x_b = pitch_shifter_b.tick(x);
    float x_c = pitch_shifter_c.tick(x + x_a);
    float x_d = pitch_shifter_d.tick(x + x_b);

    return 0.2f * (x + x_a + x_b + x_c + x_d);
}
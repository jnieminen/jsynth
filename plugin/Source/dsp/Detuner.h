#pragma once

#include "PitchShifter.h"


class Detuner
{
public:
    Detuner(double sample_rate);
    void set_intensity(float intensity);
    float tick(float x);

private:
    PitchShifter pitch_shifter_a;
    PitchShifter pitch_shifter_b;
    PitchShifter pitch_shifter_c;
    PitchShifter pitch_shifter_d;
};

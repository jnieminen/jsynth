/*
  ==============================================================================

    EnvelopeGenerator.cpp
    Created: 15 Aug 2016 1:03:38am
    Author:  jussi

  ==============================================================================
*/

#include "EnvelopeGenerator.h"
#include <math.h>

EnvelopeGenerator::EnvelopeGenerator (const AegGlobals& data)
:
g (0.0f),
globals (data),
currentState (EG_OFF)
{}

void EnvelopeGenerator::copyState (const EnvelopeGenerator& eg)
{
    currentState = eg.getState();
    g = eg.getGain();
}

// Calculates a gain coefficient. Implements logarithmic and exponential
// envelopes using a one-pole low pass filter
float EnvelopeGenerator::calculateLevel() noexcept
{
    switch (currentState)
    {
        case EG_ATTACK:
            g = globals.b0Attack * globals.decayLvOvershot + globals.a1Attack * g;

            if (g > globals.decayLv)
            {
                g = globals.decayLv;
                currentState = EG_DECAY;
            }

            break;

        case EG_DECAY:
            g = globals.b0Decay * globals.sustainLvOvershot + globals.a1Decay * g;

            if (g < globals.sustainLv)
            {
                g = globals.sustainLv;
                currentState = EG_SUSTAIN;
            }

            break;

        case EG_RELEASE:
            g = globals.b0Release / globals.overshootFactor * 0.000001f
                + globals.a1Release * g;

            // Gain is considered to be zero when it drops below -120dB
            if (g < 0.000001f)
            {
                g = 0.0f;
                currentState = EG_OFF;
            }

            break;

        default:
            break;
    }

    return g;
}

/* - EnvelopeGeneratorFreq -------------------------------------------------- */
EnvelopeGeneratorFreq::EnvelopeGeneratorFreq (const FegGlobals& data)
    :
    g (1.0f),
    a1Release (1.0f),
    globals (data),
    currentState (EG_OFF)
{}

void EnvelopeGeneratorFreq::copyState (const EnvelopeGeneratorFreq& eg)
{
    g            = eg.getGain ();
    currentState = eg.getState ();
}

float EnvelopeGeneratorFreq::calculateLevel() noexcept
{
    switch (currentState)
    {
        case EG_ATTACK:
            g *= globals.a1Attack;

            // Forward state when the gain coefficient saturates at
            // the target level.
            if ((globals.decayLevel > 1.0f && g >= globals.decayLevel)
                || (globals.decayLevel <= 1.0f && g <= globals.decayLevel))
            {
                g = globals.decayLevel;
                currentState = EG_DECAY;
            }

            break;

        case EG_DECAY:
            g *= globals.a1Decay;

            if ((globals.sustainLevel > globals.decayLevel && g >= globals.sustainLevel)
                || (globals.sustainLevel <= globals.decayLevel && g <= globals.sustainLevel))
            {
                g = globals.sustainLevel;
                currentState = EG_SUSTAIN;
            }

            break;
        case EG_SUSTAIN:
            break;

        case EG_RELEASE:
            g *= a1Release;

            if ((a1Release > 1.0f && g >= 1.0f)
                || (a1Release <= 1.0f && g <= 1.0f))
            {
                g = 1.0f;
                currentState = EG_OFF;
            }

            break;

        case EG_OFF:
            break;
    }

    return g;
}

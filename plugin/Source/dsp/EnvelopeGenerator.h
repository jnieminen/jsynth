/*
  ==============================================================================

    EnvelopeGenerator.h
    Created: 15 Aug 2016 1:03:38am
    Author:  jussi

  ==============================================================================
*/

#pragma once

#include "PluginProcessor.h"

typedef enum
{
    EG_ATTACK = 0,
    EG_DECAY,
    EG_SUSTAIN,
    EG_RELEASE,
    EG_OFF
} EnvelopeState;

class EnvelopeGenerator
{
public:
    explicit EnvelopeGenerator(const AegGlobals& data);

    EnvelopeState getState () const noexcept
    {
        return currentState;
    }

    float getGain() const noexcept
    {
        return g;
    }

    void gateOn() noexcept
    {
        currentState = EG_ATTACK;
        g = 0.0f;
    }

    void gateOff() noexcept
    {
        currentState = EG_RELEASE;
    }

    void copyState(const EnvelopeGenerator& eg);
    float calculateLevel() noexcept;

private:
    float g;                    // Gain coefficient
    const AegGlobals& globals;  // Reference to the parameter data struct
    EnvelopeState currentState; // Attack, Decay, Sustain, Release or Off
};

/*============================================================================*/
class EnvelopeGeneratorFreq
{
public:
    explicit EnvelopeGeneratorFreq(const FegGlobals& data);

    float getGain() const noexcept
    {
        return g;
    }

    EnvelopeState getState() const noexcept
    {
        return currentState;
    }

    void gateOn() noexcept
    {
        g = 1.0f;
        currentState = EG_ATTACK;
    }

    void gateOff() noexcept
    {
        // Calculate freq ratio based on current envelope gain.
        // Release level is hard coded to 1.0 (filter cutoff).
        a1Release = pow (1.0f / g , globals.releaseTimeInv);
        currentState = EG_RELEASE;
    }

    void copyState(const EnvelopeGeneratorFreq& eg);
    float calculateLevel() noexcept;

private:
    float g;                    // Gain coefficient
    float a1Release;            // Release rate coeff
    const FegGlobals& globals;  // Reference to the global parameter data struct
    EnvelopeState currentState; // ADSR (and OFF)
};

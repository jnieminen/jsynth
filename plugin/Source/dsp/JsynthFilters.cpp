/*
  ==============================================================================

    JsynthFilters.cpp
    Created: 25 Sep 2016 8:43:43pm
    Author:  jussi

  ==============================================================================
*/

#include "JsynthFilters.h"


JsynthFilter::JsynthFilter(const FilterGlobals& glob) : globals(glob)
{}

float JsynthFilter::calculate_gains(float v, size_t t)
{
    //    float G0 = 1.0f / (1.0f + v);       // G^0 scaled by 1 / (1 + g)
    //    float G = v * G0;                   // gain term in ZDF one-pole LP
    //    float G1 = G * G0;                  // G^1 scaled by 1 / (1 + g)
    //    float G2 = G * G1;                  // G^2 scaled by 1 / (1 + g)
    //    float G3 = G * G2;                  // G^3 scaled by 1 / (1 + g)
    //    float Gpow4 = G * G * G * G;        // G^4
    //
    //    // Resolve u. This produces the resonance feedback!
    //    float S = G3 * state1 + G2 * state2 + G1 * state3 + G0 * state4;
    //    float u = (in - globals.res * S) / (1.0f + globals.res * Gpow4);

    gains[t][0] = 1.0f / (1.0f + v);        // G^0 scaled by 1 / (1 + g)
    float G  = v * gains[t][0];             // gain term in ZDF one-pole LP

    for (size_t i = 1; i < gains[t].size(); ++i)
    {
        gains[t][i] = G * gains[t][i - 1];  // G^i scaled by 1 / (1 + g)
    }

    return G;
}

float JsynthFilter::derive_initial_output_sample(float input_sample,
    float resonance, float G, size_t t) const
{
    size_t num_gains = gains[t].size();
    float S = 0.0f;

    for (size_t i = 0; i < num_gains; ++i)
    {
        S += gains[t][num_gains - 1 - i] * states[t][i];
    }

    return (input_sample + resonance * S) / (1.0f + globals.res * std::pow(G, num_gains));
}

void JsynthFilter::clear_vectors()
{
    states.clear();
    gains.clear();
    g_based_on_filter_type.clear();
    resonance_sign.clear();
    one_pole_filters.clear();
}

void JsynthFilter::configure_lowpass_12dB()
{
    clear_vectors();
    g_based_on_filter_type.emplace_back([](float g) { return g; });
    resonance_sign.emplace_back(-1);
    one_pole_filters.push_back({});
    states.push_back({});
    gains.push_back({});

    for (size_t i = 0; i < 2; ++i)
    {
        states.at(0).emplace_back(0.0f);
        gains.at(0).emplace_back(0.0f);
        one_pole_filters.at(0).emplace_back(one_pole_lowpass_filter_tick);
    }
}

void JsynthFilter::configure_lowpass_24dB()
{
    clear_vectors();
    g_based_on_filter_type.emplace_back([](float g) { return g; });
    resonance_sign.emplace_back(-1);
    one_pole_filters.push_back({});
    states.push_back({});
    gains.push_back({});

    for (size_t i = 0; i < 4; ++i)
    {
        states.at(0).emplace_back(0.0f);
        gains.at(0).emplace_back(0.0f);
        one_pole_filters.at(0).emplace_back(one_pole_lowpass_filter_tick);
    }
}

void JsynthFilter::configure_highpass_12dB()
{
    clear_vectors();
    g_based_on_filter_type.emplace_back([](float g) { return float_Pi / 2.0f - g; });
    resonance_sign.emplace_back(1);
    one_pole_filters.push_back({});
    states.push_back({});
    gains.push_back({});

    for (size_t i = 0; i < 2; ++i)
    {
        states.at(0).emplace_back(0.0f);
        gains.at(0).emplace_back(0.0f);
        one_pole_filters.at(0).emplace_back(one_pole_highpass_filter_tick);
    }
}

void JsynthFilter::configure_highpass_24dB()
{
    clear_vectors();
    g_based_on_filter_type.emplace_back([](float g) { return float_Pi / 2.0f - g; });
    resonance_sign.emplace_back(1);
    one_pole_filters.push_back({});
    states.push_back({});
    gains.push_back({});

    for (size_t i = 0; i < 4; ++i)
    {
        states.at(0).emplace_back(0.0f);
        gains.at(0).emplace_back(0.0f);
        one_pole_filters.at(0).emplace_back(one_pole_highpass_filter_tick);
    }
}

void JsynthFilter::configure_bandpass_12dB()
{
    clear_vectors();

    // The cutoff of a a band pass filter is split to highpass and lowpass
    // portions. This is achieved by using the cutoff of the highpass filter
    // for both lowpass and highpass, because the highpass filter mirrors the
    // cutoff by design.
    g_based_on_filter_type.emplace_back([](float g) { return float_Pi / 2.0f - g; });
    g_based_on_filter_type.emplace_back([](float g) { return float_Pi / 2.0f - g; });
    resonance_sign.emplace_back(-1);
    resonance_sign.emplace_back(1);

    for (size_t i = 0; i < 2; ++i)
    {
        one_pole_filters.push_back({});
        states.push_back({});
        gains.push_back({});

        for (size_t j = 0; j < 2; ++j)
        {
            states.at(i).emplace_back(0.0f);
            gains.at(i).emplace_back(0.0f);
        }
    }

    for (size_t i = 0; i < 2; ++i)
        one_pole_filters.at(0).emplace_back(one_pole_lowpass_filter_tick);

    for (size_t i = 0; i < 2; ++i)
        one_pole_filters.at(1).emplace_back(one_pole_highpass_filter_tick);
}


float JsynthFilter::processSample(const float in, const float g_env, const float g_mod) noexcept
{
    float out = in;

    for (size_t t = 0; t < g_based_on_filter_type.size(); ++t)
    {
        float g = g_based_on_filter_type[t](globals.g);
        g *= g_velocity;
        g *= g_env;
        g *= g_mod;
        g = juce::jlimit(globals.minG, globals.maxG, g);
        g = tan(g);  // Prewarp filter coefficient according to bilinear transform.
        float G = calculate_gains(g, t);
        float res = resonance_sign[t] * globals.res;
        out = derive_initial_output_sample(out, res, G, t);

        for (size_t i = 0; i < one_pole_filters[t].size(); ++i)
            out = one_pole_filters[t][i](out, G, states[t][i]);
    }

    return out;
}

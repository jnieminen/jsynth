/*
  ==============================================================================

    JsynthFilters.h
    Created: 25 Sep 2016 8:43:43pm
    Author:  jussi

  ==============================================================================
*/

#pragma once

#include <math.h>
#include <functional>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Globals.h"
#include "OnePoleFilters.h"


/*
   Filter class of the synth.
*/
class JsynthFilter
{
public:
    explicit JsynthFilter(const FilterGlobals& glob);

    void clear_vectors();
    void configure_lowpass_12dB();
    void configure_lowpass_24dB();
    void configure_highpass_12dB();
    void configure_highpass_24dB();
    void configure_bandpass_12dB();

    void gateOn(const float velocity) noexcept
    {
        g_velocity = ((1.0f - globals.velDepth) + velocity * globals.velDepth);
    }

    float processSample(const float in, const float gEnv, const float gMod) noexcept;

private:
    float calculate_gains(float g, size_t t);
    float derive_initial_output_sample(float input_sample, float resonance,
        float G, size_t t) const;

    float g_velocity = 1.0f;
    const FilterGlobals& globals;

    // Initizalize the filter as lowpass 12dB/oct.
    std::vector<std::function<float(float)>> g_based_on_filter_type = {
        [](float g) { return g; }
    };

    std::vector<std::vector<float>> gains  = {{0.0f, 0.0f}};
    std::vector<std::vector<float>> states = {{0.0f, 0.0f}};
    std::vector<int> resonance_sign = {-1 };

    std::vector<std::vector<std::function<float(float, float, float&)>>> one_pole_filters = {
        {
            one_pole_lowpass_filter_tick,
            one_pole_lowpass_filter_tick
        },
    };
};

/*
  ==============================================================================

    JsynthOscillators.cpp
    Created: 24 Sep 2016 9:13:14pm
    Author:  jussi

  ==============================================================================
*/

#include "JsynthOscillators.h"


float Oscillator::calculateSample() noexcept
{
    float out = 0.0f;

    switch (globals.waveform)
    {
        case SIN:
            out = sin ((2.0f * float_Pi) * phase.getAngle());
            break;

        case TRI:

            if ( phase.getAngle() < 0.5f )
            {
                out = 4.0f * phase.getAngle() - 1.0f;
            }
            else
            {
                out = 3.0f - 4.0f * phase.getAngle();
            }

            break;

        case PULSE:
            out = trivialPulse();
            break;

        case SAW:
            out = trivialSaw();
            break;

        case PULSEPB:
            out = trivialPulse();

            // Detect non-linearity and calculate a polynomial
            // 1. Before a step up
            if ( phase.getAngle() >= (1.0f - phase.getDelta()) )
            {
                float t = (phase.getAngle() - 1.0f) / phase.getDelta();
                out += (t*t + 2.0f * t + 1.0f);
            }
            // 2. After a step up
            else if ( phase.getAngle() < phase.getDelta() )
            {
                float t = phase.getAngle() / phase.getDelta();
                out += ( -t*t + 2.0f * t - 1.0f);
            }
            // 3. Before a step down
            else if (phase.getAngle() >= (globals.pulseWidth - phase.getDelta())
                && phase.getAngle() < globals.pulseWidth)
            {
                float t = (phase.getAngle() - globals.pulseWidth) / phase.getDelta();
                out -= (t*t + 2.0f * t + 1.0f);
            }
            // 4. After a step down
            else if ( phase.getAngle() >= globals.pulseWidth
                && phase.getAngle() < (globals.pulseWidth + phase.getDelta()) )
            {
                float t = (phase.getAngle() - globals.pulseWidth) / phase.getDelta();
                out -= ( -t*t + 2.0f * t - 1.0f);
            }

            break;

        case SAWPB:
            out = trivialSaw();

            // Detect non-linearity and calculate a polynomial
            // 1. Before a non-linearity
            if ( phase.getAngle() >= (1.0f - phase.getDelta()) )
            {
                float t = (phase.getAngle() - 1.0f) / phase.getDelta();
                out -= (t*t + 2.0f * t + 1.0f);
            }
            // 2. After a non-linearity
            else if ( phase.getAngle() < phase.getDelta() )
            {
                float t = phase.getAngle() / phase.getDelta();
                out -= ( -t*t + 2.0f * t - 1.0f);
            }

            break;

        case NOISE:
            out = 0.0f;  // TODO
            break;
    }

    phase.update();

    return out;
}


float FrequencyLfo::calculateSample()
{
    float out = 0.0f;

    switch(globals.waveform)
    {
        case SIN:
            out = lookups.getFrequencySin(phase.getAngle());
            break;
        case TRI:
            out = lookups.getFrequencyTri(phase.getAngle());
            break;
        case SAW:
            out = lookups.getFrequencySaw(phase.getAngle());
            break;
    }

    phase.update();

    return out;
}

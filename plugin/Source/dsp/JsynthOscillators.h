/*
  ==============================================================================

    JsynthOscillators.h
    Created: 24 Sep 2016 3:15:31pm
    Author:  jussi

  ==============================================================================
*/

#pragma once

// include juce headers for twoPi and math stuff
#include "../JuceLibraryCode/JuceHeader.h"
#include "Phase.h"
#include "JsynthTables.h"
#include "Globals.h"


// A skeleton of an oscillator that only implements a phase counter. It can be
// used as a base class to build both voice oscillators and LFOs.
class JsynthOscillator
{
public:
    JsynthOscillator() : phase() {};

    void setPhase(const float newPhase)
    {
        // Trust newPhase to be in the range [0..1]
        phase.setAngle(newPhase);
    }

    // These might be called often due to modulation etc.
    void setFrequency(const float frequency, const float inverseSampleRate)
    {
        phase.setDelta(frequency *  inverseSampleRate);
    }

protected:
    Phase phase;
};


class Oscillator : public JsynthOscillator
{
public:
    explicit Oscillator(const OscGlobals& g) : globals(g) {};

    void initPhase()
    {
        switch (globals.waveform)
        {
            case SIN:     setPhase(0.0f);  break;
            case TRI:     setPhase(0.25f); break;
            case PULSE:   setPhase(0.0f);  break;
            case SAW:     setPhase(0.5f);  break;
            case PULSEPB: setPhase(0.0f);  break;
            case SAWPB:   setPhase(0.5f);  break;
            case NOISE:   setPhase(0.0f);  break;
        }
    }

    float trivialSaw() const noexcept
    {
        return 2.0f * phase.getAngle() - 1.0f;
    }

    float trivialPulse() const noexcept
    {
        return phase.getAngle() < globals.pulseWidth ? 1.0f : -1.0f;
    }

    float calculateSample() noexcept;

protected:
    const OscGlobals& globals;
};


// Implements LFOs that operate on frequency (filter cutoff and pitch).
class FrequencyLfo : public JsynthOscillator
{
public:
    FrequencyLfo(const LookupTables& t, const OscGlobals& g)
    :
    lookups(t),
    globals(g)
    {};

    // Calculates a sample using table lookup. The returned sample is used to
    // multiply a frequncy value.
    float calculateSample();

private:
    const LookupTables& lookups;
    const OscGlobals& globals;
};

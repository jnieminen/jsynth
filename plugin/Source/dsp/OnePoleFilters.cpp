/*
  ==============================================================================

    OnePoleFilters.cpp
    Created: 25 Sep 2019 12:08:36am
    Author:  jussi

  ==============================================================================
*/

#include "OnePoleFilters.h"


float one_pole_lowpass_filter_tick(float input_sample, float g, float& state) noexcept
{
    const float v = (input_sample - state) * g;
    const float out = v + state;
    state = out + v;
    return out;
}

float OnePoleLowpassFilter::tick(float input_sample, float g) noexcept
{
    const float v = (input_sample - state) * g;
    const float out = v + state;
    state = out + v;
    return out;
}

float one_pole_highpass_filter_tick(float input_sample, float g, float& state) noexcept
{
    const float v = (input_sample + state) * g;
    const float out = v - state;
    state = out + v;
    return out;
}

float OnePoleHighpassFilter::tick(float input_sample, float g) noexcept
{
    const float v = (input_sample + state) * g;
    const float out = v - state;
    state = out + v;
    return out;
}

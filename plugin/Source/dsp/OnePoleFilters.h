/*
  ==============================================================================

    OnePoleFilters.h
    Created: 25 Sep 2019 12:08:13am
    Author:  jussi

  ==============================================================================
*/

#pragma once


/*
   A one pole lowpass filter that attenuates frequencies by -6dB/octave.
   It holds a state variable and provides a 'tick' function that processors can
   call at every sample. The algorithm is based on the topology-preserving
   transform method described in "The Art Of VA Filter Design", chapter 3.8, by
   Vadim Zavalishin.
 */
float one_pole_lowpass_filter_tick(float in, float g, float& state) noexcept;

/*
   Similar to the one pole lowpass filter, aside from minor differences.
*/
float one_pole_highpass_filter_tick(float in, float g, float& state) noexcept;

class OnePoleLowpassFilter
{
public:
    float tick(float input_sample, float g) noexcept;
    float state = 0.0f;
};

class OnePoleHighpassFilter
{
public:
    float tick(float input_sample, float g) noexcept;
    float state = 0.0f;
};

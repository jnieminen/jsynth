/*
  ==============================================================================

    Phase.h
    Created: 3 Sep 2017 12:17:32pm
    Author:  jussi

  ==============================================================================
*/

#pragma once

class Phase
{
public:
    // Other maximum angles than 1 can be used when implementing table lookup
    // oscillators for example.
    explicit Phase()
    :
    currentAngle (0.0f),
    angleDelta   (0.0f)
    {}

    void setAngle(const float newAngle) noexcept
    {
        currentAngle = newAngle;
    }

    float getAngle() const noexcept
    {
        return currentAngle;
    }

    void setDelta(const float newDelta) noexcept
    {
        angleDelta = newDelta;
    }

    float getDelta() const noexcept
    {
        return angleDelta;
    }

    void update() noexcept
    {
        currentAngle += angleDelta;

        if (currentAngle >= 1.0f)
            currentAngle -= 1.0f;
    }

private:
    float currentAngle;
    float angleDelta;
};

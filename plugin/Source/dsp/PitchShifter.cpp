/*
  ==============================================================================

    PitchShifter.cpp
    Created: 16 May 2019 11:02:58pm
    Author:  jussi

  ==============================================================================
*/

#include <cmath>
#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PitchShifter.h"

PitchShifter::PitchShifter(const float sample_rate)
{
    size_t delay_line_size = (size_t)floor(sample_rate / 20.0f);
    delay_line.reserve(delay_line_size);

    for (size_t i = 0; i < delay_line_size; ++i)
        delay_line.push_back(0.0f);

    p_write = delay_line.begin();

    calculate_amp_mod_delta();
}

void PitchShifter::calculate_amp_mod_delta()
{
    amp_mod_delta = (shift_factor - 1.0f) / delay_line.size();
}

void PitchShifter::set_shift_factor(const float factor)
{
    shift_factor = factor;
    calculate_amp_mod_delta();
}

void PitchShifter::set_shift_factor(const float f, const float f_target)
{
    shift_factor = f_target / f;
    calculate_amp_mod_delta();
}

float PitchShifter::tick(float x)
{
    std::array<float, 3> y = {0.0f, 0.0f, 0.0f};

    for (size_t i = 0; i < a_amp_mod.size(); ++i)
        a_amp_mod[i] = std::cos(2 * float_Pi * phases[i]);

    for (float& phase : phases)
    {
        phase += amp_mod_delta;
        if (phase >= 1.0f)
            phase -= 1.0f;
    }

    for (size_t i = 0; i < y.size(); ++i)
    {
        // Fractional delay filter
        unsigned int integer_part = (unsigned int)std::floor(a_read_pointers[i]);
        float fractional_part = a_read_pointers[i] - integer_part;
        a_allpass_filter[i].set_coefficient(fractional_part);
        y[i] = delay_line[integer_part];
        y[i] = a_allpass_filter[i].tick(y[i]);

        // Amplitude modulation
        y[i] += y[i] * a_amp_mod[i];
    }

    for (float& read_ptr : a_read_pointers)
    {
        read_ptr += shift_factor;

        if (read_ptr >= delay_line.size())
            read_ptr -= delay_line.size();
    }

    *p_write = x;

    if (++p_write == delay_line.end())
        p_write = delay_line.begin();

    return (y[0] + y[1] + y[2]) / 3.0f;
}

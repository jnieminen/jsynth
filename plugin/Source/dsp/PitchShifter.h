/*
  ==============================================================================

    PitchShifter.h
    Created: 14 May 2019 11:27:03pm
    Author:  jussi

  ==============================================================================
*/

#pragma once

#include <array>
#include <vector>
#include "AllpassFilter.h"


class PitchShifter
{
public:
    PitchShifter(const float sample_rate);

    void set_shift_factor(const float factor);
    void set_shift_factor(const float f, const float f_target);

    float tick(float x);

private:
    void calculate_amp_mod_delta();

    float shift_factor = 1.0f;
    float amp_mod_delta = 0.0f;
    std::array<AllpassFilter, 3> a_allpass_filter = {};
    std::vector<float> delay_line = {};
    std::vector<float>::iterator p_write;
    std::array<float, 3> a_read_pointers = {{0.0f, 0.0f, 0.0f}};
    std::array<float, 3> phases = {{0.0f, 1/3, 2/3}};
    std::array<float, 3> a_amp_mod = {{1.0f, 1.0f, 1.0f}};
};

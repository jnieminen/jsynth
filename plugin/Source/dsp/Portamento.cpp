/*
  ==============================================================================

    Portamento.cpp
    Created: 13 Jun 2019 11:28:01pm
    Author:  jussi

  ==============================================================================
*/

#include "Portamento.h"
#include "../JuceLibraryCode/JuceHeader.h"

// This should be set to a valid frequency before calling set_factor.
float* Portamento::start_frequency = nullptr;

void Portamento::set_factor(float target_frequency, float time_in_samples)
{
    jassert(time_in_samples != 0.0f);
    factor = pow(target_frequency / *start_frequency, 1.0f / time_in_samples);
}
/*
  ==============================================================================

    Portamento.h
    Created: 28 Feb 2019 12:19:46am
    Author:  jussi

  ==============================================================================
*/

#pragma once

class Portamento
{
public:
    float tick(float g_in) noexcept
    {
        return g_in * factor;
    }

    void set_factor(float target_frequency, float time_in_samples);

    static float* start_frequency;
private:
    float factor = 1.0f;
};

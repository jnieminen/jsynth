/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.3.2

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
#include "Globals.h"
//[/Headers]

#include "PluginEditor.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...

void JsynthAudioProcessorEditor::resize_a_set_of_rotary_sliders(
    unsigned int x, unsigned int y, std::vector<ParameterSlider*> sliders) const
{
    const rotary_slider_measures measures;
    for (auto& slider : sliders)
    {
        slider->setBounds(x, y, measures.width, measures.height);
        x += measures.distance_between_sliders;
    }
}

void JsynthAudioProcessorEditor::resize_a_set_of_vertical_sliders(
    unsigned int x, unsigned int y, std::vector<ParameterSlider*> sliders) const
{
    const vertical_slider_measures measures;
    for (auto& slider : sliders)
    {
        slider->setBounds(x, y, measures.width, measures.height);
        x += measures.width;
    }
}

//[/MiscUserDefs]

//==============================================================================
JsynthAudioProcessorEditor::JsynthAudioProcessorEditor (JsynthAudioProcessor& p)
    : AudioProcessorEditor (p), processor (p)
{
    //[Constructor_pre] You can add your own custom stuff here..

    // Sliders
    aAttackTimeSlider = std::make_unique<ParameterSlider>(*(p.aegAttackParam));
    addAndMakeVisible(*aAttackTimeSlider);
    aAttackTimeSlider->setTooltip (TRANS("Attack"));
    aAttackTimeSlider->setSliderStyle (Slider::LinearVertical);
    aAttackTimeSlider->setPopupDisplayEnabled(true, true, this);
    aAttackTimeSlider->setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);

    aDecayTimeSlider = std::make_unique<ParameterSlider>(*(p.aegDecayParam));
    addAndMakeVisible(*aDecayTimeSlider);
    aDecayTimeSlider->setTooltip (TRANS("Decay"));
    aDecayTimeSlider->setSliderStyle (Slider::LinearVertical);
    aDecayTimeSlider->setPopupDisplayEnabled(true, true, this);
    aDecayTimeSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    aSustainLevelSlider = std::make_unique<ParameterSlider>(*(p.aegSustainParam));
    addAndMakeVisible(*aSustainLevelSlider);
    aSustainLevelSlider->setTooltip (TRANS("Sustain"));
    aSustainLevelSlider->setSliderStyle (Slider::LinearVertical);
    aSustainLevelSlider->setPopupDisplayEnabled(true, true, this);
    aSustainLevelSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    aReleaseTimeSlider = std::make_unique<ParameterSlider>(*(p.aegReleaseParam));
    addAndMakeVisible(*aReleaseTimeSlider);
    aReleaseTimeSlider->setTooltip (TRANS("Release"));
    aReleaseTimeSlider->setSliderStyle (Slider::LinearVertical);
    aReleaseTimeSlider->setPopupDisplayEnabled(true, true, this);
    aReleaseTimeSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    filterCutoffSlider = std::make_unique<ParameterSlider>(*(p.filterCutoffParam));
    addAndMakeVisible(*filterCutoffSlider);
    filterCutoffSlider->setTooltip(TRANS("Cutoff"));
    filterCutoffSlider->setRange(
        filterCutoffSlider->getMinimum(),
        filterCutoffSlider->getMaximum(),
        1.0
    );
    filterCutoffSlider->setSliderStyle(Slider::RotaryVerticalDrag);
    filterCutoffSlider->setPopupDisplayEnabled(true, true, this);
    filterCutoffSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    filterResonanceSlider = std::make_unique<ParameterSlider>(*(p.filterResonanceParam));
    addAndMakeVisible(*filterResonanceSlider);
    filterResonanceSlider->setTooltip (TRANS("Resonance"));
    filterResonanceSlider->setRange(
        filterResonanceSlider->getMinimum(),
        filterResonanceSlider->getMaximum(),
        0.01
    );
    filterResonanceSlider->setSliderStyle (Slider::RotaryVerticalDrag);
    filterResonanceSlider->setPopupDisplayEnabled(true, true, this);
    filterResonanceSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    filterEnvDepthSlider = std::make_unique<ParameterSlider>(*(p.filterEnvDepthParam));
    addAndMakeVisible(*filterEnvDepthSlider);
    filterEnvDepthSlider->setTooltip (TRANS("Envelope depth"));
    filterEnvDepthSlider->setSliderStyle (Slider::RotaryVerticalDrag);
    filterEnvDepthSlider->setPopupDisplayEnabled(true, true, this);
    filterEnvDepthSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    fAttackTimeSlider = std::make_unique<ParameterSlider>(*(p.fegAttackParam));
    addAndMakeVisible (*fAttackTimeSlider);
    fAttackTimeSlider->setTooltip (TRANS("Attack"));
    fAttackTimeSlider->setSliderStyle (Slider::LinearVertical);
    fAttackTimeSlider->setPopupDisplayEnabled(true, true, this);
    fAttackTimeSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    fDecayTimeSlider = std::make_unique<ParameterSlider>(*(p.fegDecayParam));
    addAndMakeVisible(*fDecayTimeSlider);
    fDecayTimeSlider->setTooltip (TRANS("Decay"));
    fDecayTimeSlider->setSliderStyle (Slider::LinearVertical);
    fDecayTimeSlider->setPopupDisplayEnabled(true, true, this);
    fDecayTimeSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    fDecayLevelSlider = std::make_unique<ParameterSlider>(*(p.fegDecayLevelParam));
    addAndMakeVisible(*fDecayLevelSlider);
    fDecayLevelSlider->setTooltip (TRANS("Decay"));
    fDecayLevelSlider->setRange(
        fDecayLevelSlider->getMinimum(),
        fDecayLevelSlider->getMaximum(),
        1.0
    );
    fDecayLevelSlider->setSliderStyle (Slider::LinearVertical);
    fDecayLevelSlider->setPopupDisplayEnabled(true, true, this);
    fDecayLevelSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    fSustainLevelSlider = std::make_unique<ParameterSlider>(*(p.fegSustainParam));
    addAndMakeVisible(*fSustainLevelSlider);
    fSustainLevelSlider->setTooltip (TRANS("Sustain"));
    fSustainLevelSlider->setRange(
        fSustainLevelSlider->getMinimum(),
        fSustainLevelSlider->getMaximum(),
        1.0
    );
    fSustainLevelSlider->setSliderStyle (Slider::LinearVertical);
    fSustainLevelSlider->setPopupDisplayEnabled(true, true, this);
    fSustainLevelSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    fReleaseTimeSlider = std::make_unique<ParameterSlider>(*(p.fegReleaseParam));
    addAndMakeVisible(*fReleaseTimeSlider);
    fReleaseTimeSlider->setTooltip (TRANS("Release"));
    fReleaseTimeSlider->setSliderStyle (Slider::LinearVertical);
    fReleaseTimeSlider->setPopupDisplayEnabled(true, true, this);
    fReleaseTimeSlider->setTextBoxStyle(Slider::NoTextBox, 0 ,0 ,0);

    GainSlider = std::make_unique<ParameterSlider>(*(p.GainParam));
    addAndMakeVisible(*GainSlider);
    GainSlider->setTooltip(TRANS("Gain"));
    GainSlider->setSliderStyle(Slider::RotaryVerticalDrag);
    GainSlider->setPopupDisplayEnabled(true, true, this);
    GainSlider->setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);

    PanSlider = std::make_unique<ParameterSlider>(*(p.PanParam));
    addAndMakeVisible(*PanSlider);
    PanSlider->setTooltip(TRANS("Pan"));
    PanSlider->setSliderStyle(Slider::RotaryVerticalDrag);
    PanSlider->setPopupDisplayEnabled(true, true, this);
    PanSlider->setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);

    VelocityDepthSlider = std::make_unique<ParameterSlider>(*(p.VelocityDepthParam));
    addAndMakeVisible(*VelocityDepthSlider);
    VelocityDepthSlider->setTooltip(TRANS("Pan"));
    VelocityDepthSlider->setSliderStyle(Slider::RotaryVerticalDrag);
    VelocityDepthSlider->setPopupDisplayEnabled(true, true, this);
    VelocityDepthSlider->setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);

    // Labels
    aegAttackLabel = std::make_unique<Label>("aeg a label", TRANS("A"));
    addAndMakeVisible(*aegAttackLabel);
    aegAttackLabel->setFont (Font (12.00, Font::plain));
    aegAttackLabel->setJustificationType (Justification::centredBottom);
    aegAttackLabel->setEditable (false, false, false);
    aegAttackLabel->setColour (TextEditor::textColourId, Colours::black);
    aegAttackLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    aegAttackLabel->attachToComponent (aAttackTimeSlider.get(), false);

    aegDecayLabel = std::make_unique<Label>("aeg d label", TRANS("D"));
    addAndMakeVisible(*aegDecayLabel);
    aegDecayLabel->setFont (Font (12.00, Font::plain));
    aegDecayLabel->setJustificationType (Justification::centredBottom);
    aegDecayLabel->setEditable (false, false, false);
    aegDecayLabel->setColour (TextEditor::textColourId, Colours::black);
    aegDecayLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    aegDecayLabel->attachToComponent (aDecayTimeSlider.get(), false);

    aegSustainLabel = std::make_unique<Label>("aeg s label", TRANS("S"));
    addAndMakeVisible(*aegSustainLabel);
    aegSustainLabel->setFont (Font (12.00, Font::plain));
    aegSustainLabel->setJustificationType (Justification::centredBottom);
    aegSustainLabel->setEditable (false, false, false);
    aegSustainLabel->setColour (TextEditor::textColourId, Colours::black);
    aegSustainLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    aegSustainLabel->attachToComponent (aSustainLevelSlider.get(), false);

    aegReleaseLabel = std::make_unique<Label>("aeg r label", TRANS("R"));
    addAndMakeVisible(*aegReleaseLabel);
    aegReleaseLabel->setFont (Font (12.00, Font::plain));
    aegReleaseLabel->setJustificationType (Justification::centredBottom);
    aegReleaseLabel->setEditable (false, false, false);
    aegReleaseLabel->setColour (TextEditor::textColourId, Colours::black);
    aegReleaseLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    aegReleaseLabel->attachToComponent (aReleaseTimeSlider.get(), false);

    cutoffLabel = std::make_unique<Label>("cutoff label", TRANS("Cut"));
    addAndMakeVisible(*cutoffLabel);
    cutoffLabel->setFont (Font (12.00, Font::plain));
    cutoffLabel->setJustificationType (Justification::centredBottom);
    cutoffLabel->setEditable (false, false, false);
    cutoffLabel->setColour (TextEditor::textColourId, Colours::black);
    cutoffLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    cutoffLabel->attachToComponent (filterCutoffSlider.get(), false);

    resonanceLabel = std::make_unique<Label>("resonance label", TRANS("Res"));
    addAndMakeVisible(*resonanceLabel);
    resonanceLabel->setFont (Font (12.00, Font::plain));
    resonanceLabel->setJustificationType (Justification::centredBottom);
    resonanceLabel->setEditable (false, false, false);
    resonanceLabel->setColour (TextEditor::textColourId, Colours::black);
    resonanceLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    resonanceLabel->attachToComponent (filterResonanceSlider.get(), false);

    filterEnvDepthLabel = std::make_unique<Label>("feg depth label", TRANS("Env"));
    addAndMakeVisible(*filterEnvDepthLabel);
    filterEnvDepthLabel->setFont (Font (12.00, Font::plain));
    filterEnvDepthLabel->setJustificationType (Justification::centredBottom);
    filterEnvDepthLabel->setEditable (false, false, false);
    filterEnvDepthLabel->setColour (TextEditor::textColourId, Colours::black);
    filterEnvDepthLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    filterEnvDepthLabel->attachToComponent (filterEnvDepthSlider.get(), false);

    fegAttackLabel = std::make_unique<Label>("feg a label", TRANS("A"));
    addAndMakeVisible(*fegAttackLabel);
    fegAttackLabel->setFont (Font (12.00, Font::plain));
    fegAttackLabel->setJustificationType (Justification::centredBottom);
    fegAttackLabel->setEditable (false, false, false);
    fegAttackLabel->setColour (TextEditor::textColourId, Colours::black);
    fegAttackLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    fegAttackLabel->attachToComponent (fAttackTimeSlider.get(), false);

    fegDecayLabel = std::make_unique<Label>("feg d label", TRANS("D"));
    addAndMakeVisible(*fegDecayLabel);
    fegDecayLabel->setFont (Font (12.00, Font::plain));
    fegDecayLabel->setJustificationType (Justification::centredBottom);
    fegDecayLabel->setEditable (false, false, false);
    fegDecayLabel->setColour (TextEditor::textColourId, Colours::black);
    fegDecayLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    fegDecayLabel->attachToComponent (fDecayTimeSlider.get(), false);

    fegDecayLevelLabel = std::make_unique<Label>("feg dl label", TRANS("DL"));
    addAndMakeVisible(*fegDecayLevelLabel);
    fegDecayLevelLabel->setFont (Font (12.00, Font::plain));
    fegDecayLevelLabel->setJustificationType (Justification::centredBottom);
    fegDecayLevelLabel->setEditable (false, false, false);
    fegDecayLevelLabel->setColour (TextEditor::textColourId, Colours::black);
    fegDecayLevelLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    fegDecayLevelLabel->attachToComponent (fDecayLevelSlider.get(), false);

    fegSustainLabel = std::make_unique<Label>("feg s label", TRANS("S"));
    addAndMakeVisible(*fegSustainLabel);
    fegSustainLabel->setFont (Font (12.00, Font::plain));
    fegSustainLabel->setJustificationType (Justification::centredBottom);
    fegSustainLabel->setEditable (false, false, false);
    fegSustainLabel->setColour (TextEditor::textColourId, Colours::black);
    fegSustainLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    fegSustainLabel->attachToComponent (fSustainLevelSlider.get(), false);

    fegReleaseLabel = std::make_unique<Label>("feg r label", TRANS("R"));
    addAndMakeVisible(*fegReleaseLabel);
    fegReleaseLabel->setFont (Font (12.00, Font::plain));
    fegReleaseLabel->setJustificationType (Justification::centredBottom);
    fegReleaseLabel->setEditable (false, false, false);
    fegReleaseLabel->setColour (TextEditor::textColourId, Colours::black);
    fegReleaseLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));
    fegReleaseLabel->attachToComponent (fReleaseTimeSlider.get(), false);

    GainLabel = std::make_unique<Label>("gain label", TRANS("Gain"));
    addAndMakeVisible(*GainLabel);
    GainLabel->setFont(Font(12.00, Font::plain));
    GainLabel->setJustificationType(Justification::centredBottom);
    GainLabel->setEditable(false, false, false);
    GainLabel->setColour(TextEditor::textColourId, Colours::black);
    GainLabel->setColour(TextEditor::backgroundColourId, Colour(0x00000000));
    GainLabel->attachToComponent(GainSlider.get(), false);

    PanLabel = std::make_unique<Label>("pan label", TRANS("Pan"));
    addAndMakeVisible(*PanLabel);
    PanLabel->setFont(Font(12.00, Font::plain));
    PanLabel->setJustificationType(Justification::centredBottom);
    PanLabel->setEditable(false, false, false);
    PanLabel->setColour(TextEditor::textColourId, Colours::black);
    PanLabel->setColour(TextEditor::backgroundColourId, Colour(0x00000000));
    PanLabel->attachToComponent(PanSlider.get(), false);

    VelocityDepthLabel = std::make_unique<Label>("velocity depth label", TRANS("Velocity"));
    addAndMakeVisible(*VelocityDepthLabel);
    VelocityDepthLabel->setFont(Font(12.00, Font::plain));
    VelocityDepthLabel->setJustificationType(Justification::centredBottom);
    VelocityDepthLabel->setEditable(false, false, false);
    VelocityDepthLabel->setColour(TextEditor::textColourId, Colours::black);
    VelocityDepthLabel->setColour(TextEditor::backgroundColourId, Colour(0x00000000));
    VelocityDepthLabel->attachToComponent(VelocityDepthSlider.get(), false);
    //[/Constructor_pre]

    oscGroup.reset (new GroupComponent ("Oscillator group",
                                        TRANS("Osc")));
    addAndMakeVisible (oscGroup.get());
    oscGroup->setTextLabelPosition (Justification::centredLeft);

    oscGroup->setBounds (2, 2, 130, 102);

    filterGroup.reset (new GroupComponent ("Filter group",
                                           TRANS("Filter")));
    addAndMakeVisible (filterGroup.get());
    filterGroup->setTextLabelPosition (Justification::centredLeft);

    filterGroup->setBounds (130, 2, 164, 102);

    ampGroup.reset (new GroupComponent ("Amp group",
                                        TRANS("Amp")));
    addAndMakeVisible (ampGroup.get());
    ampGroup->setTextLabelPosition (Justification::centredLeft);

    ampGroup->setBounds (292, 2, 136, 102);

    toggleLP12.reset (new ToggleButton ("LP12"));
    addAndMakeVisible (toggleLP12.get());
    toggleLP12->setRadioGroupId (2);
    toggleLP12->setToggleState (true, dontSendNotification);

    toggleLP12->setBounds (140, 20, 44, 20);

    toggleLP24.reset (new ToggleButton ("LP24"));
    addAndMakeVisible (toggleLP24.get());
    toggleLP24->setRadioGroupId (2);

    toggleLP24->setBounds (140, 37, 44, 20);

    toggleHP12.reset (new ToggleButton ("HP12"));
    addAndMakeVisible (toggleHP12.get());
    toggleHP12->setRadioGroupId (2);

    toggleHP12->setBounds (140, 54, 44, 20);

    toggleHP24.reset (new ToggleButton ("HP24"));
    addAndMakeVisible (toggleHP24.get());
    toggleHP24->setRadioGroupId (2);

    toggleHP24->setBounds (140, 71, 44, 20);

    toggleOscSin.reset (new ToggleButton ("sin"));
    addAndMakeVisible (toggleOscSin.get());
    toggleOscSin->setConnectedEdges (Button::ConnectedOnBottom);
    toggleOscSin->setRadioGroupId (1);
    toggleOscSin->setToggleState (true, dontSendNotification);

    toggleOscSin->setBounds (12, 20, 44, 20);

    toggleOscTri.reset (new ToggleButton ("tri"));
    addAndMakeVisible (toggleOscTri.get());
    toggleOscTri->setConnectedEdges (Button::ConnectedOnTop | Button::ConnectedOnBottom);
    toggleOscTri->setRadioGroupId (1);

    toggleOscTri->setBounds (12, 37, 44, 20);

    toggleOscPulse.reset (new ToggleButton ("pulse"));
    addAndMakeVisible (toggleOscPulse.get());
    toggleOscPulse->setConnectedEdges (Button::ConnectedOnTop | Button::ConnectedOnBottom);
    toggleOscPulse->setRadioGroupId (1);

    toggleOscPulse->setBounds (12, 54, 44, 20);

    toggleOscSaw.reset (new ToggleButton ("saw"));
    addAndMakeVisible (toggleOscSaw.get());
    toggleOscSaw->setConnectedEdges (Button::ConnectedOnTop);
    toggleOscSaw->setRadioGroupId (1);

    toggleOscSaw->setBounds (12, 71, 44, 20);

    ampEgGroup.reset (new GroupComponent ("AEG",
                                          TRANS("AEG")));
    addAndMakeVisible (ampEgGroup.get());
    ampEgGroup->setTextLabelPosition (Justification::centredLeft);

    ampEgGroup->setBounds (292, 103, 136, 112);

    filterEgGroup.reset (new GroupComponent ("FEG",
                                             TRANS("FEG")));
    addAndMakeVisible (filterEgGroup.get());
    filterEgGroup->setTextLabelPosition (Justification::centredLeft);

    filterEgGroup->setBounds (130, 103, 164, 112);

    pitchEgGroup.reset (new GroupComponent ("PEG",
                                            TRANS("PEG")));
    addAndMakeVisible (pitchEgGroup.get());
    pitchEgGroup->setTextLabelPosition (Justification::centredLeft);

    pitchEgGroup->setBounds (2, 103, 130, 112);


    //[UserPreSize]
    toggleOscSin->addListener (this);
    toggleOscTri->addListener (this);
    toggleOscPulse->addListener (this);
    toggleOscSaw->addListener (this);
    toggleLP12->addListener (this);
    toggleLP24->addListener (this);
    toggleHP12->addListener (this);
    toggleHP24->addListener (this);
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

JsynthAudioProcessorEditor::~JsynthAudioProcessorEditor()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    oscGroup = nullptr;
    filterGroup = nullptr;
    ampGroup = nullptr;
    toggleLP12 = nullptr;
    toggleLP24 = nullptr;
    toggleHP12 = nullptr;
    toggleHP24 = nullptr;
    toggleOscSin = nullptr;
    toggleOscTri = nullptr;
    toggleOscPulse = nullptr;
    toggleOscSaw = nullptr;
    ampEgGroup = nullptr;
    filterEgGroup = nullptr;
    pitchEgGroup = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void JsynthAudioProcessorEditor::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff0f0e0d));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void JsynthAudioProcessorEditor::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    //[UserResized] Add your own custom resize handling here..

    toggleLP12->changeWidthToFitText ();
    toggleLP24->changeWidthToFitText ();
    toggleHP12->changeWidthToFitText ();
    toggleHP24->changeWidthToFitText ();
    toggleOscSin->changeWidthToFitText ();
    toggleOscTri->changeWidthToFitText ();
    toggleOscPulse->changeWidthToFitText ();
    toggleOscSaw->changeWidthToFitText ();

    resize_a_set_of_rotary_sliders(
        192, 34, {
            filterCutoffSlider.get(),
            filterResonanceSlider.get(),
            filterEnvDepthSlider.get()
        }
    );

    resize_a_set_of_vertical_sliders(
        140, 130, {
            fAttackTimeSlider.get(),
            fDecayTimeSlider.get(),
            fDecayLevelSlider.get(),
            fSustainLevelSlider.get(),
            fReleaseTimeSlider.get()
        }
    );

    resize_a_set_of_rotary_sliders(
        302, 34, { GainSlider.get(), VelocityDepthSlider.get(), PanSlider.get() }
    );

    resize_a_set_of_vertical_sliders(
        302, 130, {
            aAttackTimeSlider.get(),
            aDecayTimeSlider.get(),
            aSustainLevelSlider.get(),
            aReleaseTimeSlider.get()
        }
    );
    //[/UserResized]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void JsynthAudioProcessorEditor::buttonClicked (Button* buttonThatWasClicked)
{
    if (buttonThatWasClicked->getRadioGroupId() == 1)
    {
        if (buttonThatWasClicked == toggleOscSin.get())
        {
            processor.oscGlobals.waveform = SIN;
        }
        else if (buttonThatWasClicked == toggleOscTri.get())
        {
            processor.oscGlobals.waveform = TRI;
        }
        else if (buttonThatWasClicked == toggleOscSaw.get())
        {
            processor.oscGlobals.waveform = SAWPB;
        }
        else if (buttonThatWasClicked == toggleOscPulse.get())
        {
            processor.oscGlobals.waveform = PULSEPB;
        }
    }
    else if (buttonThatWasClicked->getRadioGroupId() == 2)
    {
        //int prevType = processor.filterGlobals.filterType;

        //float t_12[4] = {1.0f, 2.0f, 1.0f, 2.0f};
        //float t_24[4] = {0.5f, 1.0f, 0.5f, 1.0f};

        if (buttonThatWasClicked == toggleLP12.get())
        {
            processor.configureFiltersToLowpass12dB();
        }
        else if (buttonThatWasClicked == toggleLP24.get())
        {
            processor.configureFiltersToLowpass24dB();
        }
        else if (buttonThatWasClicked == toggleHP12.get())
        {
            processor.configureFiltersToHighpass12dB();
        }
        else if (buttonThatWasClicked == toggleHP24.get())
        {
            processor.configureFiltersToHighpass24dB();
        }
    }
}

//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="JsynthAudioProcessorEditor"
                 componentName="" parentClasses="public AudioProcessorEditor, public ButtonListener"
                 constructorParams="JsynthAudioProcessor&amp; p" variableInitialisers="AudioProcessorEditor (p), processor (p)"
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.000"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ff0f0e0d"/>
  <GROUPCOMPONENT name="Oscillator group" id="44f4e76dcc7eb08" memberName="oscGroup"
                  virtualName="" explicitFocusOrder="0" pos="2 2 130 102" title="Osc"
                  textpos="33"/>
  <GROUPCOMPONENT name="Filter group" id="7f2f98c0982bfb88" memberName="filterGroup"
                  virtualName="" explicitFocusOrder="0" pos="130 2 164 102" title="Filter"
                  textpos="33"/>
  <GROUPCOMPONENT name="Amp group" id="54183b98e8c577cb" memberName="ampGroup"
                  virtualName="" explicitFocusOrder="0" pos="292 2 136 102" title="Amp"
                  textpos="33"/>
  <TOGGLEBUTTON name="LP12" id="d37e930928182b2" memberName="toggleLP12" virtualName=""
                explicitFocusOrder="0" pos="140 20 44 20" buttonText="LP12" connectedEdges="0"
                needsCallback="0" radioGroupId="2" state="1"/>
  <TOGGLEBUTTON name="LP24" id="69a2025031f5b2fb" memberName="toggleLP24" virtualName=""
                explicitFocusOrder="0" pos="140 37 44 20" buttonText="LP24" connectedEdges="0"
                needsCallback="0" radioGroupId="2" state="0"/>
  <TOGGLEBUTTON name="HP12" id="b478dae8879bea05" memberName="toggleHP12" virtualName=""
                explicitFocusOrder="0" pos="140 54 44 20" buttonText="HP12" connectedEdges="0"
                needsCallback="0" radioGroupId="2" state="0"/>
  <TOGGLEBUTTON name="HP24" id="ca2381452150f577" memberName="toggleHP24" virtualName=""
                explicitFocusOrder="0" pos="140 71 44 20" buttonText="HP24" connectedEdges="0"
                needsCallback="0" radioGroupId="2" state="0"/>
  <TOGGLEBUTTON name="sin" id="577bcc459c2dc8bb" memberName="toggleOscSin" virtualName=""
                explicitFocusOrder="0" pos="12 20 44 20" buttonText="sin" connectedEdges="8"
                needsCallback="0" radioGroupId="1" state="1"/>
  <TOGGLEBUTTON name="tri" id="f154aa69ab27f67a" memberName="toggleOscTri" virtualName=""
                explicitFocusOrder="0" pos="12 37 44 20" buttonText="tri" connectedEdges="12"
                needsCallback="0" radioGroupId="1" state="0"/>
  <TOGGLEBUTTON name="pulse" id="f021bafdc6ce9906" memberName="toggleOscPulse"
                virtualName="" explicitFocusOrder="0" pos="12 54 44 20" buttonText="pulse"
                connectedEdges="12" needsCallback="0" radioGroupId="1" state="0"/>
  <TOGGLEBUTTON name="saw" id="10ee2430662ea4d4" memberName="toggleOscSaw" virtualName=""
                explicitFocusOrder="0" pos="12 71 44 20" buttonText="saw" connectedEdges="4"
                needsCallback="0" radioGroupId="1" state="0"/>
  <GROUPCOMPONENT name="AEG" id="4751892e474a8dc3" memberName="ampEgGroup" virtualName=""
                  explicitFocusOrder="0" pos="292 103 136 112" title="AEG" textpos="33"/>
  <GROUPCOMPONENT name="FEG" id="eace83f332f63638" memberName="filterEgGroup" virtualName=""
                  explicitFocusOrder="0" pos="130 103 164 112" title="FEG" textpos="33"/>
  <GROUPCOMPONENT name="PEG" id="86895670a6c548f2" memberName="pitchEgGroup" virtualName=""
                  explicitFocusOrder="0" pos="2 103 130 112" title="PEG" textpos="33"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]

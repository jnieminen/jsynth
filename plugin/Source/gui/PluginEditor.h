/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.3.2

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

#pragma once

//[Headers]     -- You can add your own extra header files here --
#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "ParameterSlider.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class JsynthAudioProcessorEditor  : public AudioProcessorEditor,
                                    public Button::Listener
{
public:
    //==============================================================================
    JsynthAudioProcessorEditor (JsynthAudioProcessor& p);
    ~JsynthAudioProcessorEditor();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    void buttonClicked (Button* buttonThatWasClicked) override;
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;



private:
    //[UserVariables]   -- You can add your own custom variables in this section.

    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    JsynthAudioProcessor& processor;

    // Pointers to Slider objects
    std::unique_ptr<ParameterSlider> aAttackTimeSlider;
    std::unique_ptr<ParameterSlider> aDecayTimeSlider;
    std::unique_ptr<ParameterSlider> aSustainLevelSlider;
    std::unique_ptr<ParameterSlider> aReleaseTimeSlider;
    std::unique_ptr<ParameterSlider> filterCutoffSlider;
    std::unique_ptr<ParameterSlider> filterResonanceSlider;
    std::unique_ptr<ParameterSlider> filterEnvDepthSlider;
    std::unique_ptr<ParameterSlider> fAttackTimeSlider;
    std::unique_ptr<ParameterSlider> fDecayTimeSlider;
    std::unique_ptr<ParameterSlider> fDecayLevelSlider;
    std::unique_ptr<ParameterSlider> fSustainLevelSlider;
    std::unique_ptr<ParameterSlider> fReleaseTimeSlider;
    std::unique_ptr<ParameterSlider> GainSlider;
    std::unique_ptr<ParameterSlider> VelocityDepthSlider;
    std::unique_ptr<ParameterSlider> PanSlider;
    std::unique_ptr<Label> aegAttackLabel;
    std::unique_ptr<Label> aegDecayLabel;
    std::unique_ptr<Label> aegSustainLabel;
    std::unique_ptr<Label> aegReleaseLabel;
    std::unique_ptr<Label> cutoffLabel;
    std::unique_ptr<Label> resonanceLabel;
    std::unique_ptr<Label> filterEnvDepthLabel;
    std::unique_ptr<Label> fegAttackLabel;
    std::unique_ptr<Label> fegDecayLabel;
    std::unique_ptr<Label> fegDecayLevelLabel;
    std::unique_ptr<Label> fegSustainLabel;
    std::unique_ptr<Label> fegReleaseLabel;
    std::unique_ptr<Label> GainLabel;
    std::unique_ptr<Label> VelocityDepthLabel;
    std::unique_ptr<Label> PanLabel;

    void resize_a_set_of_rotary_sliders(
        unsigned int x, unsigned int y, std::vector<ParameterSlider*> sliders) const;
    void resize_a_set_of_vertical_sliders(
        unsigned int x, unsigned int y, std::vector<ParameterSlider*> sliders) const;
    //[/UserVariables]

    //==============================================================================
    std::unique_ptr<GroupComponent> oscGroup;
    std::unique_ptr<GroupComponent> filterGroup;
    std::unique_ptr<GroupComponent> ampGroup;
    std::unique_ptr<ToggleButton> toggleLP12;
    std::unique_ptr<ToggleButton> toggleLP24;
    std::unique_ptr<ToggleButton> toggleHP12;
    std::unique_ptr<ToggleButton> toggleHP24;
    std::unique_ptr<ToggleButton> toggleOscSin;
    std::unique_ptr<ToggleButton> toggleOscTri;
    std::unique_ptr<ToggleButton> toggleOscPulse;
    std::unique_ptr<ToggleButton> toggleOscSaw;
    std::unique_ptr<GroupComponent> ampEgGroup;
    std::unique_ptr<GroupComponent> filterEgGroup;
    std::unique_ptr<GroupComponent> pitchEgGroup;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (JsynthAudioProcessorEditor)
};

//[EndFile] You can add extra defines here...

typedef struct
{
    const uint16_t width = 42;
    const uint16_t height = 40;
    const uint16_t distance_between_sliders = 30;
} rotary_slider_measures;

typedef struct
{
    const uint16_t width = 24;
    const uint16_t height = 80;
} vertical_slider_measures;

//[/EndFile]

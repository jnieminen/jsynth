version 0.1
    - Initial alpha version
    - Oscillators
        -- sine, triangle, saw and pulse waves
        -- saw and pulse are aliasing reduced using 2nd order polyBLEP

    - Filter
        -- Lowpass 12dB and 24dB
        -- resonance

    - Amplitude envelope generator
        -- ADSR
        -- nonlinear curves using one-pole lowpass filter and overshoot.

    - Filter envelope generator
        -- AD1D2SR
        -- exponential curves to compensate logarithmic perception of frequency
           differences

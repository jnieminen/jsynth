Start Projucer
Choose "new Audio plugin"
Choose the folder and project name
    - Remember to check that the JUCE modules path is right!
Project Settings
    - Project type audio plug-in
    - Both VST and VST3 have to be ticked before the export settings show the box for the VST3_SDK folder!? Even stranger, it only shows it for the Makefile option, not for Code::Blocks.
    - Plugin Channel Configurations
        * for a stereo effect: {2,2}
        * for a stereo synth: {0,2}
    - Plugin wants MIDI input/produces output
    - For a synth: plugin is a synth

Download VST3 SDK from Steinberg
    - Set VST3 SDK path in Projucer: Tools -> Global preferences
        * Replace spaces in file names with undercores etc.
        * Also, '~' characters don't work!

Compile the Audio Plugin Host
    - Open the Plugin Host project in Projucer
    - Re-enter the VST3_SDK folder: Config -> Linux Makefile
    - save the project
    - make

You can also follow the tutorial

https://www.juce.com/doc/tutorial_create_projucer_basic_plugin

CodeBlocks
    - You might need to write the JUCE/modules and the VST3_SDK paths manually in Code::Blocks -> Debug/Release -> Header search paths
    - Doesn't compile. Linker fails anyway :(

Jsynth v0.1 by Jussi Nieminen

Overview:

    Jsynth is a virtual analog software synthesizer VST plug-in for Linux and
    Windows. The plug-in was developed using the JUCE (Jule's Utility Class
    Extensions) framework.

Installation:

    VST plug-ins are dynamic software libraries that implement audio effects and
    instruments. They are used in a host program, referred to as a DAW (Digital
    Audio Workstation). In order to load a VST plug-in, a DAW has a specific
    folder for storing VST plug-ins. Place the jsynth.dll file in the folder
    designated for VST plug-ins by your DAW and let the DAW scan the folder for
    new VSTs.

Licence:

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contact:

    Jussi Nieminen
    j.n.8.9@hotmail.com

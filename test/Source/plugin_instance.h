#pragma once

#include <memory>
#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"

class PluginInstance : public ::testing::Test
{
public:
    PluginInstance()
    {
        plugin = std::make_unique<JsynthAudioProcessor>();
    }

protected:
    std::unique_ptr<JsynthAudioProcessor> plugin;
};

/*
  ==============================================================================

    test_envelope_generator.cpp
    Created: 19 Dec 2018 12:55:46am
    Author:  jussi

  ==============================================================================
*/

#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "plugin_instance.h"
#include "EnvelopeGenerator.h"
#include "JsynthTables.h"

#define SAMPLE_RATE 8192
#define BUFFER_LENGTH (2 * SAMPLE_RATE)

class TestAmplitudeEnvelopeGenerator : public ::testing::Test
{
public:
    TestAmplitudeEnvelopeGenerator()
    {
        aeg_globals.overshootFactor = 1.1566f;
        aeg_globals.decayLvOvershot = aeg_globals.overshootFactor;
        aeg_globals.sustainLvOvershot = aeg_globals.overshootFactor
            * (aeg_globals.sustainLv - aeg_globals.decayLv) + aeg_globals.decayLv;
    }

protected:
    AegGlobals aeg_globals = {};
    EnvelopeGenerator aeg = EnvelopeGenerator(aeg_globals);
};

TEST_F(TestAmplitudeEnvelopeGenerator, Initial_State_Is_Off)
{
    EXPECT_EQ(EG_OFF, aeg.getState());
}

TEST_F(TestAmplitudeEnvelopeGenerator, Initial_Gain_Is_Zero)
{
    EXPECT_EQ(0.0f, aeg.getGain());
}

TEST_F(TestAmplitudeEnvelopeGenerator, Gain_Is_Zero_When_State_Is_Off)
{
    EXPECT_EQ(0.0f, aeg.calculateLevel());
    EXPECT_EQ(0.0f, aeg.calculateLevel());
}

TEST_F(TestAmplitudeEnvelopeGenerator, State_Changes_From_Off_To_Attack_At_Gate_On)
{
    aeg.gateOn();
    EXPECT_EQ(EG_ATTACK, aeg.getState());
}

TEST_F(TestAmplitudeEnvelopeGenerator, State_Changes_From_Attack_To_Decay_At_Correct_Level)
{
    float gain = aeg.getGain();

    // Using the following attack time and decay level, gain reaches decay
    // level at first sample.
    aeg_globals.decayLv = 0.5f;
    aeg_globals.a1Attack = expf(-1.0f);
    aeg_globals.b0Attack = 1.0f - aeg_globals.a1Attack;
    aeg.gateOn();
    EXPECT_EQ(EG_ATTACK, aeg.getState());
    gain = aeg.calculateLevel();
    EXPECT_EQ(EG_DECAY, aeg.getState());
}

TEST_F(TestAmplitudeEnvelopeGenerator, State_Changes_From_Decay_To_Sustain_At_Correct_Level)
{
    float gain = aeg.getGain();

    // Using the following parameters, gain reaches decay level at first sample
    // and sustain level at third sample.
    aeg_globals.decayLv   = 0.5f;
    aeg_globals.sustainLv = 0.0f;
    aeg_globals.sustainLvOvershot = aeg_globals.overshootFactor
        * (aeg_globals.sustainLv - aeg_globals.decayLv) + aeg_globals.decayLv;
    aeg_globals.a1Attack = expf(-1.0f);
    aeg_globals.b0Attack = 1.0f - aeg_globals.a1Attack;
    aeg_globals.a1Decay  = expf(-1.0f);
    aeg_globals.b0Decay  = 1.0f - aeg_globals.a1Decay;
    aeg.gateOn();

    EXPECT_EQ(EG_ATTACK, aeg.getState());
    gain = aeg.calculateLevel();
    EXPECT_EQ(EG_DECAY, aeg.getState());
    gain = aeg.calculateLevel();
    EXPECT_EQ(EG_DECAY, aeg.getState());
    gain = aeg.calculateLevel();
    EXPECT_EQ(EG_SUSTAIN, aeg.getState());
}

TEST_F(TestAmplitudeEnvelopeGenerator, State_Changes_To_Release_At_Gate_Off)
{
    aeg.gateOff();
    EXPECT_EQ(EG_RELEASE, aeg.getState());
}

TEST_F(TestAmplitudeEnvelopeGenerator, State_Changes_From_Release_To_Off_At_Negative_120_dB)
{
    float gain = aeg.getGain();

    aeg_globals.decayLv   = 0.5f;
    aeg_globals.sustainLv = 1.0f;
    aeg_globals.a1Attack  = expf(-1.0f);
    aeg_globals.b0Attack  = 1.0f - aeg_globals.a1Attack;
    aeg_globals.a1Decay   = expf(-1.0f);
    aeg_globals.b0Decay   = 1.0f - aeg_globals.a1Decay;
    aeg_globals.a1Release = exp(-1.0f);
    aeg_globals.b0Release = 1.0f - aeg_globals.a1Release;
    aeg.gateOn();
    EXPECT_EQ(EG_ATTACK, aeg.getState());
    gain = aeg.calculateLevel();
    EXPECT_EQ(EG_DECAY, aeg.getState());
    gain = aeg.calculateLevel();
    EXPECT_EQ(EG_SUSTAIN, aeg.getState());
    gain = aeg.calculateLevel();
    EXPECT_EQ(EG_SUSTAIN, aeg.getState());
    aeg.gateOff();
    EXPECT_EQ(EG_RELEASE, aeg.getState());

    for (size_t i = 0; i < 128; i++)
    {
        gain = aeg.calculateLevel();

        if (gain < 0.000001f)
        {
            EXPECT_EQ(EG_OFF, aeg.getState());
            break;
        }
        else
        {
            EXPECT_EQ(EG_RELEASE, aeg.getState());
        }
    }

    // This is a "back plate". By 128 samples gain should surely be below -120dB.
    EXPECT_EQ(EG_OFF, aeg.getState());
}

TEST_F(TestAmplitudeEnvelopeGenerator, Increasing_Gain_From_Decay_to_Sustain_Level_Does_Not_Work)
{
    aeg_globals.decayLv = 0.0f;
    aeg_globals.sustainLv = 0.5f;
    aeg_globals.a1Attack = expf(-1.0f / 2.0f);
    aeg_globals.b0Attack = 1.0f - aeg_globals.a1Attack;
    aeg_globals.a1Decay = expf(-1.0f / 2.0f);
    aeg_globals.b0Decay = 1.0f - aeg_globals.a1Decay;
    aeg_globals.a1Release = exp(-1.0f / 2.0f);
    aeg_globals.b0Release = 1.0f - aeg_globals.a1Release;
    aeg.gateOn();

    std::array<float, 4> a_gain = {0.0f, 0.5f, 0.5f, 0.5f};

    for (unsigned int i = 0; i < a_gain.size(); ++i)
    {
        aeg.calculateLevel();
        EXPECT_FLOAT_EQ(a_gain[i], aeg.getGain());
        std::cout << "g[" << i << "] = " << aeg.getGain() << std::endl;
    }
}

// TODO: Attack/Decay/Release time tests?

class TestFrequencyEnvelopeGenerator : public ::testing::Test
{
public:
    TestFrequencyEnvelopeGenerator()
    {
        feg_globals.envDepth = 1.0f;
    }

protected:
    FegGlobals feg_globals = {};
    EnvelopeGeneratorFreq feg = EnvelopeGeneratorFreq(feg_globals);
    LookupTables tables = LookupTables();
};

TEST_F(TestFrequencyEnvelopeGenerator, Initial_Gain_Equals_One)
{
    EXPECT_EQ(1.0f, feg.getGain());
}

TEST_F(TestFrequencyEnvelopeGenerator, Initial_State_Is_Off)
{
    EXPECT_EQ(EG_OFF, feg.getState());
}

TEST_F(TestFrequencyEnvelopeGenerator, Gain_Increases_In_Attack_State)
{
    unsigned int attack_time_in_samples = 4;

    feg_globals.decayLevel = 2.0f;
    feg_globals.attackTimeInv = 1.0f / (float)attack_time_in_samples;
    feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
    std::cout << "Attack time: " << 1.0f / feg_globals.attackTimeInv << std::endl;
    feg.gateOn();

    for (unsigned int i = 0; i < attack_time_in_samples - 1; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i << "] = " << feg.getGain() << std::endl;
        EXPECT_GT(feg_globals.decayLevel, feg.getGain());
    }

    feg.calculateLevel();
    std::cout << "g[" << attack_time_in_samples - 1 << "] = " << feg.getGain() << std::endl;
    EXPECT_FLOAT_EQ(feg_globals.decayLevel, feg.getGain());
}

TEST_F(TestFrequencyEnvelopeGenerator, Gain_Decreases_In_Attack_State)
{
    unsigned int attack_time_in_samples = 3;

    feg_globals.decayLevel = 0.5f;
    feg_globals.attackTimeInv = 1.0f / (float)attack_time_in_samples;
    feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
    std::cout << "Attack time: " << 1.0f / feg_globals.attackTimeInv << std::endl;
    feg.gateOn();

    for (unsigned int i = 0; i < attack_time_in_samples - 1; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i << "] = " << feg.getGain() << std::endl;
        EXPECT_LT(feg_globals.decayLevel, feg.getGain());
    }

    feg.calculateLevel();
    std::cout << "g[" << attack_time_in_samples - 1 << "] = " << feg.getGain() << std::endl;
    EXPECT_FLOAT_EQ(feg_globals.decayLevel, feg.getGain());
}

TEST_F(TestFrequencyEnvelopeGenerator, Gain_Increases_In_Decay_State)
{
    const unsigned int attack_time_in_samples = 1;
    const unsigned int decay_time_in_samples  = 3;

    feg_globals.decayLevel    = 2.0f;
    feg_globals.sustainLevel  = 2.0f;

    const float expected_sustain_level = feg_globals.decayLevel
        * feg_globals.sustainLevel;

    feg_globals.attackTimeInv = 1.0f / (float)attack_time_in_samples;
    feg_globals.decayTimeInv  = 1.0f / (float)decay_time_in_samples;
    feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
    feg_globals.a1Decay  = pow(feg_globals.sustainLevel, feg_globals.decayTimeInv);

    std::cout << "Decay time: " << 1.0f / feg_globals.attackTimeInv << std::endl;

    feg.gateOn();

    for (unsigned int i = 0; i < attack_time_in_samples; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i << "] = " << feg.getGain() << std::endl;
    }

    for (unsigned int i = 0; i < decay_time_in_samples - 1; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i + attack_time_in_samples << "] = " << feg.getGain() << std::endl;
        EXPECT_GT(expected_sustain_level, feg.getGain());
    }

    feg.calculateLevel();
    std::cout << "g[" << attack_time_in_samples + decay_time_in_samples - 1
        << "] = " << feg.getGain() << std::endl;
    EXPECT_FLOAT_EQ(expected_sustain_level, feg.getGain());
}

TEST_F(TestFrequencyEnvelopeGenerator, Gain_Decreases_In_Decay_State)
{
    const unsigned int attack_time_in_samples = 1;
    const unsigned int decay_time_in_samples = 2;

    feg_globals.decayLevel = 2.0f;
    feg_globals.sustainLevel = 0.5f;

    const float expected_sustain_level = feg_globals.decayLevel
        * feg_globals.sustainLevel;

    feg_globals.attackTimeInv = 1.0f / (float)attack_time_in_samples;
    feg_globals.decayTimeInv = 1.0f / (float)decay_time_in_samples;
    feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
    feg_globals.a1Decay = pow(feg_globals.sustainLevel, feg_globals.decayTimeInv);

    std::cout << "Decay time: " << 1.0f / feg_globals.attackTimeInv << std::endl;

    feg.gateOn();

    for (unsigned int i = 0; i < attack_time_in_samples; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i << "] = " << feg.getGain() << std::endl;
    }

    for (unsigned int i = 0; i < decay_time_in_samples - 1; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i + attack_time_in_samples << "] = " << feg.getGain() << std::endl;
        EXPECT_LT(expected_sustain_level, feg.getGain());
    }

    feg.calculateLevel();
    std::cout << "g[" << attack_time_in_samples + decay_time_in_samples - 1
        << "] = " << feg.getGain() << std::endl;
    EXPECT_FLOAT_EQ(expected_sustain_level, feg.getGain());
}

TEST_F(TestFrequencyEnvelopeGenerator, Gain_Increases_In_Release_State)
{
    const unsigned int attack_time_in_samples  = 1;
    const unsigned int decay_time_in_samples   = 1;
    const unsigned int release_time_in_samples = 2;

    feg_globals.decayLevel   = 1.0f;
    feg_globals.sustainLevel = 0.5f;

    const float expected_release_level = 1.0f;

    feg_globals.attackTimeInv  = 1.0f / (float)attack_time_in_samples;
    feg_globals.decayTimeInv   = 1.0f / (float)decay_time_in_samples;
    feg_globals.releaseTimeInv = 1.0f / (float)release_time_in_samples;
    feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
    feg_globals.a1Decay = pow(feg_globals.sustainLevel, feg_globals.decayTimeInv);

    std::cout << "Release time: " << 1.0f / feg_globals.attackTimeInv << std::endl;

    feg.gateOn();

    for (unsigned int i = 0; i < attack_time_in_samples + decay_time_in_samples; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i << "] = " << feg.getGain() << std::endl;
    }

    feg.gateOff();

    for (unsigned int i = 0; i < release_time_in_samples - 1; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i + attack_time_in_samples << "] = " << feg.getGain() << std::endl;
        EXPECT_GT(expected_release_level, feg.getGain());
    }

    feg.calculateLevel();
    std::cout << "g[" << attack_time_in_samples + decay_time_in_samples - 1
        << "] = " << feg.getGain() << std::endl;
    EXPECT_FLOAT_EQ(expected_release_level, feg.getGain());
}

TEST_F(TestFrequencyEnvelopeGenerator, Gain_Decreases_In_Release_State)
{
    const unsigned int attack_time_in_samples = 1;
    const unsigned int decay_time_in_samples = 1;
    const unsigned int release_time_in_samples = 3;

    feg_globals.decayLevel = 2.0f;
    feg_globals.sustainLevel = 1.0f;

    const float expected_release_level = 1.0f;

    feg_globals.attackTimeInv  = 1.0f / (float)attack_time_in_samples;
    feg_globals.decayTimeInv   = 1.0f / (float)decay_time_in_samples;
    feg_globals.releaseTimeInv = 1.0f / (float)release_time_in_samples;
    feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
    feg_globals.a1Decay  = pow(feg_globals.sustainLevel, feg_globals.decayTimeInv);

    std::cout << "Release time: " << 1.0f / feg_globals.attackTimeInv << std::endl;

    feg.gateOn();

    for (unsigned int i = 0; i < attack_time_in_samples + decay_time_in_samples; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i << "] = " << feg.getGain() << std::endl;
    }

    feg.gateOff();

    for (unsigned int i = 0; i < release_time_in_samples - 1; ++i)
    {
        feg.calculateLevel();
        std::cout << "g[" << i + attack_time_in_samples << "] = " << feg.getGain() << std::endl;
        EXPECT_LT(expected_release_level, feg.getGain());
    }

    feg.calculateLevel();
    std::cout << "g[" << attack_time_in_samples + decay_time_in_samples - 1
        << "] = " << feg.getGain() << std::endl;
    EXPECT_FLOAT_EQ(expected_release_level, feg.getGain());
}

TEST_F(TestFrequencyEnvelopeGenerator, Attack_Time_Is_Consistent)
{
    unsigned int expected_attack_time = 8;

    std::array<float, 3> decay_levels = {0.5f, 0.9f, 234.0f};
    std::vector<unsigned int> actual_attack_times;

    for (float& decay_level : decay_levels)
    {
        feg_globals.decayLevel = decay_level;
        feg_globals.attackTimeInv = 1.0f / (float)expected_attack_time;
        feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
        std::cout << "Attack time: " << 1.0f / feg_globals.attackTimeInv << std::endl;
        feg.gateOn();

        for (unsigned int i = 0; i < 256; ++i)
        {
            feg.calculateLevel();
            std::cout << "g[" << i << "] = " << feg.getGain()  << std::endl;

            if (feg.getState() == EG_DECAY)
            {
                actual_attack_times.push_back(i + 1);
                break;
            }
        }
    }

    for (unsigned int& actual_attack_time : actual_attack_times)
        // Allow one sample overflow due to floating point error.
        EXPECT_TRUE(expected_attack_time == actual_attack_time
            || expected_attack_time + 1 == actual_attack_time
        );
}

TEST_F(TestFrequencyEnvelopeGenerator, Decay_Time_Is_Consistent)
{
    unsigned int attack_time = 1;
    unsigned int expected_decay_time = 8;

    std::array<float, 3> sustain_levels = {0.002f, 550.0f, 2.0f};
    std::vector<unsigned int> actual_decay_times;

    for (float& sustain_level : sustain_levels)
    {
        feg_globals.decayLevel    = 1.0f;
        feg_globals.sustainLevel  = sustain_level;
        feg_globals.attackTimeInv = 1.0f / (float)attack_time;
        feg_globals.decayTimeInv  = 1.0f / (float)expected_decay_time;
        feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
        feg_globals.a1Decay  = pow(feg_globals.sustainLevel, feg_globals.decayTimeInv);
        std::cout << "Decay time: " << 1.0f / feg_globals.decayTimeInv << std::endl;
        feg.gateOn();

        for (unsigned int i = 0; i < 256; ++i)
        {
            feg.calculateLevel();
            std::cout << "g[" << i << "] = " << feg.getGain() << std::endl;

            if (feg.getState() == EG_SUSTAIN)
            {
                actual_decay_times.push_back(i - attack_time + 1);
                break;
            }
        }
    }

    for (unsigned int& actual_decay_time : actual_decay_times)
        // Allow one sample overflow due to floating point error.
        EXPECT_TRUE(expected_decay_time == actual_decay_time
            || expected_decay_time + 1 == actual_decay_time
        );
}

TEST_F(TestFrequencyEnvelopeGenerator, Release_Time_Is_Consistent)
{
    unsigned int attack_time = 1;
    unsigned int decay_time = 1;
    unsigned int expected_release_time = 5;

    std::array<float, 3> sustain_levels = {0.002f, 550.0f, 2.0f};
    std::vector<unsigned int> actual_release_times;

    for (float& sustain_level : sustain_levels)
    {
        feg_globals.decayLevel     = 1.0f;
        feg_globals.sustainLevel   = sustain_level;
        feg_globals.attackTimeInv  = 1.0f / (float)attack_time;
        feg_globals.decayTimeInv   = 1.0f / (float)decay_time;
        feg_globals.releaseTimeInv = 1.0f / (float)expected_release_time;
        feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
        feg_globals.a1Decay  = pow(feg_globals.sustainLevel, feg_globals.decayTimeInv);
        std::cout << "Release time: " << 1.0f / feg_globals.releaseTimeInv << std::endl;
        feg.gateOn();

        for (unsigned int i = 0; i < 256; ++i)
        {
            feg.calculateLevel();
            std::cout << "g[" << i << "] = " << feg.getGain() << std::endl;

            if (feg.getState() == EG_OFF)
            {
                actual_release_times.push_back(i - attack_time - decay_time + 1);
                break;
            }
            else if (feg.getState() == EG_SUSTAIN)
            {
                feg.gateOff();
            }
        }
    }

    for (unsigned int& actual_release_time : actual_release_times)
        // Allow one sample overflow due to floating point error.
        EXPECT_TRUE(expected_release_time == actual_release_time
            || expected_release_time + 1 == actual_release_time
        );
}

TEST_F(TestFrequencyEnvelopeGenerator, Attack_Slope_Is_Exponential)
{
    const size_t attack_time_in_samples = 8;

    feg_globals.decayLevel = 8.0f;
    feg_globals.attackTimeInv = 1.0f / (float)attack_time_in_samples;
    feg_globals.a1Attack = pow(feg_globals.decayLevel, feg_globals.attackTimeInv);
    feg.gateOn();

    for (unsigned int i = 0; i < attack_time_in_samples; ++i)
    {
        feg.calculateLevel();
        EXPECT_FLOAT_EQ(powf(feg_globals.a1Attack, (float)(i+1)), feg.getGain());
    }
}

// Todo: Slope test for decay and release

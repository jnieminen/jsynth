/*
  ==============================================================================

    test_filter.cpp
    Created: 11 Nov 2018 8:36:40pm
    Author:  jussi

  ==============================================================================
*/

#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "plugin_instance.h"
#include "utilities.h"
#include "JsynthFilters.h"

#define SAMPLE_RATE 8192.0f
#define BUFFER_LENGTH (2*8192)
#define SIX_DB 6.0205999f

size_t index_of_frequency_in_buffer(const float cutoff)
{
    // juce::fft::performFrequencyOnlyForwardTransform populates only half
    // of the input array. Therefore divide extra by 2.
    return (size_t)ceilf((float)BUFFER_LENGTH / SAMPLE_RATE * cutoff / 2.0f);
}

class TestFilter : public ::testing::TestWithParam<float>
{
public:
    TestFilter()
    {
        const float min_cutoff = 4.0f;
        const float max_cutoff = 4096.0f;
        filter_globals.piOverFs = (float_Pi / SAMPLE_RATE);
        filter_globals.minG = filter_globals.piOverFs * min_cutoff;
        filter_globals.maxG = filter_globals.piOverFs * max_cutoff;
        filter_globals.g = filter_globals.minG;
    }

    virtual void SetUp()
    {
        filter_globals.g = filter_globals.piOverFs * GetParam();
        sample_buffer.fill(0.0f);
        sample_buffer.front() = 1.0f;
    }

    void processBuffer()
    {
        for (float& sample : sample_buffer)
            sample = filter.processSample(sample, 1.0f, 1.0f);
    }

    void magnitude_response_in_db()
    {
        fft.performFrequencyOnlyForwardTransform(sample_buffer.data());

        for (float& sample : sample_buffer)
            sample = 20.0f * log10f(sample);
    }

protected:
    FilterGlobals filter_globals = {};
    JsynthFilter filter = JsynthFilter(filter_globals);
    juce::dsp::FFT fft = {juce::roundToInt(std::log2f(BUFFER_LENGTH)) - 1};
    std::array<float, BUFFER_LENGTH> sample_buffer;
    size_t index_of_evaluated_value = 0;
    const float threshold = 0.5f;
};

class LP12_Filter_Cutoff_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_lowpass_12dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        index_of_evaluated_value = index_of_frequency_in_buffer(GetParam());
    }
protected:
    const float expected_dB = -SIX_DB;
};

class LP24_Filter_Cutoff_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_lowpass_24dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        index_of_evaluated_value = index_of_frequency_in_buffer(GetParam());
    }
protected:
    const float expected_dB = -2 * SIX_DB;
};

class HP12_Filter_Cutoff_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_highpass_12dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        index_of_evaluated_value = index_of_frequency_in_buffer(GetParam());
    }
protected:
    const float expected_dB = -SIX_DB;
};

class HP24_Filter_Cutoff_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_highpass_24dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        index_of_evaluated_value = index_of_frequency_in_buffer(GetParam());
    }
protected:
    const float expected_dB = -2 * SIX_DB;
};

class BP12_Filter_Cutoff_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_bandpass_12dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        index_of_evaluated_value = index_of_frequency_in_buffer(GetParam());
    }
protected:
    const float expected_dB = -SIX_DB;
};

TEST_P(LP12_Filter_Cutoff_Attenuation, is_roughly_6dB)
{
    EXPECT_NEAR(expected_dB, sample_buffer.at(index_of_evaluated_value), threshold);
}

TEST_P(LP24_Filter_Cutoff_Attenuation, is_roughly_12dB)
{
    EXPECT_NEAR(expected_dB, sample_buffer.at(index_of_evaluated_value), threshold);
}

TEST_P(HP12_Filter_Cutoff_Attenuation, is_roughly_6dB)
{
    EXPECT_NEAR(expected_dB, sample_buffer.at(index_of_evaluated_value), threshold);
}

TEST_P(HP24_Filter_Cutoff_Attenuation, is_roughly_12dB)
{
    EXPECT_NEAR(expected_dB, sample_buffer.at(index_of_evaluated_value), threshold);
}

TEST_P(BP12_Filter_Cutoff_Attenuation, is_roughly_6dB)
{
    EXPECT_NEAR(expected_dB, sample_buffer.at(index_of_evaluated_value), threshold);
    size_t hp_index = (size_t)SAMPLE_RATE / 2 - index_of_evaluated_value;
    EXPECT_NEAR(expected_dB, sample_buffer.at(hp_index), threshold);
}

const std::array<float, 10> cutoff_frequencies = {
    SAMPLE_RATE / 1024,
    SAMPLE_RATE / 512,
    SAMPLE_RATE / 256,
    SAMPLE_RATE / 128,
    SAMPLE_RATE / 64,
    SAMPLE_RATE / 32,
    SAMPLE_RATE / 16,
    SAMPLE_RATE / 8,
    SAMPLE_RATE / 4,
    SAMPLE_RATE / 2 - 128
};

INSTANTIATE_TEST_CASE_P(Parametrized, LP12_Filter_Cutoff_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies)
);

INSTANTIATE_TEST_CASE_P(Parametrized, LP24_Filter_Cutoff_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies)
);

INSTANTIATE_TEST_CASE_P(Parametrized, HP12_Filter_Cutoff_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies)
);

INSTANTIATE_TEST_CASE_P(Parametrized, HP24_Filter_Cutoff_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies)
);

INSTANTIATE_TEST_CASE_P(Parametrized, BP12_Filter_Cutoff_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies.begin(), cutoff_frequencies.end() - 2)
);

//------------------------ Octave attenuation ---------------------------------

class LP12_Filter_Octave_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_lowpass_12dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        const float cutoff_plus_octave = 2.0f * GetParam();
        index_of_evaluated_value = index_of_frequency_in_buffer(cutoff_plus_octave);
    }
protected:
    const float expected_dB = -2 * SIX_DB;
};

class LP24_Filter_Octave_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_lowpass_24dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        const float cutoff_plus_octave = 2.0f * GetParam();
        index_of_evaluated_value = index_of_frequency_in_buffer(cutoff_plus_octave);
    }
protected:
    const float expected_dB = -4 * SIX_DB;
};

class HP12_Filter_Octave_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_highpass_12dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        const float cutoff_minus_octave = GetParam() / 2.0f;
        index_of_evaluated_value = index_of_frequency_in_buffer(cutoff_minus_octave);
    }
protected:
    const float expected_dB = -2 * SIX_DB;
};

class HP24_Filter_Octave_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_highpass_24dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        const float cutoff_minus_octave = GetParam() / 2.0f;
        index_of_evaluated_value = index_of_frequency_in_buffer(cutoff_minus_octave);
    }
protected:
    const float expected_dB = -4 * SIX_DB;
};

TEST_P(LP12_Filter_Octave_Attenuation, is_12dB_or_more)
{
    EXPECT_GE(expected_dB + threshold, sample_buffer.at(index_of_evaluated_value));
}

TEST_P(LP24_Filter_Octave_Attenuation, is_24dB_or_more)
{
    EXPECT_GE(expected_dB + threshold, sample_buffer.at(index_of_evaluated_value));
}

TEST_P(HP12_Filter_Octave_Attenuation, is_12dB_or_more)
{
    EXPECT_GE(expected_dB + threshold, sample_buffer.at(index_of_evaluated_value));
}

TEST_P(HP24_Filter_Octave_Attenuation, is_24dB_or_more)
{
    EXPECT_GE(expected_dB + threshold, sample_buffer.at(index_of_evaluated_value));
}

const std::array<float, 10> cutoff_frequencies_for_octave_test = {
    SAMPLE_RATE / 2048,
    SAMPLE_RATE / 1024,
    SAMPLE_RATE / 512,
    SAMPLE_RATE / 256,
    SAMPLE_RATE / 128,
    SAMPLE_RATE / 64,
    SAMPLE_RATE / 32,
    SAMPLE_RATE / 16,
    SAMPLE_RATE / 8,
    SAMPLE_RATE / 4
};

INSTANTIATE_TEST_CASE_P(Parametrized, LP12_Filter_Octave_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies_for_octave_test)
);

INSTANTIATE_TEST_CASE_P(Parametrized, LP24_Filter_Octave_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies_for_octave_test)
);

INSTANTIATE_TEST_CASE_P(Parametrized, HP12_Filter_Octave_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies_for_octave_test)
);

INSTANTIATE_TEST_CASE_P(Parametrized, HP24_Filter_Octave_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies_for_octave_test)
);

//------------------------ Decade attenuation ---------------------------------

class LP12_Filter_Decade_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_lowpass_12dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        const float cutoff_plus_decade = 10.0f * GetParam();
        index_of_evaluated_value = index_of_frequency_in_buffer(cutoff_plus_decade);
    }
protected:
    const float expected_dB = -40.0f;
};

class LP24_Filter_Decade_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_lowpass_24dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        const float cutoff_plus_decade = 10.0f * GetParam();
        index_of_evaluated_value = index_of_frequency_in_buffer(cutoff_plus_decade);
    }
protected:
    const float expected_dB = -80.0f;
};

class HP12_Filter_Decade_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_highpass_12dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        const float cutoff_minus_decade = GetParam() / 10.0f;
        index_of_evaluated_value = index_of_frequency_in_buffer(cutoff_minus_decade);
    }
protected:
    const float expected_dB = -40.0f;
};

class HP24_Filter_Decade_Attenuation : public TestFilter
{
    virtual void SetUp()
    {
        filter.configure_highpass_24dB();
        TestFilter::SetUp();
        processBuffer();
        magnitude_response_in_db();
        const float cutoff_minus_decade = GetParam() / 10.0f;
        index_of_evaluated_value = index_of_frequency_in_buffer(cutoff_minus_decade);
    }
protected:
    const float expected_dB = -80.0f;
};

TEST_P(LP12_Filter_Decade_Attenuation, is_40dB_more)
{
    EXPECT_GE(expected_dB + threshold, sample_buffer.at(index_of_evaluated_value));
}

TEST_P(LP24_Filter_Decade_Attenuation, is_80dB_or_more)
{
    EXPECT_GE(expected_dB + threshold, sample_buffer.at(index_of_evaluated_value));
}

TEST_P(HP12_Filter_Decade_Attenuation, is_40dB_more)
{
    EXPECT_GE(expected_dB + threshold, sample_buffer.at(index_of_evaluated_value));
}

TEST_P(HP24_Filter_Decade_Attenuation, is_80dB_or_more)
{
    EXPECT_GE(expected_dB + threshold, sample_buffer.at(index_of_evaluated_value));
}

const std::array<float, 5> cutoff_frequencies_for_decade_test = {
    40,
    80,
    160,
    240,
    409,
};

INSTANTIATE_TEST_CASE_P(Parametrized, LP12_Filter_Decade_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies_for_decade_test)
);

INSTANTIATE_TEST_CASE_P(Parametrized, LP24_Filter_Decade_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies_for_decade_test)
);

INSTANTIATE_TEST_CASE_P(Parametrized, HP12_Filter_Decade_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies_for_decade_test)
);

INSTANTIATE_TEST_CASE_P(Parametrized, HP24_Filter_Decade_Attenuation,
    ::testing::ValuesIn(cutoff_frequencies_for_decade_test)
);


// Same with resonance?
class TestFilterResonance : public ::testing::Test
{
public:
    TestFilterResonance()
    {
        const float min_cutoff = 4.0f;
        const float max_cutoff = 4096.0f;
        filter_globals.piOverFs = (float_Pi / SAMPLE_RATE);
        filter_globals.minG = filter_globals.piOverFs * min_cutoff;
        filter_globals.maxG = filter_globals.piOverFs * max_cutoff;
        filter_globals.g = filter_globals.minG;
    }

    virtual void SetUp()
    {
        sample_buffer.fill(0.0f);
        sample_buffer.front() = 1.0f;
    }

    void processBuffer()
    {
        for (float& sample : sample_buffer)
            sample = filter.processSample(sample, 1.0f, 1.0f);
    }

protected:
    FilterGlobals filter_globals = {};
    JsynthFilter filter = JsynthFilter(filter_globals);
    std::array<float, BUFFER_LENGTH> sample_buffer;
    juce::dsp::FFT fft = {juce::roundToInt(std::log2f(BUFFER_LENGTH)) - 1};
};

TEST_F(TestFilterResonance, Value_99_Does_Not_Cause_Clipping)
{
    filter_globals.g = filter_globals.piOverFs * 1000.0f;
    filter_globals.res = 4.0f * 0.99f;
    processBuffer();

    for (float& sample : sample_buffer)
    {
        ASSERT_GE(1.0f, sample);
    }
}

// The following tests verify that there is a boost in the magnitude response
// between the cutoff frequency and octave below (or above in highpass case)
// the cutoff frequency. Explicit dB values are not evaluated.
TEST_F(TestFilterResonance, Boost_around_the_cutoff_frequency_LP12)
{
    const float cutoff = 1000.0f;
    filter_globals.g = filter_globals.piOverFs * cutoff;
    filter_globals.res = 2.0f * 0.7f;
    filter.configure_lowpass_12dB();
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    size_t i_cutoff = (size_t)cutoff;

    EXPECT_LT(sample_buffer[i_cutoff / 2], sample_buffer[i_cutoff * 3 / 4]);
}

TEST_F(TestFilterResonance, Boost_around_the_cutoff_frequency_LP24)
{
    const float cutoff = 1000.0f;
    filter_globals.g = filter_globals.piOverFs * cutoff;
    filter_globals.res = 4.0f * 0.5f;
    filter.configure_lowpass_24dB();
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    size_t i_cutoff = (size_t)cutoff;

    EXPECT_LT(sample_buffer[i_cutoff / 2], sample_buffer[i_cutoff * 3 / 4]);
}

TEST_F(TestFilterResonance, Boost_around_the_cutoff_frequency_HP12)
{
    const float cutoff = 1000.0f;
    filter_globals.g = filter_globals.piOverFs * cutoff;
    filter_globals.res = 2.0f * 0.7f;
    filter.configure_highpass_12dB();
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    size_t i_cutoff = (size_t)cutoff;

    EXPECT_LT(sample_buffer[i_cutoff * 2], sample_buffer[i_cutoff * 3 / 2]);
}

TEST_F(TestFilterResonance, Boost_around_the_cutoff_frequency_HP24)
{
    const float cutoff = 1000.0f;
    filter_globals.g = filter_globals.piOverFs * cutoff;
    filter_globals.res = 4.0f * 0.5f;
    filter.configure_highpass_24dB();
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    size_t i_cutoff = (size_t)cutoff;

    EXPECT_LT(sample_buffer[i_cutoff * 2], sample_buffer[i_cutoff * 3 / 2]);
}

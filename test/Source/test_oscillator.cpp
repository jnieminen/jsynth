/*
  ==============================================================================

    test_oscillator.cpp
    Created: 2 Dec 2018 6:42:39pm
    Author:  jussi

  ==============================================================================
*/

#include <algorithm>
#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "plugin_instance.h"
#include "utilities.h"
#include "JsynthOscillators.h"

#define SAMPLE_RATE 8192.0f
#define BUFFER_LENGTH (2*8192)
#define SIX_DB 6.0205999f


class TestOscillator : public ::testing::Test
{
public:
    TestOscillator()
    {
        osc_globals.waveform = SIN;
        osc_globals.pulseWidth = 0.5f;
        osc_globals.inverseSampleRate = 1.0f / SAMPLE_RATE;
    }

    virtual void SetUp()
    {
        sample_buffer.fill(0.0f);
    }

    void processBuffer()
    {
        for (float& sample : sample_buffer)
            sample = oscillator.calculateSample();
    }

protected:
    OscGlobals osc_globals = {};
    Oscillator oscillator = Oscillator(osc_globals);
    std::array<float, BUFFER_LENGTH> sample_buffer;
    juce::dsp::FFT fft = {juce::roundToInt(std::log2f(BUFFER_LENGTH)) - 1};
};


TEST_F(TestOscillator,
    Sine_Wave_Has_Max_Energy_At_Specified_Frequency_And_Zero_Energy_Otherwise)
{
    const float frequency = 128.0f;
    const unsigned int frequency_index = roundToInt(frequency);
    osc_globals.waveform = SIN;
    oscillator.initPhase();
    oscillator.setFrequency(frequency, osc_globals.inverseSampleRate);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    // Verify the frequency bin.
    EXPECT_FLOAT_EQ(sample_buffer[frequency_index], 0.0f);

    // Verify that there is no energy before the frequency bin.
    for (unsigned int i = 0; i < frequency_index; ++i)
        EXPECT_LT(sample_buffer[i], -96.0f);

    // Verify that there is no energy after the frequency bin.
    for (unsigned int i = frequency_index + 1; i > sample_buffer.size() / 4; ++i)
        EXPECT_LT(sample_buffer[i], -96.0f);
}


TEST_F(TestOscillator, The_Harmonics_Of_Trivial_Saw_Wave_Match_The_Formula)
{
    const unsigned int frequency_index = 256;
    const float fundamental_frequency = (float)frequency_index;

    osc_globals.waveform = SAW;
    oscillator.initPhase();
    oscillator.setFrequency(fundamental_frequency, osc_globals.inverseSampleRate);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    unsigned int harmonic = frequency_index;
    float expected_power = 2.0f / float_Pi;

    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (i == harmonic)
        {
            float expected_power_dB = 20.0f * log10(expected_power);
            EXPECT_FLOAT_EQ(sample_buffer[i], expected_power_dB);
            expected_power /= 2.0f;
            harmonic += frequency_index;
        }
        else
        {
            EXPECT_LT(sample_buffer[i], -96.0f);
        }
    }
}


TEST_F(TestOscillator, The_Harmonics_Of_Trivial_Square_Wave_Match_The_Formula)
{
    const unsigned int fundamental_frequency = 256;

    osc_globals.waveform = PULSE;
    oscillator.initPhase();
    oscillator.setFrequency((float)fundamental_frequency, osc_globals.inverseSampleRate);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    const unsigned int total_num_harmonics = juce::roundToInt(
        SAMPLE_RATE / fundamental_frequency / 2);
    unsigned int k = 1;

    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (i == k * fundamental_frequency)
        {
            // Pulse wave contains odd harmonics.
            float expected_power = 4.0f / (float_Pi * 2.0f * k - float_Pi);
            expected_power = 20.0f * log10(expected_power);
            EXPECT_FLOAT_EQ(sample_buffer[i], expected_power);
            k += 2;
        }
        //else if (i == (k - 1) * fundamental_frequency)
        //{
        //    // Even harmonics contain energy due to aliasing.
        //    float expected_power = 4.0f / (float_Pi * 2.0f * (total_num_harmonics - k) - 1.0f);
        //    expected_power = 20.0f * log10(expected_power);
        //    EXPECT_FLOAT_EQ(sample_buffer[i], expected_power);
        //}
        else
        {
            EXPECT_LT(sample_buffer[i], -96.0f);
        }
    }
}


TEST_F(TestOscillator, PolyBlep_Pulse_Wave_Has_Correct_Magnitude_Spectrum)
{
    const unsigned int fundamental_frequency = 128;
    osc_globals.waveform = PULSEPB;
    oscillator.initPhase();
    oscillator.setFrequency(
        (float)fundamental_frequency, osc_globals.inverseSampleRate);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    unsigned int k = 1;
    const float threshold_dB = 0.001f;

    const std::array<float, BUFFER_LENGTH / 8 / fundamental_frequency>
    expected_dB_harmonics =
    {
         2.0912f,
        -7.5073f,
        -12.058f,
        -15.155f,
        -17.579f,
        -19.636f,
        -21.486f,
        -23.228f,
        -24.937f,
        -26.678f,
        -28.528f,
        -30.586f,
        -33.009f,
        -36.107f,
        -40.657f,
        -50.256f
    };

    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (i == (2*k - 1) * fundamental_frequency)
        {
            EXPECT_NEAR(expected_dB_harmonics[k-1], sample_buffer[i], threshold_dB);
            k++;
        }
        else
        {
            EXPECT_GE(-96.0f, sample_buffer[i]);
        }
    }
}

TEST_F(TestOscillator, PolyBlep_Saw_Wave_Has_Correct_Magnitude_Spectrum)
{
    const unsigned int fundamental_frequency = 256;
    osc_globals.waveform = SAWPB;
    oscillator.initPhase();
    oscillator.setFrequency(
        (float)fundamental_frequency, osc_globals.inverseSampleRate);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    unsigned int k = 1;
    const float threshold_dB = 0.001f;

    const std::array<float, BUFFER_LENGTH / 4 / fundamental_frequency - 1>
    expected_harmonics_in_dB =
    {
        -3.9504f,
        -10.056f,
        -13.721f,
        -16.427f,
        -18.642f,
        -20.580f,
        -22.366f,
        -24.082f,
        -25.799f,
        -27.585f,
        -29.523f,
        -31.738f,
        -34.444f,
        -38.109f,
        -44.214f
    };

    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (i == k * fundamental_frequency)
        {
            EXPECT_NEAR(expected_harmonics_in_dB[k-1], sample_buffer[i], threshold_dB);
            k++;
        }
        else
        {
            EXPECT_GE(-96.0f, sample_buffer[i]);
        }
    }
}


TEST_F(TestOscillator, PolyBlep_Pulse_Wave_12_5_Percent_PW_Cancels_Out_The_Eight_Harmonic)
{
    const unsigned int fundamental_frequency = 256;
    osc_globals.waveform = PULSEPB;
    osc_globals.pulseWidth = 0.125f;
    oscillator.initPhase();
    oscillator.setFrequency(
        (float)fundamental_frequency, osc_globals.inverseSampleRate);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    unsigned int k = 0;
    const float threshold_dB = 0.001f;

    const std::map<unsigned int, float> expected_harmonics_in_dB =
    {
        {0,   3.5218f},
        {1,  -6.2730f},
        {2,  -7.0453f},
        {3,  -8.3883f},
        {4,  -10.406f},
        {5,  -13.309f},
        {6,  -17.570f},
        {7,  -24.688f},
        // -inf
        {9,  -28.122f},
        {10, -24.574f},
        {11, -24.190f},
        {12, -25.717f},
        {13, -29.111f},
        {14, -35.099f},
        {15, -46.537f}
        // -inf
    };

    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (i == k * fundamental_frequency
            && expected_harmonics_in_dB.find(k) != expected_harmonics_in_dB.end())
        {
            EXPECT_NEAR(expected_harmonics_in_dB.at(k), sample_buffer[i], threshold_dB);
        }
        else
        {
            EXPECT_GE(-96.0f, sample_buffer[i]);
        }

        if (i == k * fundamental_frequency)
            k++;
    }
}


TEST_F(TestOscillator, PolyBlep_Pulse_Wave_87_5_Percent_PW_Cancels_Out_The_Eight_Harmonic)
{
    const unsigned int fundamental_frequency = 256;
    osc_globals.waveform = PULSEPB;
    osc_globals.pulseWidth = 0.875f;
    oscillator.initPhase();
    oscillator.setFrequency(
        (float)fundamental_frequency, osc_globals.inverseSampleRate);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    unsigned int k = 0;
    const float threshold_dB = 0.001f;

    const std::map<unsigned int, float> expected_harmonics_in_dB =
    {
        {0,   3.5218f},
        {1,  -6.2730f},
        {2,  -7.0453f},
        {3,  -8.3883f},
        {4,  -10.406f},
        {5,  -13.309f},
        {6,  -17.570f},
        {7,  -24.688f},
        // -inf
        {9,  -28.122f},
        {10, -24.574f},
        {11, -24.190f},
        {12, -25.717f},
        {13, -29.111f},
        {14, -35.099f},
        {15, -46.537f}
        // -inf
    };

    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (i == k * fundamental_frequency
            && expected_harmonics_in_dB.find(k) != expected_harmonics_in_dB.end())
        {
            EXPECT_NEAR(expected_harmonics_in_dB.at(k), sample_buffer[i], threshold_dB);
        }
        else
        {
            EXPECT_GE(-96.0f, sample_buffer[i]);
        }

        if (i == k * fundamental_frequency)
            k++;
    }
}


TEST_F(TestOscillator, PolyBlep_Tri_Wave_Has_Correct_Magnitude_Spectrum)
{
    const unsigned int fundamental_frequency = 256;
    osc_globals.waveform = PULSEPB;
    osc_globals.pulseWidth = 0.875f;
    oscillator.initPhase();
    oscillator.setFrequency(
        (float)fundamental_frequency, osc_globals.inverseSampleRate);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, SAMPLE_RATE);

    unsigned int k = 0;
    const float threshold_dB = 0.001f;

    const std::map<unsigned int, float> expected_harmonics_in_dB =
    {
        {0,   3.5218f},
        {1,  -6.2730f},
        {2,  -7.0453f},
        {3,  -8.3883f},
        {4,  -10.406f},
        {5,  -13.309f},
        {6,  -17.570f},
        {7,  -24.688f},
        // -inf
        {9,  -28.122f},
        {10, -24.574f},
        {11, -24.190f},
        {12, -25.717f},
        {13, -29.111f},
        {14, -35.099f},
        {15, -46.537f}
        // -inf
    };

    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (i == k * fundamental_frequency
            && expected_harmonics_in_dB.find(k) != expected_harmonics_in_dB.end())
        {
            EXPECT_NEAR(expected_harmonics_in_dB.at(k), sample_buffer[i], threshold_dB);
            k++;
        }
        else
        {
            EXPECT_GE(-96.0f, sample_buffer[i]);
        }
    }
}

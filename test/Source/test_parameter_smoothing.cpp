#include <climits>
#include <iostream>
#include "gtest/gtest.h"
#include "Globals.h"

#if 0
typedef struct {
    float foo = 0.0f;
    float bar = 0.0f;
} ValueContainer;


class TestShockAbsorber : public ::testing::Test
{
protected:
    ShockAbsorber shock_absorber;
    ValueContainer value_container;
};

TEST_F(TestShockAbsorber, Update_does_nothing_when_cache_is_empty)
{

}

TEST_F(TestShockAbsorber, Update_pops_a_cached_value_and_copies_it_to_the_reference)
{

}

TEST_F(TestShockAbsorber, Update_pops_multiple_cached_values_and_copies_them_to_the_references)
{

}

TEST_F(TestShockAbsorber, Emptied_cache_is_removed)
{

}

TEST_F(TestShockAbsorber, Emptied_caches_are_removed)
{

}

TEST_F(TestShockAbsorber, New_entry_is_added_to_the_cache)
{

}

TEST_F(TestShockAbsorber, Old_entry_is_refreshed_in_the_cache)
{

}

TEST_F(TestShockAbsorber, Filter_is_applied)
{

}

TEST_F(TestShockAbsorber, An_existing_entry_is_found_in_cache)
{

}

TEST_F(TestShockAbsorber, A_non_existing_entry_is_not_found_in_cache)
{

}

TEST_F(TestShockAbsorber, An_entry_is_not_found_in_an_empty_cache)
{

}
#endif

class TestParamWithSmoothing : public ::testing::Test
{
protected:
    ParamWithSmoothing param;
};

TEST_F(TestParamWithSmoothing, cache_zero_initialized_by_default)
{
    EXPECT_FLOAT_EQ(0.0f, param.get());
}

TEST_F(TestParamWithSmoothing, cache_initialized_with_given_value)
{
    const float given_value = 123.0f;
    param = ParamWithSmoothing(given_value);
    EXPECT_FLOAT_EQ(given_value, param.get());
}

TEST_F(TestParamWithSmoothing, get_always_gives_the_same_value)
{
    const float given_value = 123.0f;
    param = ParamWithSmoothing(given_value);
    EXPECT_FLOAT_EQ(given_value, param.get());
    EXPECT_FLOAT_EQ(given_value, param.get());
    EXPECT_FLOAT_EQ(given_value, param.get());
}

TEST_F(TestParamWithSmoothing, ramp_size_larger_than_blocksize)
{
    const unsigned int block_size = 4u;

    // 1 ms == 5.5 samples
    ParamWithSmoothing::update_cache_size_info(5500.0, block_size);
    const unsigned int expected_ramp_size = 5u;
    const unsigned int expected_cache_size = 8u;  // 4 rounded up to next 2^n
    const float new_value = 1.0f;
    param.init_cache();
    param = new_value;
    param.update_cache();

    for (auto i = 0; i < expected_cache_size; ++i)
    {
        std::cout << param.get() << ", ";
        param.next_sample();
    }
    std::cout << "\n";

    // The 1st sample (the wrapped around 5th ramp sample)
    EXPECT_LT(param.get(), new_value);
    EXPECT_GE(param.get(), 0.0f);
    param.next_sample();

    // The three padding samples
    for (auto i = expected_ramp_size; i < expected_cache_size; ++i)
    {
        EXPECT_FLOAT_EQ(param.get(), new_value);
        param.next_sample();
    }

    // The four first ramp samples.
    for (auto i = 0; i < expected_ramp_size - 1; ++i)
    {
        EXPECT_LT(param.get(), new_value);
        EXPECT_GE(param.get(), 0.0f);
        param.next_sample();
    }
}

TEST_F(TestParamWithSmoothing, ramp_size_equal_to_blocksize)
{
    // 1 ms == 8.5 samples
    ParamWithSmoothing::update_cache_size_info(8500.0, 8u);
    const unsigned int expected_ramp_size = 8u;
    const unsigned int expected_cache_size = 8u;
    const float new_value = 64.0f;
    param.init_cache();
    param = new_value;
    param.update_cache();

    for (auto i = 0; i < expected_cache_size; ++i)
    {
        std::cout << param.get() << ", ";
        param.next_sample();
    }
    std::cout << "\n";

    for (auto i = 0; i < expected_ramp_size; ++i)
    {
        EXPECT_LT(param.get(), new_value);
        EXPECT_GE(param.get(), 0.0f);
        param.next_sample();
    }
}

TEST_F(TestParamWithSmoothing, ramp_size_less_than_blocksize)
{
    const unsigned int block_size = 17u;

    // 1 ms == 9.5 samples
    ParamWithSmoothing::update_cache_size_info(9500.0, block_size);
    const unsigned int expected_ramp_size = 9u;
    const unsigned int expected_cache_size = 32u;  // 17 rounded up to next 2^n
    const float new_value = 127.0f;
    param.init_cache();
    param = new_value;
    param.update_cache();

    for (unsigned int i = 0; i < block_size; ++i)
    {
        EXPECT_FLOAT_EQ(param.get(), new_value);
        param.next_sample();
    }

    for (unsigned int i = 0; i < expected_ramp_size; ++i)
    {
        EXPECT_LT(param.get(), new_value);
        EXPECT_GE(param.get(), 0.0f);
        param.next_sample();
    }

    const unsigned int num_remaining_indices = expected_cache_size
        - block_size - expected_ramp_size;

    for (unsigned int i = 0; i < num_remaining_indices; ++i)
    {
        EXPECT_FLOAT_EQ(param.get(), new_value);
        param.next_sample();
    }
}

TEST(Test_round_to_next_power_of_two, Next_power_of_two_of_0_is_1)
{
    EXPECT_EQ(1u, round_to_next_power_of_two(0u));
}

TEST(Test_round_to_next_power_of_two, Next_power_of_two_of_3_is_4)
{
    EXPECT_EQ(4u, round_to_next_power_of_two(3u));
}

// 2^32 = UINT_MAX + 1, therefore UINT_MAX/2 < x <= UINT_MAX  wraps around to 1.
TEST(Test_round_to_next_power_of_two, Next_power_of_two_of_2147483648_is_1)
{
    EXPECT_EQ(1u, round_to_next_power_of_two(UINT_MAX / 2 + 1));
}

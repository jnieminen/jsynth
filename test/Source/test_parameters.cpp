#include <complex>
#include <iostream>
#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "plugin_instance.h"
#include "utilities.h"


TEST_F(PluginInstance, Has_correct_amount_of_parameters)
{
    EXPECT_EQ(15, plugin->getParameters().size());
}


TEST_F(PluginInstance, AEG_Initial_Values_Are_Correct)
{
    EXPECT_FLOAT_EQ(63.0f, plugin->aegAttackParam->get());
    EXPECT_FLOAT_EQ(63.0f, plugin->aegDecayParam->get());
    EXPECT_FLOAT_EQ(63.0f, plugin->aegSustainParam->get());
    EXPECT_FLOAT_EQ(63.0f, plugin->aegReleaseParam->get());

    EXPECT_FLOAT_EQ(plugin->aegAttackParam->getDefaultValue(), plugin->aegAttackParam->getValue());
    EXPECT_FLOAT_EQ(plugin->aegAttackParam->getDefaultValue(), plugin->aegDecayParam->getValue());
    EXPECT_FLOAT_EQ(plugin->aegAttackParam->getDefaultValue(), plugin->aegDecayParam->getValue());
    EXPECT_FLOAT_EQ(plugin->aegAttackParam->getDefaultValue(), plugin->aegReleaseParam->getValue());
}


TEST_F(PluginInstance, FEG_Init_Parameters_Are_Correct)
{
    EXPECT_FLOAT_EQ(63.0f, plugin->fegAttackParam->get());
    EXPECT_FLOAT_EQ(63.0f, plugin->fegDecayParam->get());
    EXPECT_FLOAT_EQ(0.0f,  plugin->fegDecayLevelParam->get());
    EXPECT_FLOAT_EQ(0.0f,  plugin->fegSustainParam->get());
    EXPECT_FLOAT_EQ(63.0f, plugin->fegReleaseParam->get());

    EXPECT_FLOAT_EQ(plugin->fegAttackParam->getDefaultValue(), plugin->fegAttackParam->getValue());
    EXPECT_FLOAT_EQ(plugin->fegAttackParam->getDefaultValue(), plugin->fegDecayParam->getValue());
    EXPECT_FLOAT_EQ(plugin->fegAttackParam->getDefaultValue(), plugin->fegDecayParam->getValue());
    EXPECT_FLOAT_EQ(plugin->fegAttackParam->getDefaultValue(), plugin->fegReleaseParam->getValue());
}


TEST_F(PluginInstance, Filter_Init_Parameters_Are_Correct)
{
    EXPECT_FLOAT_EQ(512.0f, plugin->filterCutoffParam->get());
    EXPECT_FLOAT_EQ(0.0f, plugin->filterResonanceParam->get());
    EXPECT_FLOAT_EQ(0.0f, plugin->filterEnvDepthParam->get());

    EXPECT_FLOAT_EQ(plugin->filterCutoffParam->getDefaultValue(), plugin->filterCutoffParam->getValue());
    EXPECT_FLOAT_EQ(plugin->filterResonanceParam->getDefaultValue(), plugin->filterResonanceParam->getValue());
    EXPECT_FLOAT_EQ(plugin->filterEnvDepthParam->getDefaultValue(), plugin->filterEnvDepthParam->getValue());
}

TEST_F(PluginInstance, Amplitude_Init_Parameters_Are_Correct)
{
    EXPECT_FLOAT_EQ(1.0f, plugin->GainParam->get());
    EXPECT_FLOAT_EQ(0.5f, plugin->PanParam->get());
    EXPECT_FLOAT_EQ(1.0f, plugin->VelocityDepthParam->get());
    EXPECT_FLOAT_EQ(plugin->GainParam->getDefaultValue(), plugin->GainParam->getValue());
    EXPECT_FLOAT_EQ(plugin->PanParam->getDefaultValue(), plugin->PanParam->getValue());
    EXPECT_FLOAT_EQ(plugin->VelocityDepthParam->getDefaultValue(), plugin->VelocityDepthParam->getValue());
}

TEST_F(PluginInstance, JsynthParameter_Set_And_Get_End_Up_In_The_Original_Value)
{
    // The input value should be in the range [0, 127]. Otherwise, convertTo0to1
    // triggers an assert.
    //
    // Setting a value with operator= triggers
    // setValueNotifyingHost(convertTo0to1(new_value))
    //     setValue()
    //         value = convertFrom0to1(new_value)

    std::vector<std::pair<float, float>> param_value_pairs = {
        {0.0f,     0.0f},
        {0.001f,   0.001f},
        {126.9f,   126.9f},
        {127.0f,   127.0f},
    };

    plugin->prepareToPlay(44100, 0);

    for (auto& pair : param_value_pairs)
    {
        *plugin->aegAttackParam = pair.first;
        EXPECT_FLOAT_EQ(pair.second, plugin->aegAttackParam->get());
    }
}

TEST_F(PluginInstance, Setting_Max_Parameter_Value_Keeps_AEG_Attack_Stable)
{
    plugin->prepareToPlay(44100, 0);
    *plugin->aegAttackParam = 127.0f;
    EXPECT_GE(plugin->aegGlobals.a1Attack, 0.0f);
    EXPECT_LE(plugin->aegGlobals.a1Attack, 1.0f);
    EXPECT_GE(plugin->aegGlobals.b0Attack, 0.0f);
    EXPECT_LE(plugin->aegGlobals.b0Attack, 1.0f);
}

TEST_F(PluginInstance, Setting_Max_Parameter_Value_Keeps_AEG_Decay_Stable)
{
    plugin->prepareToPlay(44100, 0);
    *plugin->aegDecayParam = 127.0f;
    EXPECT_GE(plugin->aegGlobals.a1Decay, 0.0f);
    EXPECT_LE(plugin->aegGlobals.a1Decay, 1.0f);
    EXPECT_GE(plugin->aegGlobals.b0Decay, 0.0f);
    EXPECT_LE(plugin->aegGlobals.b0Decay, 1.0f);
}

TEST_F(PluginInstance, Setting_Max_Parameter_Value_Keeps_AEG_Release_Stable)
{
    plugin->prepareToPlay(44100, 0);
    *plugin->aegReleaseParam = 127.0f;
    EXPECT_GE(plugin->aegGlobals.a1Release, 0.0f);
    EXPECT_LE(plugin->aegGlobals.a1Release, 1.0f);
    EXPECT_GE(plugin->aegGlobals.b0Release, 0.0f);
    EXPECT_LE(plugin->aegGlobals.b0Release, 1.0f);
}

TEST_F(PluginInstance, Setting_Max_Parameter_Value_Keeps_FEG_Attack_Stable)
{
    plugin->prepareToPlay(44100, 0);
    *plugin->fegAttackParam = 127.0f;
    EXPECT_GE(plugin->fegGlobals.a1Attack, 0.0f);
    EXPECT_LE(plugin->fegGlobals.a1Attack, 1.0f);
}

TEST_F(PluginInstance, Setting_Max_Parameter_Value_Keeps_FEG_Decay_Stable)
{
    plugin->prepareToPlay(44100, 0);
    *plugin->fegDecayParam = 127.0f;
    EXPECT_GE(plugin->fegGlobals.a1Decay, 0.0f);
    EXPECT_LE(plugin->fegGlobals.a1Decay, 1.0f);
}

TEST_F(PluginInstance, AEG_Attack_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("AEG attack", plugin->aegAttackParam->getName(10).getCharPointer());
}

TEST_F(PluginInstance, AEG_Decay_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("AEG decay", plugin->aegDecayParam->getName(9).getCharPointer());
}

TEST_F(PluginInstance, AEG_Sustain_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("AEG sustain", plugin->aegSustainParam->getName(11).getCharPointer());
}

TEST_F(PluginInstance, AEG_Release_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("AEG release", plugin->aegReleaseParam->getName(11).getCharPointer());
}

TEST_F(PluginInstance, FEG_Attack_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("FEG attack", plugin->fegAttackParam->getName(10).getCharPointer());
}

TEST_F(PluginInstance, FEG_Decay_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("FEG decay", plugin->fegDecayParam->getName(9).getCharPointer());
}

TEST_F(PluginInstance, FEG_Decay_Level_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("FEG decay level", plugin->fegDecayLevelParam->getName(15).getCharPointer());
}

TEST_F(PluginInstance, FEG_Sustain_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("FEG sustain", plugin->fegSustainParam->getName(11).getCharPointer());
}

TEST_F(PluginInstance, FEG_Release_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("FEG release", plugin->fegReleaseParam->getName(11).getCharPointer());
}

TEST_F(PluginInstance, Filter_Cutoff_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("filter cutoff frequency", plugin->filterCutoffParam->getName(23).getCharPointer());
}

TEST_F(PluginInstance, Filter_Resonance_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("filter resonance", plugin->filterResonanceParam->getName(16).getCharPointer());
}

TEST_F(PluginInstance, Filter_Envelope_Depth_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("filter envelope depth", plugin->filterEnvDepthParam->getName(21).getCharPointer());
}

TEST_F(PluginInstance, Gain_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("Gain", plugin->GainParam->getName(5).getCharPointer());
}

TEST_F(PluginInstance, Pan_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("Pan", plugin->PanParam->getName(4).getCharPointer());
}

TEST_F(PluginInstance, Velocity_Depth_Parameter_Has_Correct_Name)
{
    EXPECT_STREQ("Velocity depth", plugin->VelocityDepthParam->getName(15).getCharPointer());
}

TEST_F(PluginInstance, Gain_parameter_sets_gain_data_correctly)
{
    *plugin->GainParam = 0.0f;
    EXPECT_EQ(0.0f, plugin->amplifierGlobals.gain);
    *plugin->GainParam = 1.1f;
    EXPECT_EQ(1.0f, plugin->amplifierGlobals.gain);
    *plugin->GainParam = -0.0001f;
    EXPECT_EQ(0.0f, plugin->amplifierGlobals.gain);
    *plugin->GainParam = 1.0f;
    EXPECT_EQ(1.0f, plugin->amplifierGlobals.gain);
}

TEST_F(PluginInstance, Pan_parameter_sets_gain_data_correctly)
{
    *plugin->PanParam = 0.0f;
    EXPECT_EQ(1.0f, plugin->amplifierGlobals.pan.left_scalar);
    EXPECT_EQ(0.0f, plugin->amplifierGlobals.pan.right_scalar);
    *plugin->PanParam = 1.0f;
    EXPECT_EQ(0.0f, plugin->amplifierGlobals.pan.left_scalar);
    EXPECT_EQ(1.0f, plugin->amplifierGlobals.pan.right_scalar);
    *plugin->PanParam = 0.5f;
    EXPECT_EQ(0.5f, plugin->amplifierGlobals.pan.left_scalar);
    EXPECT_EQ(0.5f, plugin->amplifierGlobals.pan.right_scalar);
}

TEST_F(PluginInstance, Velocity_depth_parameter_sets_gain_data_correctly)
{
    *plugin->VelocityDepthParam = 1.0f;
    EXPECT_EQ(1.0f, plugin->amplifierGlobals.velocity_depth);
    *plugin->VelocityDepthParam = 0.0f;
    EXPECT_EQ(0.0f, plugin->amplifierGlobals.velocity_depth);
}

#if 0 // getDefaultValue converts the value to a float in [0, 1] range.
TEST_F(PluginInstance, Parameters_have_correct_default_values)
{
    EXPECT_FLOAT_EQ(63.0f,  plugin->aegAttackParam->getDefaultValue());
    EXPECT_FLOAT_EQ(63.0f,  plugin->aegDecayParam->getDefaultValue());
    EXPECT_FLOAT_EQ(63.0f,  plugin->aegSustainParam->getDefaultValue());
    EXPECT_FLOAT_EQ(63.0f,  plugin->aegReleaseParam->getDefaultValue());
    EXPECT_FLOAT_EQ(63.0f,  plugin->fegAttackParam->getDefaultValue());
    EXPECT_FLOAT_EQ(63.0f,  plugin->fegDecayParam->getDefaultValue());
    EXPECT_FLOAT_EQ(63.0f,  plugin->fegDecayLevelParam->getDefaultValue());
    EXPECT_FLOAT_EQ(63.0f,  plugin->fegSustainParam->getDefaultValue());
    EXPECT_FLOAT_EQ(63.0f,  plugin->fegReleaseParam->getDefaultValue());
    EXPECT_FLOAT_EQ(512.0f, plugin->filterCutoffParam->getDefaultValue());
    EXPECT_FLOAT_EQ(0.0f,   plugin->filterResonanceParam->getDefaultValue());
    EXPECT_FLOAT_EQ(0.0f,   plugin->filterEnvDepthParam->getDefaultValue());
}
#endif
// Get Label is meant for units, e.g. 'Hz'. It's not used at the moment.

TEST_F(PluginInstance, Sample_Rate_Dependent_Parameters_Update_Correctly)
{
    const double sample_rate = 100.0;
    const int buffer_size = 1;
    const float cutoff = 1000.0f;

    plugin->filterGlobals.g = cutoff * plugin->filterGlobals.piOverFs;
    plugin->prepareToPlay(sample_rate, buffer_size);
    const float pi_over_sample_rate = float_Pi / (float)sample_rate;
    const float conversion_factor = sample_rate / 44100.0f;

    EXPECT_FLOAT_EQ(pi_over_sample_rate, plugin->filterGlobals.piOverFs);
    // EXPECT_FLOAT_EQ(pi_over_sample_rate * cutoff, plugin->filterGlobals.g); SKIP for now.
    EXPECT_FLOAT_EQ(pi_over_sample_rate * conversion_factor * MAX_CUTOFF, plugin->filterGlobals.maxG);
    EXPECT_FLOAT_EQ(pi_over_sample_rate * conversion_factor * MIN_CUTOFF, plugin->filterGlobals.minG);
    EXPECT_FLOAT_EQ(1.0f / sample_rate, plugin->oscGlobals.inverseSampleRate);
    
}

typedef struct
{
    JsynthParameter0to127* pointer;
    float init_value;
    float new_value;
} Parameter;

TEST_F(PluginInstance, Parameter_value_changes_cause_no_distortion)
{
    const int sample_rate = 8192;
    const int buffer_length = sample_rate / 8;
    const int midi_channel = 1;
    const int midi_note_number = 69;  // A 440 Hz
    const int num_buffers = 8;

    plugin->prepareToPlay((double)sample_rate, buffer_length);

    MidiBuffer midi_buf;
    midi_buf.addEvent(MidiMessage::noteOn(midi_channel, midi_note_number, 1.0f), 0);

    std::vector<Parameter> parameters = {
        {plugin->aegAttackParam, 0.0f, 30.0f},
        {plugin->aegDecayParam, 127.0f, 0.0f},
        {plugin->aegSustainParam, 127.0f, 60.0f},
        {plugin->aegReleaseParam, 50.0f, 0.0f},
        {plugin->filterCutoffParam, 1023.0f, 400.0f},
        {plugin->filterResonanceParam, 0.0f, 0.9f},
        {plugin->filterEnvDepthParam, 0.0f, 0.7f},
        {plugin->fegAttackParam, 63.0f, 0.0f},
        {plugin->fegDecayParam, 63.0f, 127.0f},
        {plugin->fegDecayLevelParam, -30.0f, 0.0f},
        {plugin->fegSustainParam, 0.0f, -23.0f},
        {plugin->fegReleaseParam, 127.0f, 1.0f},
        {plugin->GainParam, 1.0f, 0.5f},
        {plugin->PanParam, 0.25f, 0.75f},
        {plugin->VelocityDepthParam, 0.0f, 1.0f}
    };

    for (auto& param : parameters)
        *param.pointer = param.init_value;

    std::vector<std::complex<float>> input_to_spectrum = {};
    input_to_spectrum.reserve(parameters.size() * buffer_length);

    for (auto& param : parameters)
    {
        AudioSampleBuffer sample_buffer(2, buffer_length);
        plugin->processBlock(sample_buffer, midi_buf);

        // Store a mix of the two channels.
        const float* p_sample_left  = sample_buffer.getReadPointer(0);
        const float* p_sample_right = sample_buffer.getReadPointer(1);

        for (size_t i = 0; i < buffer_length; ++i)
            input_to_spectrum.emplace_back((*p_sample_left++ + *p_sample_right++) / 2.0f);

        *param.pointer = param.new_value;
        midi_buf.clear();
    }

    juce::dsp::FFT fft = {juce::roundToInt(std::log2f(sample_rate))};
    std::vector<std::complex<float>> fft_output;
    fft_output.reserve(fft.getSize());

    for (size_t i = 0; i < fft.getSize(); ++i)
        fft_output.emplace_back(0.0f);

    fft.perform(input_to_spectrum.data(), fft_output.data(), false);
    std::vector<float> fft_output_abs;
    fft_output_abs.reserve(fft_output.size());

    for (auto& complex_number : fft_output)
        fft_output_abs.emplace_back(std::abs(complex_number));

    utilities::magnitude_response_in_db(fft_output_abs, sample_rate);

    // Verify the frequency bin.
    const size_t frequency_index = 440;

    EXPECT_GT(fft_output_abs[frequency_index], -13.0f);

    // Verify that there is no significant energy before or after the frequency bin.
    for (size_t i = 0; i < frequency_index; ++i)
        EXPECT_LT(fft_output_abs[i], -50.0f);

    for (size_t i = frequency_index + 1; i < fft_output_abs.size() / 2; ++i)
        EXPECT_LT(fft_output_abs[i], -50.0f);
}

/*
  ==============================================================================

    test_phase_counter.cpp
    Created: 2 Dec 2018 6:54:28pm
    Author:  jussi

  ==============================================================================
*/

#include "gtest/gtest.h"
#include "Phase.h"

class TestPhaseCounter : public ::testing::Test
{
protected:
    Phase phase = Phase();
};

TEST_F(TestPhaseCounter, Constructor_Sets_Angle_To_Zero)
{
    EXPECT_EQ(0.0f, phase.getAngle());
}

TEST_F(TestPhaseCounter, Constructor_Sets_Delta_To_Zero)
{
    EXPECT_EQ(0.0f, phase.getDelta());
}

TEST_F(TestPhaseCounter, SetAngle_Sets_A_Correct_Value)
{
    phase.setAngle(2.0f);
    EXPECT_EQ(2.0f, phase.getAngle());
    phase.setAngle(-0.1f);
    EXPECT_EQ(-0.1f, phase.getAngle());
    phase.setAngle(0.0f);
    EXPECT_EQ(0.0f, phase.getAngle());
}

TEST_F(TestPhaseCounter, SetDelta_Sets_A_Correct_Value)
{
    phase.setDelta(2.0f);
    EXPECT_EQ(2.0f, phase.getDelta());
    phase.setDelta(-0.1f);
    EXPECT_EQ(-0.1f, phase.getDelta());
    phase.setDelta(0.0f);
    EXPECT_EQ(0.0f, phase.getDelta());
}

TEST_F(TestPhaseCounter, Update_Increases_Angle_By_Delta)
{
    phase.setDelta(0.99999f);
    phase.update();
    EXPECT_EQ(0.99999f, phase.getAngle());
}

TEST_F(TestPhaseCounter, Angle_Wraps_Around_When_Its_Value_Equals_One_After_Increment)
{
    phase.setDelta(1.0f);
    phase.update();
    EXPECT_EQ(0.0f, phase.getAngle());
}

TEST_F(TestPhaseCounter, Angle_Wraps_Around_When_Its_Value_Is_Higher_Than_One_After_Increment)
{
    phase.setDelta(1.00001f);
    phase.update();
    EXPECT_NEAR(0.00001f, phase.getAngle(), 0.00001f);
}

TEST_F(TestPhaseCounter, Angle_Does_Not_Wrap_Around_At_Zero_When_Delta_is_Negative)
{
    phase.setDelta(-0.1f);
    phase.update();
    EXPECT_EQ(-0.1f, phase.getAngle());
}

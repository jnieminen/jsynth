/*
  ==============================================================================

    test_pitch_shifter.cpp
    Created: 27 May 2019 10:58:53pm
    Author:  jussi

  ==============================================================================
*/

#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "utilities.h"
#include "PitchShifter.h"


class TestPitchShifter : public ::testing::Test
{
public:
    virtual void SetUp()
    {
        sample_buffer.fill(0.0f);
    }

    void processBuffer()
    {
        for (float& sample : sample_buffer)
            sample = pitch_shifter.tick(sample);
    }

protected:
    const float sample_rate = 8192.0f;
    PitchShifter pitch_shifter = {sample_rate};
    std::array<float, 2 * 8192> sample_buffer;
    juce::dsp::FFT fft = {juce::roundToInt(std::log2(sample_buffer.size())) - 1};
};

TEST_F(TestPitchShifter, shift_up_an_octave)
{
    float f0 = 1024.0f;
    float f1 = 2048.0f;

    utilities::sine_tone(sample_buffer, f0, sample_rate);
    pitch_shifter.set_shift_factor(f0, f1);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, sample_rate);

    const unsigned int expected_peak_index = roundToInt(f1);
    unsigned int actual_peak_index = 0;
    float max = -96.0f;

    // Get the actual peak frequency index.
    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (sample_buffer[i] > max)
        {
            max = sample_buffer[i];
            actual_peak_index = i;
        }
    }

    EXPECT_EQ(expected_peak_index, actual_peak_index);
}

TEST_F(TestPitchShifter, shift_down_an_octave)
{
    float f0 = 128.0f;
    float f1 = 64.0f;

    utilities::sine_tone(sample_buffer, f0, sample_rate);
    pitch_shifter.set_shift_factor(f0, f1);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, sample_rate);

    const unsigned int expected_peak_index = roundToInt(f1);
    unsigned int actual_peak_index = 0;
    float max = -96.0f;

    // Get the actual peak frequency index.
    for (unsigned int i = 0; i < sample_buffer.size() / 4; ++i)
    {
        if (sample_buffer[i] > max)
        {
            max = sample_buffer[i];
            actual_peak_index = i;
        }
    }

    EXPECT_EQ(expected_peak_index, actual_peak_index);
}

TEST_F(TestPitchShifter, shifting_up_an_octave_creates_less_than_30dB_of_distortion)
{
    float f0 = 128.0f;
    float f1 = 256.0f;

    utilities::sine_tone(sample_buffer, f0, sample_rate);
    pitch_shifter.set_shift_factor(f0, f1);
    processBuffer();
    fft.performFrequencyOnlyForwardTransform(sample_buffer.data());
    utilities::magnitude_response_in_db(sample_buffer, sample_rate);

    const unsigned int expected_peak_index = roundToInt(f1);
    const unsigned int peak_index_tolerance = 8;
    const float distortion_limit_in_dB = -30.0f;

    for (unsigned int i = 0; i < expected_peak_index - peak_index_tolerance; ++i)
    {
        EXPECT_LT(sample_buffer[i], distortion_limit_in_dB)
            << "sample_buffer[" << i << "] = "
            << ::testing::PrintToString(sample_buffer[i]) << "dB (> "
            << distortion_limit_in_dB << " dB)";
    }

    for (unsigned int i = expected_peak_index + peak_index_tolerance;
         i < sample_buffer.size() / 4; ++i)
    {
        EXPECT_LT(sample_buffer[i], distortion_limit_in_dB)
            << "sample_buffer[" << i << "] = "
            << ::testing::PrintToString(sample_buffer[i]) << "dB (> "
            << distortion_limit_in_dB  << " dB)";
    }
}

/*
  ==============================================================================

    test_plugin_config.cpp
    Created: 1 May 2018 7:29:04pm
    Author:  jussi

  ==============================================================================
*/

#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "plugin_instance.h"


TEST_F(PluginInstance, HasCorrectName)
{
    EXPECT_EQ("JSynth", plugin->getName().toStdString());
}


TEST_F(PluginInstance, HasEditor)
{
    EXPECT_TRUE(plugin->hasEditor());
}


TEST_F(PluginInstance, AcceptsMidi)
{
    EXPECT_TRUE(plugin->acceptsMidi());
}


TEST_F(PluginInstance, DoesNotProduceMidi)
{
    EXPECT_FALSE(plugin->producesMidi());
}


TEST_F(PluginInstance, NumProgramsIsOne)
{
    EXPECT_EQ(1, plugin->getNumPrograms());
}


TEST_F(PluginInstance, CurrentProgramIsZero)
{
    EXPECT_EQ(0, plugin->getCurrentProgram());
}


TEST_F(PluginInstance, EmptyProgramName)
{
    EXPECT_EQ("", plugin->getProgramName(0).toStdString());
    EXPECT_EQ("", plugin->getProgramName(1).toStdString());
}


TEST_F(PluginInstance, IsRealTime)
{
    EXPECT_FALSE(plugin->isNonRealtime());
}


TEST_F(PluginInstance, NotMidiEffect)
{
    EXPECT_FALSE(plugin->isMidiEffect());
}


TEST_F(PluginInstance, DoesNotSupportDoublePrecision)
{
    EXPECT_FALSE(plugin->supportsDoublePrecisionProcessing());
}


TEST_F(PluginInstance, NotUsingDoublePrecision)
{
    EXPECT_FALSE(plugin->isUsingDoublePrecision());
}


TEST_F(PluginInstance, DoesNotSupportMpe)
{
    EXPECT_FALSE(plugin->supportsMPE());
}


TEST_F(PluginInstance, ZeroInputChannels)
{
    EXPECT_EQ(0, plugin->getTotalNumInputChannels());
}


TEST_F(PluginInstance, TwoOutputChannels)
{
    EXPECT_EQ(2, plugin->getTotalNumOutputChannels());
}


TEST_F(PluginInstance, SynthNumVoices)
{
    EXPECT_EQ(16, plugin->synth.getNumVoices());
}


TEST_F(PluginInstance, SynthNumSounds)
{
    EXPECT_EQ(1, plugin->synth.getNumSounds());
}


TEST_F(PluginInstance, SynthSampleRate)
{
    std::array<double, 3> expected_samplerates = {-1, 0, 192000};

    for (auto& fs : expected_samplerates)
    {
        plugin->prepareToPlay(fs, 0);
        EXPECT_DOUBLE_EQ(fs, plugin->synth.getSampleRate());
    }
}


// int 	getMainBusNumInputChannels () const noexcept
//  	Returns the number of input channels on the main bus. More...
//
// int 	getMainBusNumOutputChannels () const noexcept
//  	Returns the number of output channels on the main bus. More...
// double 	getSampleRate () const noexcept
//  	Returns the current sample rate. More...
//
// int 	getBlockSize () const noexcept
//  	Returns the current typical block size that is being used. More...

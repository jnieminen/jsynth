/*
  ==============================================================================

    test_portamento.cpp
    Created: 4 Mar 2019 9:40:35pm
    Author:  jussi

  ==============================================================================
*/

#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "plugin_instance.h"
#include "Portamento.h"


class TestPortamento : public ::testing::Test
{
public:
    virtual void SetUp()
    {
        buffer.fill(1.0f);
    }

    void eight_ticks();
protected:
    Portamento portamento = Portamento();
    std::array<float, 8> buffer = {};
    const float tolerance = 2e-5f;
};

void TestPortamento::eight_ticks()
{
    buffer.fill(1.0f);
    float s_prev = buffer[0];

    for (float& s : buffer)
    {
        s = portamento.tick(s_prev);
        s_prev = s;
    }
}

TEST_F(TestPortamento, Low_To_High_Frequency_In_Expected_Time)
{
    float f0       = 1.0f;
    const float f1 = 16.0f;
    Portamento::start_frequency = &f0;
    portamento.set_factor(f1, 8.0f);
    eight_ticks();
    EXPECT_FLOAT_EQ(f1, buffer.back() * f0);
}

TEST_F(TestPortamento, High_To_Low_Frequency_In_Expected_Time)
{
    float f0       = 8000.0f;
    const float f1 = 1.0f;
    Portamento::start_frequency = &f0;
    portamento.set_factor(f1, 8.0f);
    eight_ticks();
    EXPECT_FLOAT_EQ(f1, buffer.back() * f0);
}

TEST_F(TestPortamento, Low_To_High_Note_Produces_An_Ascending_Curve)
{
    std::array<const float, 8> expected_freq_ascend = {
        1.77828f,
        3.16228f,
        5.62341f,
        10.00000f,
        17.78279f,
        31.62278f,
        56.23413f,
        100.00000f
    };

    float f0       = 1.0f;
    const float f1 = 100.0f;
    Portamento::start_frequency = &f0;
    portamento.set_factor(f1, 8.0f);
    eight_ticks();

    for (size_t n = 0; n < buffer.size(); ++n)
        EXPECT_NEAR(expected_freq_ascend[n], buffer[n], tolerance);
}

TEST_F(TestPortamento, High_To_Low_Note_Produces_A_Descending_Curve)
{
    std::array<const float, 8> expected_freq_descend = {
        0.56234f,
        0.31623f,
        0.17783f,
        0.1f,
        0.05623f,
        0.031623f,
        0.017783f,
        0.010000f
    };

    float f0       = 100.0f;
    const float f1 = 1.0f;
    Portamento::start_frequency = &f0;
    portamento.set_factor(f1, 8.0f);
    eight_ticks();

    for (size_t n = 0; n < buffer.size(); ++n)
        EXPECT_NEAR(expected_freq_descend[n], buffer[n], tolerance);
}

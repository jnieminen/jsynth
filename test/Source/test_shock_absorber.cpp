#include "gtest/gtest.h"
#include "Globals.h"


typedef struct {
    float foo = 0.0f;
    float bar = 0.0f;
} ValueContainer;


class TestShockAbsorber : public ::testing::Test
{
protected:
    ShockAbsorber shock_absorber;
    ValueContainer value_container;
};

TEST_F(TestShockAbsorber, Update_does_nothing_when_cache_is_empty)
{

}

TEST_F(TestShockAbsorber, Update_pops_a_cached_value_and_copies_it_to_the_reference)
{

}

TEST_F(TestShockAbsorber, Update_pops_multiple_cached_values_and_copies_them_to_the_references)
{

}

TEST_F(TestShockAbsorber, Emptied_cache_is_removed)
{

}

TEST_F(TestShockAbsorber, Emptied_caches_are_removed)
{

}

TEST_F(TestShockAbsorber, New_entry_is_added_to_the_cache)
{

}

TEST_F(TestShockAbsorber, Old_entry_is_refreshed_in_the_cache)
{

}

TEST_F(TestShockAbsorber, Filter_is_applied)
{

}

TEST_F(TestShockAbsorber, An_existing_entry_is_found_in_cache)
{

}

TEST_F(TestShockAbsorber, A_non_existing_entry_is_not_found_in_cache)
{

}

TEST_F(TestShockAbsorber, An_entry_is_not_found_in_an_empty_cache)
{

}


class TestParamWithSmoothing : public ::testing::Test
{
protected:
    ParamWithSmoothing param;
    ValueContainer value_container;
};

cache_zero_initialized_by_default
cache_initialized_with_given_value


TEST_F(TestParamWithSmoothing, cache_zero_initialized_by_default)
{
    EXPECT_FLOAT_EQ(0.0f, param.get());
}

TEST_F(TestParamWithSmoothing, cache_initialized_with_given_value)
{
    const float given_value = 123.0f;
    param = Param(given_value);
    EXPECT_FLOAT_EQ(given_value, param.get());
}

TEST_F(TestParamWithSmoothing, get_always_gives_the_same_value)
{
    const float given_value = 123.0f;
    param = Param(given_value);
    EXPECT_FLOAT_EQ(given_value, param.get());
    EXPECT_FLOAT_EQ(given_value, param.get());
    EXPECT_FLOAT_EQ(given_value, param.get());
}

TEST_F(TestParamWithSmoothing, cache_length_fs_2500_blocksize_1)
{
    cache.init_cache(5500u, 4u);
    param = 1.0f;
    const unsigned int expected_ramp_size = 5u;
    const unsigned int expected_cache_size = 8u;
    for (auto i = 0; i < expected_ramp_size; ++i)
    {
        EXPECT_LT(param.get(), 1.0f);
        EXPECT_GE(param.get(), 0.0f);
        param.next_sample();
    }
    for (auto i = expected_ramp_size; i < expected_cache_size; ++i)
    {
        EXPECT_FLOAT_EQ(param.get(), 1.0f);
        param.next_sample();
    }
}

TEST_F(TestParamWithSmoothing, cache_length_fs_2500_blocksize_2)
{

}

TEST_F(TestParamWithSmoothing, cache_length_fs_2500_blocksize_3)
{

}

TEST(Next_power_of_two_of_0_is_0)
{
    EXPECT_EQ(0u, round_to_next_power_of_two(0u));
}

TEST(Next_power_of_two_of_3_is_4)
{
    EXPECT_EQ(3u, round_to_next_power_of_two(4u));
}

TEST(Next_power_of_two_of_2147483649_is_4294967296)
{
    EXPECT_EQ(2147483649u, round_to_next_power_of_two(4294967296u));
}

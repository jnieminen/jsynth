/*
  ==============================================================================

    test_voice.cpp
    Created: 24 Jan 2019 10:41:44pm
    Author:  jussi

  ==============================================================================
*/

#include "gtest/gtest.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "plugin_instance.h"
#include "utilities.h"
#include "JsynthVoice.h"
#include "JsynthTables.h"

#define SAMPLE_RATE 8192
#define BUFFER_LENGTH (2 * SAMPLE_RATE)

class TestVoice : public PluginInstance
{
protected:
    TestVoice()
    {
        plugin->filterGlobals.g = float_Pi / 2.0f;
        plugin->aegGlobals.a1Attack = exp(-1.0f / 1);
        plugin->aegGlobals.b0Attack = 1.0f - plugin->aegGlobals.a1Attack;
        plugin->aegGlobals.a1Decay = exp(-1.0f / 1);
        plugin->aegGlobals.b0Decay = 1 - plugin->aegGlobals.a1Decay;
        plugin->aegGlobals.sustainLv = 1.0f;
        plugin->aegGlobals.sustainLvOvershot = 1.0f;
        plugin->aegGlobals.a1Release = exp(-1.0f / 1);
        plugin->aegGlobals.b0Release = 1.0f - plugin->aegGlobals.a1Release;
    };

    std::vector<float> linspace(float start, float end, unsigned int length)
    {
        std::vector<float> x;
        x.reserve(length);

        for (size_t i = 0; i < length; ++i)
        {
            x.push_back(start + i / (float)length * (end - start));
        }

        return x;
    }

    juce::dsp::FFT fft = {juce::roundToInt(std::log2f(BUFFER_LENGTH)) - 1};
};

/* Test plan:

When no notes are played, there should be zeroes in the output buffer.
MIDI note numbers produce correct frequencies.
Velocities produce correct gains.
AEG produces correct amplitude ramps.
Todo: FEG produces correct frequency spectrum.
Todo: At max polyphony, voices should start and stop in FIFO principle:
    - Instantly, but gracefully reassign old voices, i.e. without audible artifacts
    - e.g. sine tones and measure dB at peaks and in between.
*/

TEST_F(TestVoice, No_Notes_Produces_a_Zero_Buffer)
{
    int buffer_length = SAMPLE_RATE;
    AudioSampleBuffer sample_buffer(2, buffer_length);
    MidiBuffer midi_buf;
    plugin->prepareToPlay(SAMPLE_RATE, buffer_length);
    plugin->processBlock(sample_buffer, midi_buf);

    for (auto i = 0; i < sample_buffer.getNumChannels(); ++i)
    {
        const float* p = sample_buffer.getReadPointer(i);

        for (auto j = 0; j < sample_buffer.getNumSamples(); ++j)
            EXPECT_FLOAT_EQ(0.0f, p[j]);
    }
}

TEST_F(TestVoice, Note_Produces_a_Correct_Frequency_And_Amplitude)
{
    int buffer_length = SAMPLE_RATE;
    AudioSampleBuffer sample_buffer(2, buffer_length);
    MidiBuffer midi_buf;
    plugin->prepareToPlay(SAMPLE_RATE, buffer_length);

    int midi_channel = 1;
    int midi_note_number = 69;  // A 440 Hz
    midi_buf.addEvent(MidiMessage::noteOn(midi_channel, midi_note_number, 1.0f), 0);
    midi_buf.addEvent(MidiMessage::noteOff(midi_channel, midi_note_number, 1.0f), buffer_length / 2);

    plugin->processBlock(sample_buffer, midi_buf);

    std::vector<float> input_to_spectrum = {};
    const float* p_sample = sample_buffer.getReadPointer(1);  // Panned to full right by default!

    input_to_spectrum.reserve(2 * SAMPLE_RATE);

    for (size_t i = 0; i < buffer_length; ++i)
        input_to_spectrum.emplace_back(*p_sample++);

    for (size_t i = buffer_length; i < SAMPLE_RATE * 2; ++i)
        input_to_spectrum.emplace_back(0.0f);

    fft.performFrequencyOnlyForwardTransform(input_to_spectrum.data());
    utilities::magnitude_response_in_db(input_to_spectrum, SAMPLE_RATE);

    // Index is actually (SAMPLE_RATE/44100) * 440 Hz but why?
    const int index_to_evaluate = 1 + (SAMPLE_RATE / 44100.0f)
        * roundToInt(MidiMessage::getMidiNoteInHertz(midi_note_number));
    const float negative_six_dB = 20.0f * log10f(0.5f);
    EXPECT_NEAR(input_to_spectrum.at(index_to_evaluate), negative_six_dB, 0.3f);
}

TEST_F(TestVoice, Zero_Velocity_Produces_a_Zero_Buffer)
{
    int buffer_length = SAMPLE_RATE;
    AudioSampleBuffer sample_buffer(2, buffer_length);
    MidiBuffer midi_buf;
    plugin->prepareToPlay(SAMPLE_RATE, buffer_length);

    int midi_channel = 1;
    int midi_note_number = 48;  // C 130.813 Hz
    midi_buf.addEvent(MidiMessage::noteOn(midi_channel, midi_note_number, 0.0f), 0);
    midi_buf.addEvent(MidiMessage::noteOff(midi_channel, midi_note_number, 0.0f), buffer_length / 2);
    plugin->processBlock(sample_buffer, midi_buf);

    for (auto i = 0; i < sample_buffer.getNumChannels(); ++i)
    {
        const float* p = sample_buffer.getReadPointer(i);

        for (auto j = 0; j < sample_buffer.getNumSamples(); ++j)
            EXPECT_FLOAT_EQ(0.0f, p[j]);
    }
}

TEST_F(TestVoice, Amplitude_Increases_During_Attack)
{
    unsigned int buffer_length = SAMPLE_RATE / 2;
    std::vector<float> threshold = linspace(0.0f, 0.45f, buffer_length / 4);
    std::vector<float> threshold2 = linspace(0.45f, 0.7f, buffer_length / 4);
    std::vector<float> threshold3 = linspace(0.7f, 1.0f, buffer_length / 2);
    threshold.insert(threshold.end(), threshold2.begin(), threshold2.end());
    threshold.insert(threshold.end(), threshold3.begin(), threshold3.end());
    AudioSampleBuffer sample_buffer(2, buffer_length);
    MidiBuffer midi_buf;
    unsigned int attack_time_in_samples = buffer_length;

    plugin->aegGlobals.a1Attack = exp(-1.0f / attack_time_in_samples);
    plugin->aegGlobals.b0Attack = 1.0f - plugin->aegGlobals.a1Attack;

    plugin->prepareToPlay(SAMPLE_RATE, buffer_length);

    int midi_channel = 1;
    int midi_note_number = 108;  // C8 4186 Hz
    midi_buf.addEvent(MidiMessage::noteOn(midi_channel, midi_note_number, 1.0f), 0);
    plugin->processBlock(sample_buffer, midi_buf);

    for (auto i = 0; i < sample_buffer.getNumChannels(); ++i)
    {
        const float* p = sample_buffer.getReadPointer(i);

        for (auto j = 0; j < sample_buffer.getNumSamples(); ++j)
            EXPECT_LE(p[j], threshold[j]);
    }
}

TEST_F(TestVoice, Amplitude_Decreases_During_Release)
{
    unsigned int buffer_length = SAMPLE_RATE / 2;

    // Use a more relaxed threshold, because release time is a bit longer in reality.
    std::vector<float> threshold = linspace(1.0f, 0.6f, buffer_length / 4);
    std::vector<float> threshold2 = linspace(0.6f, 0.45f, buffer_length / 4);
    std::vector<float> threshold3 = linspace(0.45f, 0.3f, buffer_length / 2);
    threshold.insert(threshold.end(), threshold2.begin(), threshold2.end());
    threshold.insert(threshold.end(), threshold3.begin(), threshold3.end());
    AudioSampleBuffer sample_buffer(2, buffer_length);
    MidiBuffer midi_buf;

    plugin->aegGlobals.a1Release = exp(-1.0f / buffer_length);
    plugin->aegGlobals.b0Release = 1.0f - plugin->aegGlobals.a1Release;

    plugin->prepareToPlay(SAMPLE_RATE, buffer_length);

    int midi_channel = 1;
    int midi_note_number = 108;  // C8 4186 Hz
    midi_buf.addEvent(MidiMessage::noteOn(midi_channel, midi_note_number, 1.0f), 0);
    midi_buf.addEvent(MidiMessage::noteOff(midi_channel, midi_note_number, 1.0f), 1);
    plugin->processBlock(sample_buffer, midi_buf);

    for (auto i = 0; i < sample_buffer.getNumChannels(); ++i)
    {
        const float* p = sample_buffer.getReadPointer(i);

        for (auto j = 0; j < sample_buffer.getNumSamples(); ++j)
            EXPECT_LE(p[j], threshold[j]);
    }
}

TEST_F(TestVoice, Amplitude_Decreases_During_Decay)
{
    unsigned int buffer_length = SAMPLE_RATE / 2;

    // Strangely the decay time is quite a bit longer than specified.
    std::vector<float> threshold = linspace(1.0f, 0.28f, buffer_length);
    AudioSampleBuffer sample_buffer(2, buffer_length);
    MidiBuffer midi_buf;

    plugin->aegGlobals.a1Decay = exp(-1.0f / buffer_length);
    plugin->aegGlobals.b0Decay = 1.0f - plugin->aegGlobals.a1Decay;
    plugin->aegGlobals.decayLv = 1.0f;
    plugin->aegGlobals.sustainLv = 0.0f;
    plugin->aegGlobals.sustainLvOvershot = plugin->aegGlobals.overshootFactor
        * (plugin->aegGlobals.sustainLv - plugin->aegGlobals.decayLv) + plugin->aegGlobals.decayLv;

    plugin->prepareToPlay(SAMPLE_RATE, buffer_length);

    int midi_channel = 1;
    int midi_note_number = 108;  // C8 4186 Hz
    midi_buf.addEvent(MidiMessage::noteOn(midi_channel, midi_note_number, 1.0f), 0);
    plugin->processBlock(sample_buffer, midi_buf);

    for (auto i = 0; i < sample_buffer.getNumChannels(); ++i)
    {
        const float* p = sample_buffer.getReadPointer(i);

        for (auto j = 0; j < sample_buffer.getNumSamples(); ++j)
            EXPECT_LE(p[j], threshold[j]);
    }
}

// plugin->fegGlobals.a1Attack = pow(plugin->fegGlobals.decayLevel, 1.0f / 4.0f);
// plugin->fegGlobals.releaseTimeInv = 1.0f / buffer_length;
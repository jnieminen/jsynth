/*
  ==============================================================================

    utilities.cpp
    Created: 28 May 2019 1:05:49am
    Author:  jussi

  ==============================================================================
*/

#include <vector>
#include "../JuceLibraryCode/JuceHeader.h"
#include "utilities.h"

template<std::size_t SIZE>
void utilities::magnitude_response_in_db(std::array<float, SIZE>& buffer, float sample_rate)
{
    // Do operations one by one as (I think) it compiles to SIMD instructions.
    for (float& sample : buffer)
        sample /= (sample_rate / 2.0f);

    for (float& sample : buffer)
        sample = log10f(sample);

    for (float& sample : buffer)
        sample *= 20.0f;
}

void utilities::magnitude_response_in_db(std::vector<float>& buffer, float sample_rate)
{
    for (float& sample : buffer)
        sample /= (sample_rate / 2.0f);

    for (float& sample : buffer)
        sample = log10f(sample);

    for (float& sample : buffer)
        sample *= 20.0f;
}

template<std::size_t SIZE>
void utilities::sine_tone(std::array<float, SIZE>& buffer, float f, float fs)
{
    float phase = 0.0f;
    float delta = f / fs;

    for (float& sample : buffer)
    {
        sample = sin((2.0f * float_Pi) * phase);
        phase += delta;
        if (phase > 1.0f)
            phase -= 1.0f;
    }
}

template void utilities::magnitude_response_in_db<16384>(std::array<float, 16384>& buffer, float sample_rate);
template void utilities::sine_tone<16384>(std::array<float, 16384>& buffer, float f, float fs);
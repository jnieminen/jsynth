/*
  ==============================================================================

    utilities.h
    Created: 27 May 2019 11:14:22pm
    Author:  jussi

  ==============================================================================
*/

#pragma once

#include <array>
#include <vector>

namespace utilities
{
    template<std::size_t SIZE>
    void magnitude_response_in_db(std::array<float, SIZE>& buffer, float sample_rate);

    void magnitude_response_in_db(std::vector<float>& buffer, float sample_rate);

    template<std::size_t SIZE>
    void sine_tone(std::array<float, SIZE>& buffer, float f, float fs);
}
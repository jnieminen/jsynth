import numpy as np
import matplotlib.pyplot as plt
from math import floor, ceil
from scipy.io.wavfile import write as writewav
import scipy.fftpack as fft
from filter_design import convert_to_dB


class ThiranAllpassFirstOrder:
    def __init__(self):
        self._x_prev = 0
        self._y_prev = 0
        self._a1     = 0

    def set_coefficient(self, fraction):
        self._a1 = (1-fraction) / (fraction + 1)

    def tick(self, x):
        y = self._a1 * (x-self._y_prev) + self._x_prev
        self._x_prev = x
        self._y_prev = y
        return y


def linear_interpolation(x, arr):
    i = int(floor(x))
    j = int(ceil(x))
    return arr[i] + (x - floor(x)) * (arr[j] - arr[i])


def saw_wave_pb(f, fs, length):
    x = np.ndarray([length], dtype=float)
    delta = f / fs
    angle = 0.5

    for n in range(0, fs):
        x[n] = angle * 2 - 1

        # Detect non-linearity and calculate a polynomial
        # 1. Before a non-linearity
        if angle >= (1.0 - delta):
            t = (angle - 1.0) / delta
            x[n] -= (t * t + 2.0 * t + 1.0)

        # 2. After a non-linearity
        elif angle < delta:
            t = angle / delta
            x[n] -= (-t * t + 2.0 * t - 1.0)

        angle += delta
        if angle >= 1.0:
            angle -= 1.0

    return x


def pitch_shifter_process(x, delay_line, scale_factor, amp_mod, phases, allpass_filters):
    y_paral = np.zeros([3, len(x)], dtype=float)
    y = np.zeros([len(x)], dtype=float)

    for p in range(len(phases)):
        p_write = 0
        p_read = phases[p] * len(delay_line)

        for n in range(len(x)):
            delay_line[p_write] = x[n]

            if not allpass_filters:
                y_paral[p][n] = linear_interpolation(p_read, delay_line)
            else:
                integer_part = floor(p_read)
                fractional_part = p_read - integer_part

                y_paral[p][n] = delay_line[integer_part]
                allpass_filters[p].set_coefficient(fractional_part)
                y_paral[p][n] = allpass_filters[p].tick(y_paral[p][n])
            # Todo: Try a higher order Thiran allpass

            # Apply cosine amp mod.
            y_paral[p][n] += y_paral[p][n] * amp_mod[p][n]

            # Update ring buffer pointers.
            p_read += scale_factor

            if p_read >= len(delay_line):
                p_read -= len(delay_line)

            p_write += 1

            if p_write >= len(delay_line):
                p_write = 0

        for n, sample in enumerate(y_paral[p]):
            y[n] += sample

    for n in range(len(y)):
        y[n] /= 3

    plt.figure(1)
    plt.clf()
    plt.plot(y_paral[0])
    plt.plot(y_paral[1], 'r')
    plt.plot(y_paral[2], 'k')
    plt.grid(True)

    return y


def pitch_shifter_process_v2(x, delay_line, scale_factor, amp_mod, phases, allpass_filters):
    y_paral = np.zeros([3, len(x)], dtype=float)
    y = np.zeros([len(x)], dtype=float)

    p_write = 0
    p_read = [p * len(delay_line) for p in phases]

    # Shift
    for n in range(len(x)):
        for p in range(len(phases)):

            if not allpass_filters:
                y_paral[p][n] = linear_interpolation(p_read[p], delay_line)
            else:
                integer_part = floor(p_read[p])
                fractional_part = p_read[p] - integer_part

                y_paral[p][n] = delay_line[integer_part]
                allpass_filters[p].set_coefficient(fractional_part)
                y_paral[p][n] = allpass_filters[p].tick(y_paral[p][n])
            # Todo: Try a higher order Thiran allpass

            # Apply cosine amp mod.
            y_paral[p][n] += y_paral[p][n] * amp_mod[p][n]

            # Update ring buffer pointers.
            p_read[p] += scale_factor

            if p_read[p] >= len(delay_line):
                p_read[p] -= len(delay_line)

        delay_line[p_write] = x[n]
        p_write += 1

        if p_write >= len(delay_line):
            p_write = 0

    # Sum
    for p in range(len(phases)):
        for n, sample in enumerate(y_paral[p]):
            y[n] += sample

    # Scale
    for n in range(len(y)):
        y[n] /= 3

    plt.figure(1)
    plt.clf()
    plt.plot(y_paral[0])
    plt.plot(y_paral[1], 'r')
    plt.plot(y_paral[2], 'k')
    plt.grid(True)

    return y


def pitch_shifter(x, f0, f1, fs, interpolation_type=0):
    if interpolation_type == 0:
        allpass_filters = list()
    else:
        allpass_filters = [
            ThiranAllpassFirstOrder(),
            ThiranAllpassFirstOrder(),
            ThiranAllpassFirstOrder()
        ]

    scale_factor = f1 / f0
    delay_line_size = fs // 20
    fmod = (scale_factor - 1) * fs / delay_line_size

    delay_line = np.zeros([delay_line_size], dtype=float)

    # Initialize the delay time modulation signal.
    phases = (0, 1/3, 2/3)
    delay_time_mod = np.zeros([3, len(x)], dtype=float)

    for p in range(len(phases)):
        phase = phases[p]
        for n in range(len(x)):
            delay_time_mod[p][n] = phase
            phase += fmod / fs

            if phase >= 1:
                phase -= 1

    # Initialize the cosine window that is used in the amplitude modulation later.
    amp_mod = np.zeros([3, len(x)], dtype=float)

    for p in range(len(phases)):
        phase = phases[p]
        for n in range(len(x)):
            amp_mod[p][n] = -np.cos(2 * np.pi * phase)
            phase += fmod / fs

            if phase >= 1:
                phase -= 1

    #y = pitch_shifter_process(x, delay_line, scale_factor, amp_mod, phases, allpass_filters)
    y = pitch_shifter_process_v2(x, delay_line, scale_factor, amp_mod, phases, allpass_filters)

    # plt.figure(2)
    # plt.clf()
    # plt.plot(amp_mod[0])
    # plt.plot(amp_mod[1], 'r')
    # plt.plot(amp_mod[2], 'k')
    # plt.grid(True)

    return y


def sine_wave(f, fs, length):
    delta = f/fs
    phase = 0
    y = np.zeros([length], dtype=float)

    for n in range(length):
        y[n] = np.sin(2*np.pi*phase)
        phase += delta
        if phase >= 1:
            phase -= 1

    return y


def main():
    fs = 44100
    f0 = 1000
    f1 = 1020

    # x = saw_wave_pb(f0, fs, fs)
    x = sine_wave(f0, fs, fs)
    y = pitch_shifter(x, f0, f1, fs, interpolation_type=1)

    writewav("C:\\Users\\jussi\\PycharmProjects\\jsynth\\pitch_shifter.wav", fs, y)

    plt.figure(3)
    plt.clf()
    plt.plot(y)
    plt.grid(True)
    plt.figure(2)
    plt.clf()
    plt.plot(convert_to_dB(fft.fft(y, fs)))
    plt.xlim([0, fs / 2])
    plt.grid(True)
    plt.show()


if __name__ == "__main__":
    main()
